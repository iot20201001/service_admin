var MongoClient = require('mongodb').MongoClient
var url = 'mongodb://127.0.0.1:27017/'
MongoClient.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}, async (err, client) => {
  if (err) throw err
  console.log('数据库已创建')
  var db = client.db('runoob')
  await db.collection('tableName').insertOne({
    name: '000',
    password: '123456'
  })
  var session = client.startSession()
  await session.startTransaction()
  await db.collection('tableName').insertOne({
    name: '000',
    password: '12ssssss3456'
  }, {
    session
  })
  // await db.collection('tableName').insertOne({
  //   _id: '1',
  //   name: '000'
  // }, {
  //   session
  // })
  await db.collection('tableName')
    .insertOne({
      id: 'Utilsssssssssster()',
      code: 0
    }, {
      session
    })
  // await session.abortTransaction()
  await session.commitTransaction()
  session.endSession()
})