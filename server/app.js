/* eslint-disable camelcase */
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
// 返回的对象是一个键值对，当extended为false的时候，键值对中的值就为'String'或'Array'形式，为true的时候，则可为任何数据类型。
app.use(bodyParser.urlencoded({
  extended: true
}))
// parse application/json
app.use(bodyParser.json())
var {
  Utils,
  Entity
} = require('./dbV1.2/MessageEntitys')
class SysUserToken extends Entity {}
let noTokenPages = [
  // /v1/i,
  // /dict\/type\//i, // 字典不需要授权
  // /user\/login/i, // 后台管理用户登录
  // /v1\/wx_service\/api/i, // 微信自动登录
  // /\/Phone\//i, // 微信
  // /v1.1\/wx_service\/api/i, // 支付账户切换 【生成环境须注释掉】
  // /\/sys\/dict\/.*/i,
  // /\/sys\/menu\/.*/i,
  /\/auth\/.*/i,
  /\/monitor.*/i
  // '/api/sys/dict/type/all',
  // '/user/login'
]
let VerifyToken = true
// console.info('/sys/dict/type/all?_t=1590459323555'.match(/\/sys\/dict\/.*/i))
// 设置跨域访问
// https://www.jianshu.com/p/a763dd674293
app.all('*', async (req, res, next) => {
  // 设置允许跨域的域名，*代表允许任意域名跨域
  // console.info(req.headers.origin)
  // console.info(req.headers['token'])
  // console.info(req.headers['accept-language'])
  // console.info(req.headers['foreendversion'])
  // console.info(req.headers['terminaltype'])
  // console.info(req.headers['tenant_code'])
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  // 允许的header类型
  res.header('Access-Control-Allow-Headers', 'content-type,token,tenant_code, Accept-Language,X-Requested-With,TerminalType,foreEndVersion')
  // 跨域允许的请求方式
  res.header('Access-Control-Allow-Methods', '*')
  //
  // res.header('Access-Control-Allow-Credentials', 'true')
  var token = req.headers['token']
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  console.info(req.method, token, tenant_code, noTokenPages.findIndex(e => req.path.match(e)))
  if (req.method === 'OPTIONS') {
    res.end()
  } else if (noTokenPages.findIndex(e => req.path.match(e)) !== -1 && VerifyToken) {
    let t = await new SysUserToken(tenant_code).FindOneEntity({
      token
    })
    if (t.data !== null) {
      req.headers['uid'] = t.data.uid
      req.headers['dept_id'] = t.data.dept_id
      // console.info('-------------------------------', req.headers['dept_id'], t.data.dept_id)
    }
    next()
  } else {
    // console.info('tenant_code', tenant_code)
    let t = await new SysUserToken(tenant_code).FindOneEntity({
      token
    })
    console.info(token, t)
    if (t.data !== null) {
      req.headers['uid'] = t.data.uid
      req.headers['dept_id'] = t.data.dept_id
      // console.info('-------------------------------', req.headers['dept_id'], t.data.dept_id)
      // console.info('uid--VV->', t.data.uid)
      next()
    } else {
      res.json(Utils.resReturn(null, 401, '请先登录！' + req.url))
    }
    // next()
  }
})
// app.get('*',(req,res, next)=>{
//   next()
//   setTimeout(() => {
//     sssssssss
//   }, 2000);
//   new Promise((resolve,reject)=>{
// asas
//   })
// })

// let app_zzz_error = require('./BaseSvr/app_zzz_error')
// app.use(app_zzz_error.subPath, app_zzz_error)

let app_auth = require('./BaseSvr/app_auth')
app.use(app_auth.subPath, app_auth)
let app_sys_user = require('./BaseSvr/app_sys_user')
app.use(app_sys_user.subPath, app_sys_user)
let app_sys_menu = require('./BaseSvr/app_sys_menu')
app.use(app_sys_menu.subPath, app_sys_menu)
let app_sys_notice = require('./BaseSvr/app_sys_notice')
app.use(app_sys_notice.subPath, app_sys_notice)
let app_sys_tenant = require('./BaseSvr/app_sys_tenant')
app.use(app_sys_tenant.subPath, app_sys_tenant)
let app_sys_tenant_role = require('./BaseSvr/app_sys_tenant_role')
app.use(app_sys_tenant_role.subPath, app_sys_tenant_role)
let app_sys_dept = require('./BaseSvr/app_sys_dept')
app.use(app_sys_dept.subPath, app_sys_dept)
let app_sys_params = require('./BaseSvr/app_sys_params')
app.use(app_sys_params.subPath, app_sys_params)
let app_sys_params_history = require('./BaseSvr/app_sys_params_history')
app.use(app_sys_params_history.subPath, app_sys_params_history)
let app_sys_role = require('./BaseSvr/app_sys_role')
app.use(app_sys_role.subPath, app_sys_role)
let app_sys_dict_type = require('./BaseSvr/app_sys_dict_type')
app.use(app_sys_dict_type.subPath, app_sys_dict_type)
let app_sys_dict_data = require('./BaseSvr/app_sys_dict_data')
app.use(app_sys_dict_data.subPath, app_sys_dict_data)
let app_sys_region = require('./BaseSvr/app_sys_region')
app.use(app_sys_region.subPath, app_sys_region)
let app_sys_log_login = require('./BaseSvr/app_sys_log_login')
app.use(app_sys_log_login.subPath, app_sys_log_login)
// ===================================================================================
let app_pre_lib_marking = require('./PreLibSvr/app_pre_lib_marking')
app.use(app_pre_lib_marking.subPath, app_pre_lib_marking)
let app_pre_lib_certificate = require('./PreLibSvr/app_pre_lib_certificate')
app.use(app_pre_lib_certificate.subPath, app_pre_lib_certificate)
// ===================================================================================
let app_lib_instock = require('./LibSvr/app_lib_instock')
app.use(app_lib_instock.subPath, app_lib_instock)
let app_lib_plan = require('./LibSvr/app_lib_plan')
app.use(app_lib_plan.subPath, app_lib_plan)
let app_lib_outstock = require('./LibSvr/app_lib_outstock')
app.use(app_lib_outstock.subPath, app_lib_outstock)
let app_lib_settings = require('./LibSvr/app_lib_settings')
app.use(app_lib_settings.subPath, app_lib_settings)
// ===================================================================================
let executeCmd = require('./AdminSvr/ExecuteCmd')
app.use(executeCmd.subPath, executeCmd)
let UploadFiles = require('./AdminSvr/UploadFiles')
app.use(UploadFiles.subPath, UploadFiles)
// ===================================================================================

app.get('/monitor', async (req, res) => {
  // http://nodejs.cn/api/process.html
  // https://segmentfault.com/a/1190000011237265
  // http://www.haiyang.me/read.php?key=765
  // https://www.jianshu.com/p/71a999baafbb
  // https://www.jianshu.com/p/0c2efec91a4f
  // http://www.ruanyifeng.com/blog/2017/04/memory-leak.html
  let monitorInfo = {
    env: process.env,
    version: `版本: ${process.version}`,
    versions: `版本: ${process.versions}`,
    memoryUsage: process.memoryUsage()
  }
  monitorInfo.memoryUsage = Object.keys(monitorInfo.memoryUsage).map(x => {
    return {
      key: x,
      value: (monitorInfo.memoryUsage[x] / 1024 / 1024).toFixed(2) + 'MB'
    }
  })
  res.json(monitorInfo)
})

app.all('/api/submit', (req, res) => {
  res.json({
    code: 0,
    data: req.body,
    msg: 'success'
  })
})
// app.use((err, req, res, next) => {
//   // log the error...
//   console.error('err...............')
//   // res.sendStatus(err.httpStatusCode).json(err)
// })
process.on('uncaughtException', (err) => {
  console.log('这里有个未捕获的同步错误', err)
})
process.on('unhandledRejection', (err) => {
  console.log('这里有个未处理的异步错误', err)
})

var server = app.listen(8080, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('应用实例，访问地址为 http://%s:%s', host, port)
})
// 代码存在安全隐患，下个版本需要加入沙箱机制
// 借鉴YAPI  Ctrl+F let a = sandbox({a: 1}, 'a=2')
// https://gitee.com/mirrors/eval5
// https://gitee.com/-/ide/project/mirrors/YApi/edit/master/-/npm-publish.js
