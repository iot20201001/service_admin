var redis = require('redis')
let redisConfig = {
  url: 'localhost',
  port: 6379,
  password: '21232F297A57A5A743894A0E4A801FC3'
}
let rclient = redis.createClient(
  redisConfig.port,
  redisConfig.url,
  redisConfig.options)
rclient.auth(redisConfig.password, function () {
  console.log('redis通过认证')
})

const RedisUtils = {}
RedisUtils.setItem = (key, value, expire = 6) => {
  return new Promise((resolve, reject) => {
    rclient.set(key, value, function (err, result) {
      if (err) {
        console.log(err)
        reject(err)
      }
      if (!isNaN(expire) && expire > 0) {
        rclient.expire(key, parseInt(expire))
      }
      resolve(result)
    })
  })
}
RedisUtils.getItem = (key) => {
  return new Promise((resolve, reject) => {
    rclient.get(key, function (err, result) {
      if (err) {
        console.log(err)
        reject(err)
      }
      resolve(result)
    })
  })
}
// let ok = async () => {
//   RedisUtils.setItem('req.body.uuid', '123 456', 1)
//   let code = await RedisUtils.getItem('req.body.uuid')
//   console.info(code)

//   setTimeout(async () => {
//     code = await RedisUtils.getItem('req.body.uuid')
//     console.info(code)
//   }, 1001)
// }
// ok()
module.exports = RedisUtils
// redis数据库
// rclient.set('token', 'kaola') // 赋值
// rclient.expire('token', 6)