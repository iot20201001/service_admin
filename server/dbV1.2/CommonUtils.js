let CommonUtils = {}
const buildTreeData = (rowData, pid = '0', pNameIsTitle = false, desc = false) => {
  // https://blog.csdn.net/u010377383/article/details/99711717
  return rowData.filter(item => (item.pid + '') === pid + '').map(item => ({
    ...item,
    name: pNameIsTitle ? item.title : item.name,
    children: buildTreeData(rowData, item.id, pNameIsTitle, desc).sort((a, b) => {
      if (desc) {
        return a.sort > b.sort ? -1 : a.sort < b.sort ? 1 : 0
      } else {
        return a.sort < b.sort ? -1 : a.sort > b.sort ? 1 : 0
      }
    })
  })).sort((a, b) => {
    if (desc) {
      return a.sort > b.sort ? -1 : a.sort < b.sort ? 1 : 0
    } else {
      return a.sort < b.sort ? -1 : a.sort > b.sort ? 1 : 0
    }
  })
}
CommonUtils.BuildTreeData = buildTreeData
module.exports = CommonUtils
