const MongoClient = require('mongodb')
const ObjectId = require('mongodb').ObjectId
// https://blog.csdn.net/weixin_43704471/article/details/86590392
const crypto = require('crypto')
// https://www.cnblogs.com/songqingbo/articles/10951569.html
const stringRandom = require('string-random')
const UUID = require('uuid')
// https://www.cnblogs.com/Bideam/p/5760051.html
// 基于nodejs的封装之后,实际我们只需要维持一个单例对象即可，通过配置相关参数实现mongodb的连接池
var MongoDbConfig = {
  dbOptions: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // poolSize: 50,
    // ssl: false,
    // keepAlive: 240,
    // auto_reconnect: true,
    connectTimeoutMS: 300000,
    socketTimeoutMS: 300000,
    promiseLibrary: global.Promise
  },
  // mongodb: //ydsy:ydsy20200215@172.101.108.172:27017/?authSource=ydsy&readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false
  // url: 'mongodb://ydsy:ydsy20200215@172.101.108.172:27017',
  // url: "mongodb://ydsy:ydsy20200215@mongo.ugo.com.cn:27017",
  // dbName: 'ydsy'
  // url: 'mongodb://admin:21232F297A57A5A743894A0E4A801FC3@localhost:27017',
  url: 'mongodb://localhost:27117',
  dbName: 'tenant'
}
var ErrHelper = { // ExecptionHelper
  OK: {
    code: 0,
    msg: '成功！'
  },
  DbConnectError: {
    code: 90001,
    msg: '数据库连接失败！'
  },
  IsExistError: {
    code: 90001,
    msg: '数据已存在！'
  },
  DataVerifyError: {
    code: 90002,
    msg: '数据校验失败！'
  },
  DataTypeVerifyError: {
    code: 90002,
    msg: '数据类型或格式异常！'
  },
  InsertOneError: {
    code: 90101,
    msg: '插入一条数据失败！'
  },
  InsertManyError: {
    code: 90102,
    msg: '插入多条数据失败！'
  },
  DeleteOneError: {
    code: 90111,
    msg: '删除一条数据失败！'
  },
  DeleteManyError: {
    code: 90112,
    msg: '删除多条数据失败！'
  },
  DeleteManyByIdsError: {
    code: 90113,
    msg: '存在删除失败的数据！'
  },
  UpdateOneError: {
    code: 90121,
    msg: '更新一条数据失败！'
  },
  UpdateManyError: {
    code: 90122,
    msg: '更新多条数据失败！'
  },
  FindOneError: {
    code: 90131,
    msg: '查找一条数据失败！'
  },
  FindManyError: {
    code: 90132,
    msg: '查找多条数据失败！'
  },
  FindError: {
    code: 90141,
    msg: '查找失败！'
  },
  DropError: {
    code: 90141,
    msg: '删表失败！'
  },
  AggregateError: {
    code: 90151,
    msg: '聚合查询失败！'
  }
}
class Utils {
  static FormatDate(value, fmt = 'yyyy-MM-dd hh:mm:ss') {
    let getDate = value ? new Date(value) : new Date()
    let o = {
      'M+': getDate.getMonth() + 1,
      'd+': getDate.getDate(),
      'h+': getDate.getHours(),
      'm+': getDate.getMinutes(),
      's+': getDate.getSeconds(),
      'q+': Math.floor((getDate.getMonth() + 3) / 3),
      'S': getDate.getMilliseconds()
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (var k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
      }
    }
    return fmt
  }
  static BuildObjectId(jsonObject) {
    if (jsonObject._id) jsonObject._id = ObjectId(jsonObject._id)
    return jsonObject
  }
  static VerifyObjectId(val1, val2) {
    return val1._id + '' === val2._id + ''
  }
  static GetMD5(str) {
    var md5 = crypto.createHash('md5')
    md5.update(str)
    return md5.digest('hex')
  }
  static VerifyMD5(str, md5Str) {
    var md5 = crypto.createHash('md5')
    md5.update(str)
    return md5.digest('hex') === md5Str
  }
  static GetRandomStr(length = 16) {
    return stringRandom(length)
  }
  static UUID_V1() {
    return UUID.v1()
  }
  static ReturnOK(data, msg = '成功！', code = 0) {
    return {
      code,
      data,
      msg
    }
  }
  static resReturn(data, code, errmsg) {
    code = code || 0
    return {
      code: code,
      msg: errmsg || '成功！',
      data: data
    }
  }

  static resReturnResult({
    data,
    code,
    errmsg
  }) {
    code = code || 0
    return {
      code: code,
      msg: errmsg || '成功！',
      data: data
    }
  }
  static ReturnError(data, errmsg = '失败', code = 1001) {
    return Utils.ReturnOK(data, errmsg, code)
  }
  static ReturnResult({
    data,
    errmsg,
    code
  }) {
    code = code || 0
    return {
      code,
      data,
      msg: msg || errmsg || '成功！'
    }
  }
  static ReturnDbResult(data, err_type) {
    return Utils.ReturnOK(data, err_type.msg, err_type.code)
  }
  static VerifyData(data, targetDataType = Object) {
    // console.warn(data, targetDataType, data instanceof targetDataType)
    // console.info(Object.prototype.toString.call(data) !== `[object ${targetDataType.name}]`)
    // console.warn(Object.prototype.toString.call(data), `[object ${targetDataType.name}]`)
    if (Object.prototype.toString.call(data) !== `[object ${targetDataType.name}]`) {
      // if (!(data instanceof targetDataType)) {
      return Promise.reject(Utils.ReturnDbResult({
          data,
          dataType: Object.prototype.toString.call(data), // typeof data,
          targetDataType: targetDataType.name
        },
        ErrHelper.DataVerifyError
      ))
    } else {
      return Promise.resolve(Utils.ReturnOK(null))
    }
  }
}

class MongoDB {
  static db = {}
  static dbConfig = {}
  constructor(defaultConfig, tenant = 'Tenant_Default') {
    this.tenant = tenant
    defaultConfig.dbName = tenant
    MongoDB.dbConfig[tenant] = defaultConfig
    if (!MongoDB.db[tenant]) {
      this.connect()
    }
  }
  static BuildObjectId(jsonObject) {
    // console.info(jsonObject)
    if (jsonObject._id) {
      jsonObject._id = ObjectId(jsonObject._id)
    }
    // console.info(jsonObject)
    return jsonObject
  }
  connect() {
    return new Promise((resolve, reject) => {
      if (MongoDB.db[this.tenant]) return resolve(MongoDB.db[this.tenant])
      MongoClient.connect(MongoDB.dbConfig[this.tenant].url, MongoDB.dbConfig[this.tenant].dbOptions, (err, db) => {
        if (err) return reject(err)
        if (MongoDB.db[this.tenant]) db.close()
        else {
          MongoDB.db[this.tenant] = {
            db,
            dbo: db.db(MongoDB.dbConfig[this.tenant].dbName)
          }
        }
        resolve(MongoDB.db[this.tenant])
      })
    })
  }
  async insertOne(tableName, jsonObject, isObjectId = true) {
    let r = await Utils.VerifyData(jsonObject)
    if (r.code !== 0) return r
    if (isObjectId) {
      jsonObject = MongoDB.BuildObjectId(jsonObject)
    }
    jsonObject.ctime = Date.now()
    jsonObject.utime = Date.now()
    // jsonObject = {
    //   ...jsonObject,
    //   ctime: Date.now(),
    //   utime: Date.now()
    //   // var moment = require('moment');
    //   // moment.locale('zh-cn');
    //   // https://blog.csdn.net/u011146511/article/details/81146336
    // }
    return new Promise((resolve, reject) => {
      // 优化数据库操作，不加setTimeout，会出现假死的状态，相当于给了mongodb喘息的机会
      // setTimeout(async _ => {
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .insertOne(jsonObject, (err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.InsertOneError
              ))
            } else {
              // res.data.ops [addObject]
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
      // }, 1);
    })
  }
  async insertMany(tableName, jsonArray, isObjectId = true) {
    let r = await Utils.VerifyData(jsonArray, Array)
    if (r.code !== 0) return r
    jsonArray.forEach(jsonObject => {
      if (isObjectId) {
        jsonObject = MongoDB.BuildObjectId(jsonObject)
      }
      jsonObject.ctime = Date.now()
      jsonObject.utime = Date.now()
      // jsonObject = {
      //   ...jsonObject,
      //   ctime: Date.now(),
      //   utime: Date.now()
      // }
    })
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .insertMany(jsonArray, (err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.InsertManyError
              ))
            } else {
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  async deleteOne(tableName, whereJsonObject, isObjectId = true) {
    let r = await Utils.VerifyData(whereJsonObject)
    if (r.code !== 0) return r
    if (isObjectId) {
      whereJsonObject = MongoDB.BuildObjectId(whereJsonObject)
    }
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .deleteOne(whereJsonObject, (err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.DeleteOneError
              ))
            } else {
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  async deleteMany(tableName, whereJsonObject, isObjectId = true) {
    let r = await Utils.VerifyData(whereJsonObject)
    if (r.code !== 0) return r
    try {
      if (isObjectId) {
        whereJsonObject = MongoDB.BuildObjectId(whereJsonObject)
      }
    } catch (err) {
      return Promise.reject(Utils.ReturnDbResult(err, ErrHelper.DataVerifyError))
    }
    return new Promise((resolve, reject) => {
      console.info(whereJsonObject)
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .deleteMany(whereJsonObject, (err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.DeleteManyError
              ))
            } else {
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  async deleteManyByIds(tableName, ids, isObjectId = true) {
    let r = await Utils.VerifyData(ids, Array)
    if (r.code !== 0) return r
    // 此处算是一个比较重要的知识点
    // https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
    return new Promise((resolve, reject) => {
      let promises = []
      ids.forEach((id, i) => {
        promises.push(new Promise(async (resolve, reject) => {
          try {
            let json = {}
            if (isObjectId) {
              json = {
                _id: id
              }
            } else {
              json = {
                id
              }
            }
            let rr = await this.deleteOne(tableName, json)
            resolve({
              id,
              state: rr.code === 0,
              deletedCount: rr.data.deletedCount
            })
          } catch (err) {
            resolve({
              id,
              state: false,
              deletedCount: 0
            })
          }
        }))
      })
      Promise.all(promises).then(res => {
        // 结果会被自动组合为数组
        resolve(res)
      })
    })
  }
  async updateOne(tableName, whereJsonObject, updateJsonObject, isObjectId = true) {
    let r = await Utils.VerifyData(whereJsonObject)
    if (r.code !== 0) return r
    r = await Utils.VerifyData(updateJsonObject)
    if (r.code !== 0) return r
    if (isObjectId) {
      whereJsonObject = MongoDB.BuildObjectId(whereJsonObject)
      updateJsonObject = MongoDB.BuildObjectId(updateJsonObject)
      // 不能更新_id 和 ctime
      delete updateJsonObject._id
      delete updateJsonObject.ctime
    } else {
      // 不能更新_id 和 ctime
      delete updateJsonObject._id
      delete updateJsonObject.ctime
    }
    updateJsonObject.utime = Date.now()
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        updateJsonObject = {
          $set: updateJsonObject
        }
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .updateOne(whereJsonObject, updateJsonObject, (err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.UpdateOneError
              ))
            } else {
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  async updateMany(tableName, whereJsonObject, updateJsonObject, isObjectId = true) {
    let r = await Utils.VerifyData(whereJsonObject)
    if (r.code !== 0) return r
    r = await Utils.VerifyData(updateJsonObject)
    if (r.code !== 0) return r
    if (isObjectId) {
      whereJsonObject = MongoDB.BuildObjectId(whereJsonObject)
      updateJsonObject = MongoDB.BuildObjectId(updateJsonObject)
      // 不能更新_id 和 ctime
      delete updateJsonObject._id
      delete updateJsonObject.ctime
    } else {
      // 不能更新_id 和 ctime
      delete updateJsonObject._id
      delete updateJsonObject.ctime
    }
    updateJsonObject.utime = Date.now()
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        updateJsonObject = {
          $set: updateJsonObject
        }
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .updateMany(whereJsonObject, updateJsonObject, (err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.UpdateOneError
              ))
            } else {
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  async findByIds(tableName, ids, projection = null, isObjectId = true) {
    let r = await Utils.VerifyData(ids, Array)
    if (r.code !== 0) return r
    if (isObjectId) {
      let tt = []
      ids.forEach(e => {
        tt.push(ObjectId(e))
      })
      ids = [...tt]
    }
    return new Promise((resolve, reject) => {
      this.connect().then(async _ => {
        try {
          let res = null
          let find = {
            id: {
              $in: ids
            }
          }
          if (isObjectId) {
            find = {
              _id: {
                $in: ids
              }
            }
          }
          let a = MongoDB.db[this.tenant].dbo
            .collection(tableName)
          if (projection === null) {
            res = await a.find(find).toArray()
            // res = await a.find({$or:[{_id:{$in:ids}},{id:{$in:ids}}]})
          } else {
            res = await a.findOne(find, {
              projection
            }).toArray()
          }
          resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
        } catch (err) {
          reject(Utils.ReturnDbResult(
            err,
            ErrHelper.FindOneError
          ))
        }
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  async findOne(tableName, whereJsonObject, projection = null, isObjectId = true) {
    let r = await Utils.VerifyData(whereJsonObject)
    if (r.code !== 0) return r
    if (isObjectId) {
      whereJsonObject = MongoDB.BuildObjectId(whereJsonObject)
    }
    return new Promise((resolve, reject) => {
      this.connect().then(async _ => {
        try {
          let res = null
          let a = MongoDB.db[this.tenant].dbo
            .collection(tableName)
          if (projection === null) {
            res = await a.findOne(whereJsonObject)
          } else {
            res = await a.findOne(whereJsonObject, {
              projection
            })
          }
          resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
        } catch (err) {
          reject(Utils.ReturnDbResult(
            err,
            ErrHelper.FindOneError
          ))
        }
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  async aggregate(tableName, options, isObjectId = true) {
    let r = await Utils.VerifyData(options, Array)
    if (r.code !== 0) return r
    return new Promise((resolve, reject) => {
      this.connect().then(async _ => {
        try {
          let res = null
          res = await MongoDB.db[this.tenant].dbo
            .collection(tableName)
            .aggregate(options)
          resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
        } catch (err) {
          reject(Utils.ReturnDbResult(
            err,
            ErrHelper.AggregateError
          ))
        }
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  selectObject = null
  select(selectStr = 'username,password,age') {
    if (selectStr instanceof Object) {
      this.selectObject = selectStr
      return this
    }
    var arr = selectStr.split(',')
    let result = {}
    if (!arr.includes('_id')) {
      eval(`result._id=0`)
    }
    arr.forEach(e => {
      eval(`result.${e}=1`)
    })
    this.selectObject = result
    // console.warn(result)
    return this
  }
  whereObject = null
  where(whereObject) {
    this.whereObject = whereObject || {}
    return this
  }
  sortObject = null
  sort(sortObject) {
    this.sortObject = sortObject || {}
    return this
  }
  _skip = 0
  skip(skip) {
    this._skip = skip || 0
    return this
  }
  _limit = 1000
  limit(limit) {
    this._limit = limit || 1000
    return this
  }
  resetToArrayStatus() {
    this.selectObject = null
    this.whereObject = null
    this.sortObject = null
    this._skip = 0
    this._limit = 1000
    this.includeObj = null
  }
  async ToArray(tableName, includeArray, whereObject, selectObject, sortObject, _skip, _limit, showTotal = false, isObjectId = true) {
    this.includeObj = includeArray || null
    this.whereObject = whereObject || null
    this.selectObject = selectObject || null
    this.sortObject = sortObject || null
    this._skip = _skip || 0
    this._limit = _limit || 1000
    return this.toArray(tableName, showTotal, isObjectId)
  }
  async toArray(tableName, showTotal = false, isObjectId = true) {
    return new Promise((resolve, reject) => {
      this.connect().then(async _ => {
        // http://mongodb.github.io/node-mongodb-native/3.2/api/Collection.html#find
        // let options = {
        //     projection: this.selectObject,
        //     sort: this.sortObject,
        //     skip: this._skip,
        //     limit: this._limit,

        // }
        // this.selectObject = { _id: 1, id: 1 }
        // this.sortObject = { id: 1 }
        if (this.whereObject && isObjectId) {
          this.whereObject = Utils.BuildObjectId(this.whereObject)
        }
        let total = null
        // showTotal = false
        if (showTotal === true) {
          // console.info(showTotal)
          total = await MongoDB.db[this.tenant].dbo
            .collection(tableName)
            .find(this.whereObject)
            .count()
        }
        // console.info(total)
        // console.info(this.whereObject)
        let a = MongoDB.db[this.tenant].dbo
          .collection(tableName)
        if (this.includeObj && this.selectObject && this.sortObject) {
          this.includeObj.push({
            $match: {
              ...this.whereObject
            }
          })
          a = a.aggregate(this.includeObj)
            .project(this.selectObject)
            .sort(this.sortObject)
        } else {
          // console.info(this.whereObject)
          a = a.find(this.whereObject)
          if (this.selectObject) {
            a = a.project(this.selectObject)
          }
          if (this.sortObject) {
            a = a.sort(this.sortObject)
          } // 排序 //按某个字段升序(1)降序(-1)
        }
        a.skip(this._skip)
          .limit(this._limit)
          .toArray((err, res) => {
            this.resetToArrayStatus()
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.FindManyError
              ))
            } else {
              resolve({
                code: 0,
                data: res,
                total,
                msg: ErrHelper.OK.msg
              })
            }
          })
      }).catch(err => {
        this.resetToArrayStatus()
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  includeObj = null
  // [聚合查询]https://www.runoob.com/mongodb/mongodb-aggregate.html
  leftJoin(object, localField, foreignField, asField) {
    if (!this.includeObj) {
      this.includeObj = []
    }
    this.includeObj.push({
      $lookup: {
        from: object.name || object, // 'products',            // 右集合
        localField, // : 'product_id',    // 左集合 join 字段
        foreignField, // : 'orderid',         // 右集合 join 字段
        as: asField // : 'orderdetails'           // 新生成字段（类型array）
      }
    })
    return this
  }
  include(object, localField, foreignField, asField) {
    return this.leftJoin(object, localField, foreignField, asField)
  }
  // async find(tableName, whereJsonObject, projection = { _id: 0, json: 1 }, options = {}, isObjectId = true) {
  //     if (isObjectId)
  //         whereJsonObject = MongoDB.BuildObjectId(whereJsonObject)
  //     return new Promise((resolve, reject) => {
  //         this.connect().then(_ => {
  //             MongoDB.db[this.tenant].dbo
  //                 .collection(tableName)
  //                 .find(whereJsonObject, { projection })
  //                 //.find(whereJsonObject)
  //                 //.select(options.fields || '')
  //                 .sort(options.sort || {})//排序 //按某个字段升序(1)降序(-1)
  //                 .skip(options.skip || 0)
  //                 .limit(options.limit || 1)
  //                 .toArray((err, res) => {
  //                     if (err)
  //                         reject(Utils.ReturnDbResult(
  //                             err,
  //                             ErrHelper.FindManyError
  //                         ))
  //                     resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
  //                 })
  //         }).catch(err => {
  //             reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
  //         })
  //     })

  //     return new Promise((resolve, reject) => {
  //         this.connect().then(async _ => {
  //             try {
  //                 let res = await MongoDB.db[this.tenant].dbo
  //                     .collection(tableName)
  //                     .findOne(whereJsonObject, projection)
  //                 resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
  //             } catch (err) {
  //                 reject(Utils.ReturnDbResult(
  //                     err,
  //                     ErrHelper.DeleteManyError
  //                 ))
  //             }
  //         })
  //     }).catch(err => {
  //         reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
  //     })
  // }
  drop(tableName) {
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .drop((err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.DropError
              ))
            } else {
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  dropDatabase() {
    console.info('aaaaaaaaaaaaaaaaaaaaa')
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo.dropDatabase((err, res) => {
          if (err) {
            reject(Utils.ReturnDbResult(
              err,
              ErrHelper.DropError
            ))
          } else {
            resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
          }
        })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
  // http://mongodb.github.io/node-mongodb-native/3.5/tutorials/projections/
  // 待完善，不可用
  aggregate(tableName, options) {
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .aggregate(options, (err, cursor) => {
            if (err) {
              reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
            } else {
              cursor.toArray((err, documents) => {
                if (err) {
                  reject(Utils.ReturnDbResult(
                    err,
                    ErrHelper.FindError
                  ))
                } else {
                  resolve({
                    code: 0,
                    data: documents,
                    msg: ErrHelper.OK.msg
                  })
                }
              })
            }
          })
      })
    })
  }
  // 待完善，不可用
  aggregate_test(tableName) {
    return new Promise((resolve, reject) => {
      this.connect().then(_ => {
        let res = MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .aggregate([{
            $group: {
              _id: '$pid',
              num_tutorial: {
                $sum: 1
              }
            }
          }], (err, cursor) => {
            // console.info('5555555')
          })
        // resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
    })
  }
}

// this.db = MongoDB.getInstance(MongoDbConfig);
{
  // const options = [{$match: { name: 'Dan' }},{$graphLookup:{from: "employees",startWith:"$reportsTo",connectFromField:"reportsTo",connectToField:"name",as:"parent"}}]
  // const options = [{$match: { name: 'Dan' }},{$graphLookup:{from: "employees",startWith:"$pid",connectFromField:"pid",connectToField:"id",as:"parent"}}]
  // const options = [{$match: { name: 'Dev' }},{$:{from: "employees",startWith:"$id",connectFromField:"id",connectToField:"pid",as:"children"}}]
  // let options = [{ $match: { name: 'Dev' } },
  // {
  //     $graphLookup: {
  //         from: "employees",
  //         startWith: "$id",
  //         connectFromField: "id",
  //         connectToField: "pid",
  //         as: "children",
  //         maxDepth: 4,
  //         depthField: "level"
  //     }
  // },
  // {
  //     $unwind: "$children"
  // },
  // {
  //     $sort: { "children.level": -1 }
  // },
  // {
  //     $group: {
  //         _id: "$id",
  //         children: { $push: "$children" }
  //     }
  // },
  // {
  //     $addFields: {
  //         children: {
  //             $reduce: {
  //                 input: "$children",
  //                 initialValue: {
  //                     currentLevel: -1,
  //                     currentLevelProjects: [],
  //                     previousLevelProjects: []
  //                 },
  //                 in: {
  //                     $let: {
  //                         vars: {
  //                             prev: {
  //                                 $cond: [
  //                                     { $eq: ["$$value.currentLevel", "$$this.level"] },
  //                                     "$$value.previousLevelProjects",
  //                                     "$$value.currentLevelProjects"
  //                                 ]
  //                             },
  //                             current: {
  //                                 $cond: [
  //                                     { $eq: ["$$value.currentLevel", "$$this.level"] },
  //                                     "$$value.currentLevelProjects",
  //                                     []
  //                                 ]
  //                             }
  //                         },
  //                         in: {
  //                             currentLevel: "$$this.level",
  //                             previousLevelProjects: "$$prev",
  //                             currentLevelProjects: {
  //                                 $concatArrays: [
  //                                     "$$current",
  //                                     [
  //                                         {
  //                                             $mergeObjects: [
  //                                                 "$$this",
  //                                                 { children: { $filter: { input: "$$prev", as: "e", cond: { $eq: ["$$e.pid", "$$this.id"] } } } }
  //                                             ]
  //                                         }
  //                                     ]
  //                                 ]
  //                             }
  //                         }
  //                     }
  //                 }
  //             }
  //         }
  //     }
  // },
  // {
  //     $addFields: { children: "$children.currentLevelProjects" }
  // },
  // {
  //     $match: {
  //         id: 1
  //     }
  // }
  // ]
} {
  // options = [
  //     // get user1
  //     { $match: { name: 'Dev' } },
  //     // get user1's friend with username of 'user2'
  //     {
  //         $graphLookup: {
  //             from: 'employees',
  //             startWith: '$id',
  //             connectFromField: 'id',
  //             connectToField: 'pid',
  //             as: 'children',
  //             maxDepth: 2,
  //             // restrictSearchWithMatch: { name: 'Dev' }
  //         }
  //     },
  //     { $unwind: '$children' },
  //     // only return the overlap of friends between user1 and user2
  //     { $project: { children: { $setIntersection: ['$id', '$children.pid'] }, _id: 1 } }
  // ]
} {
  // let options = [
  //     [
  //         { $match: { name: 'Eliot' } },
  //         {
  //             $graphLookup: {
  //                 from: "employees",
  //                 startWith: "$id",
  //                 connectFromField: "id",
  //                 connectToField: "pid",
  //                 as: "children",
  //                 maxDepth: 2,
  //                 depthField: 'level',
  //                 restrictSearchWithMatch: { level: 2 }
  //             }
  //         }
  //     ],
  //     [
  //         { $match: { name: 'Eliot' } },
  //         {
  //             $lookup: {
  //                 from: "employees",
  //                 localField: "id",
  //                 foreignField: "pid",
  //                 as: "children",
  //             }
  //         }
  //     ],
  //     [
  //         {
  //             $graphLookup: {
  //                 from: "employees",
  //                 startWith: "$id",
  //                 connectFromField: "id",
  //                 connectToField: "parent",
  //                 as: "children",
  //                 maxDepth: 4,
  //                 depthField: "level"
  //             }
  //         },
  //         {
  //             $unwind: "$children"
  //         },
  //         {
  //             $sort: { "children.level": -1 }
  //         },
  //         {
  //             $group: {
  //                 _id: "$_id",
  //                 children: { $push: "$children" }
  //             }
  //         },
  //         {
  //             $addFields: {
  //                 children: {
  //                     $reduce: {
  //                         input: "$children",
  //                         initialValue: {
  //                             currentLevel: -1,
  //                             currentLevelProjects: [],
  //                             previousLevelProjects: []
  //                         },
  //                         in: {
  //                             $let: {
  //                                 vars: {
  //                                     prev: {
  //                                         $cond: [
  //                                             { $eq: ["$$value.currentLevel", "$$this.level"] },
  //                                             "$$value.previousLevelProjects",
  //                                             "$$value.currentLevelProjects"
  //                                         ]
  //                                     },
  //                                     current: {
  //                                         $cond: [
  //                                             { $eq: ["$$value.currentLevel", "$$this.level"] },
  //                                             "$$value.currentLevelProjects",
  //                                             []
  //                                         ]
  //                                     }
  //                                 },
  //                                 in: {
  //                                     currentLevel: "$$this.level",
  //                                     previousLevelProjects: "$$prev",
  //                                     currentLevelProjects: {
  //                                         $concatArrays: [
  //                                             "$$current",
  //                                             [
  //                                                 {
  //                                                     $mergeObjects: [
  //                                                         "$$this",
  //                                                         { children: { $filter: { input: "$$prev", as: "e", cond: { $eq: ["$$e.parent", "$$this._id"] } } } }
  //                                                     ]
  //                                                 }
  //                                             ]
  //                                         ]
  //                                     }
  //                                 }
  //                             }
  //                         }
  //                     }
  //                 }
  //             }
  //         },
  //         {
  //             $addFields: { children: "$children.currentLevelProjects" }
  //         },
  //         {
  //             $match: {
  //                 id: 2
  //             }
  //         }
  //     ]
  // ]
  // this.db.aggregate('employees', options[2]).then(db => {
  //     // console.info(db)
  //     console.info(JSON.stringify(db.data))
  //     // db.data
  //     // .$match({})
  //     // .$group({ _id: '$pid', num_tutorial: { $sum: 1 } })
  //     // .exec((err, cursor) => {
  //     //     if (err)
  //     //         console.info(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
  //     //     else {
  //     //         cursor.toArray((err, documents) => {
  //     //             if (err)
  //     //                 console.info(Utils.ReturnDbResult(
  //     //                     err,
  //     //                     ErrHelper.FindError
  //     //                 ))
  //     //             else {
  //     //                 console.info({
  //     //                     code: 0,
  //     //                     data: documents,
  //     //                     msg: ErrHelper.OK.msg
  //     //                 })
  //     //             }
  //     //         });
  //     //     }
  //     // })
  // }).catch(err => {
  //     console.error(err)
  // })
} {
  // this.db = MongoDB.getInstance(MongoDbConfig);
  // const options = [
  //     [
  //         { '$match': { id: 100010 } },
  //         { $group: { _id: "$pid", num_tutorial: { $sum: 1 } } },
  //         { $sort: { num_tutorial: -1, _id: 1 } },
  //     ],
  //     [{ $project: { id: 1 } }]
  // ]
  // const exe = 0

  // this.db.aggregate('Region', options[exe]).then(db => {
  //     console.info(db)
  //     // db.data
  //     // .$match({})
  //     // .$group({ _id: '$pid', num_tutorial: { $sum: 1 } })
  //     // .exec((err, cursor) => {
  //     //     if (err)
  //     //         console.info(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
  //     //     else {
  //     //         cursor.toArray((err, documents) => {
  //     //             if (err)
  //     //                 console.info(Utils.ReturnDbResult(
  //     //                     err,
  //     //                     ErrHelper.FindError
  //     //                 ))
  //     //             else {
  //     //                 console.info({
  //     //                     code: 0,
  //     //                     data: documents,
  //     //                     msg: ErrHelper.OK.msg
  //     //                 })
  //     //             }
  //     //         });
  //     //     }
  //     // })

  // }).catch(err => {
  //     console.error(err)
  // })

  // this.db = new MongoDB(MongoDbConfig);
  // let ok = _ => {
  //     this.db.findOne('Region', { id: 'aa' })
  //         .then(res => {
  //             // console.info(res)
  //         }).catch(err => {
  //             // console.error(err)
  //         })
  // }
  // let count = 0
  // let arr = []
  // setInterval(() => {
  //     for (var i = 0; i < 10; i++)
  //         ok()
  //     let tt = process.memoryUsage()
  //     Object.keys(tt).forEach(x => {
  //         tt[x] = (tt[x] / 1024 / 1024).toFixed(2) + 'MB'
  //     })
  //     console.info(++count, '进程占用内存：', tt.rss)
  // }, 50);
  // // 需要写个where解释器
  // this.db //leftJoin===include
  //     .leftJoin('products', 'product_id', 'orderid', 'oks')
  //     .include('extInfos', 'product_id', 'orderid', 'exts')
  //     // .where({ _id: 1 })//.where({ "i": { $lt: 50 }, $or: [{ "i": { $lt: 43 } }] })
  //     .select('_id,oks,exts')
  //     .sort({ i: -1 })
  //     .skip(0)
  //     .limit(2)
  //     .toArray('orders').then(res => {
  //         console.info(JSON.stringify(res))
  //     }).catch(err => {
  //         console.error(err)
  //     })
  // // this.db.drop('tableName').then(res => {
  // //     console.info(res)
  // // }).catch(err => {
  // //     console.error(err)
  // // })
  // // for (var i = 1; i < 1000; i++) {
  // //     // setTimeout(_ => {
  // //     this.db.insertOne('tableName', { json: 'JSONA' + i, i, i2: i * 2, ii: i * i })
  // //         .then(res => {
  // //             console.info(res.data.ops[0]._id)
  // //         }).catch(err => {
  // //             console.error(err)
  // //         })
  // //     // }, 500)
  // // }
  // // this.db.find('tableName', { json: 'JSON3' })
  // //     .then(res => {
  // //         console.info(res)
  // //     }).catch(err => {
  // //         console.error(err)
  // //     })
  // // new MongoDB(url).findObject("user", {
  // //     "username": "296565890",
  // //     "email": "296565890@qq.com",
  // // }).then(res => {
  // //     console.info(res)
  // // }).catch(err => {
  // //     console.err(err)
  // // })
  // // new MongoDB(url).save("user", [{
  // //     _id: '5e33ed64a85d310cb8af4707',
  // //     username: 'AAAAAAAAA',
  // //     password: 'Straaaaaaing',
  // // }, {
  // //     _id: '5e33ed64a85d310cb8af4707',
  // //     username: 'BBBBBBBBBB',
  // //     password: 'Straaaaaaing',
  // // }]).then(res => {
  // //     console.info(res)
  // // }).catch(err => {
  // //     console.info(err)
  // // })
  // // MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
  // //     if (err) throw err;
  // //     console.log("数据库已创建!");
  // //     db.close();
  // // });
}

module.exports = {
  Utils,
  MongoDbConfig,
  ErrHelper,
  MongoDB
}