var {
  Utils,
  MongoDbConfig,
  ErrHelper,
  MongoDB
} = require('./MongoDB')
var ObjectId = require('mongodb').ObjectId

class Entity {
  db = null
  constructor(tenant = '', tableName = null) {
    let config = {
      ...MongoDbConfig
    }
    if (tenant !== '') config.dbName = tenant
    this.db = new MongoDB(config, tenant)
    this.tableName = tableName || this.constructor.name
  }
  AddEntity(json) {
    return new Promise((resolve, reject) => {
      this.db.findOne(this.tableName, json).then(r => {
        if (!r.data) {
          this.db
            .insertOne(this.tableName, json)
            .then(res => {
              resolve({
                code: res.code,
                data: res.data.ops[0],
                msg: res.msg
              })
            }).catch(err => {
              reject(err)
            })
        } else {
          reject(Utils.ReturnDbResult(null, ErrHelper.IsExistError))
        }
      }).catch(err => {
        reject(err)
      })
    })
  }
  AddManyEntity(jsonArray) {
    return new Promise((resolve, reject) => {
      this.db.insertMany(this.tableName, jsonArray)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }
  DeleteEntity(json, isObjectId = true) {
    return new Promise((resolve, reject) => {
      this.db.deleteOne(this.tableName, json, isObjectId)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }
  DeleteManyEntity(json, isObjectId = true) {
    return new Promise((resolve, reject) => {
      this.db.deleteMany(this.tableName, json, isObjectId)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }
  DeleteManyByIds(ids, isObjectId = false) {
    return new Promise((resolve, reject) => {
      this.db.deleteManyByIds(this.tableName, ids, isObjectId)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }
  UpdateEntity(oldEntity, newEntity) {
    return new Promise((resolve, reject) => {
      this.db.updateOne(this.tableName, oldEntity, newEntity)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }
  UpdateManyEntity(oldEntity, newEntity) {
    return new Promise((resolve, reject) => {
      this.db.updateMany(this.tableName, oldEntity, newEntity)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }
  drop() {
    return new Promise((resolve, reject) => {
      this.db.drop(this.tableName)
        .then(res => {
          resolve({
            code: 0,
            msg: '删除成功',
            data: res
          })
        }).catch(err => {
          // console.info(err)
          reject({
            code: err.ok,
            msg: err.errmsg,
            data: err
          })
        })
    })
  }
  dropDatabase() {
    return new Promise((resolve, reject) => {
      this.db.dropDatabase()
        .then(res => {
          resolve({
            code: 0,
            msg: '删除成功',
            data: res
          })
        }).catch(err => {
          // console.info(err)
          reject({
            code: err.ok,
            msg: err.errmsg,
            data: err
          })
        })
    })
  }
  _selectObject = null
  select(selectStr = 'username,password,age') {
    if (selectStr instanceof Object) {
      this._selectObject = {
        ...this._selectObject,
        ...selectStr
      }
      return this
    }
    var arr = selectStr.trim().split(',')
    let result = {}
    if (!arr.includes('_id')) {
      eval(`result._id=0`)
    }
    arr.forEach(e => {
      if (e) {
        eval(`result.${e}=1`)
      }
    })
    this._selectObject = {
      ...this._selectObject,
      ...result
    }
    // console.info(this._selectObject)
    return this
  }
  noSelect(selectStr = 'username,password,age') {
    if (selectStr instanceof Object) {
      this._selectObject = {
        ...this._selectObject,
        ...selectStr
      }
      return this
    }
    var arr = selectStr.trim().split(',')
    let result = {}
    arr.forEach(e => {
      if (e) {
        eval(`result.${e}=0`)
      }
    })
    this._selectObject = {
      ...this._selectObject,
      ...result
    }
    console.info(this._selectObject)
    return this
  }
  _whereObject = null
  where(_whereObject) {
    this._whereObject = _whereObject || {}
    return this
  }
  _sortObject = null
  sort(_sortObject) {
    this._sortObject = _sortObject || {}
    return this
  }
  _skip = 0
  skip(skip) {
    skip = parseInt(skip)
    this._skip = isNaN(skip) ? 0 : skip
    return this
  }
  _limit = 10000
  limit(limit) {
    limit = parseInt(limit)
    this._limit = isNaN(limit) ? 10000 : limit
    return this
  }
  resetToArrayStatus() {
    this._includeObj = null
    this._selectObject = null
    this._whereObject = null
    this._sortObject = null
    this._skip = 0
    this._limit = 10000
  }
  toArray(showTotal = true) {
    return new Promise((resolve, reject) => {
      this.db
        .ToArray(this.tableName, this._includeObj, this._whereObject, this._selectObject, this._sortObject, this._skip, this._limit, showTotal)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }
  _includeObj = null
  // [聚合查询]https://www.runoob.com/mongodb/mongodb-aggregate.html
  leftJoin(object, localField, foreignField, asField) {
    if (!this._includeObj) {
      this._includeObj = []
    }
    this._includeObj.push({
        $lookup: {
          from: object.name || object, // 'products',            // 右集合
          localField, // : 'product_id',    // 左集合 join 字段
          foreignField, // : 'orderid',         // 右集合 join 字段
          as: asField // : 'orderdetails'           // 新生成字段（类型array）
        }
        // $project: {
        //     _id: 1
        // }
      }
      // { $unwind: "$Products" },
      // { $unwind: "$Products" },//数据打散
      // { $project: { _id: 1}}
      // { $match: { order_id: '123456789012345678901234' } }
    )
    // console.info(this._includeObj)
    return this
  }
  include(object, localField = null, foreignField = null, asField = null) {
    return this.leftJoin(object.tableName, localField || '_id',
      foreignField || this.tableName + '_id',
      asField || object.tableName)
  }
  FindOneEntity(json) {
    return new Promise((resolve, reject) => {
      this.db.findOne(this.tableName, json).then(r => {
        resolve(r)
      }).catch(err => {
        reject(err)
      })
    })
  }
  FindEntityByIds(ids, projection = null, isObjectId = false) {
    return new Promise((resolve, reject) => {
      this.db.findByIds(this.tableName, ids, projection, isObjectId).then(r => {
        resolve(r)
      }).catch(err => {
        reject(err)
      })
    })
  }
  async SaveEntity(json, findJson = null) {
    let opt = this.where(findJson || json).limit(1).skip(0)
    let result = await opt.toArray()
    // console.info(result)
    if (result.data.length === 0) {
      result = await this.AddEntity(json)
      return result.code === 0
    } else {
      let oVal = result.data[0]
      json = {
        ...oVal,
        ...json
      }
      // console.info(oVal)
      // console.info(json)
      result = await this.UpdateEntity(oVal, json)
      return result.code === 0
    }
  }
  Aggregate(options) {
    return new Promise((resolve, reject) => {
      this.db.aggregate(this.tableName, options)
        .then(r => {
          resolve(r)
        }).catch(err => {
          reject(err)
        })
    })
  }
}
let debug = false
if (debug) {
  // class Orders extends Entity {
  // }
  // class Products extends Entity {
  // }
  // class Extends extends Entity {
  // }
  { // 插入单条数据
    // for (let i = 1; i < 100; i++)
    //     new Orders().AddEntity({ id: i, name: 'admin' + i })
    //         .then(res => {
    //             // 成功
    //             console.info(res)
    //         }).catch(err => {
    //             // 失败
    //             console.error(err)
    //         })
    // new Orders().drop()
  } { // 插入多条数据
    // new Orders().AddManyEntity([{ _id: '5e43d8480573ab513ce09c46', username: 'admin' }, { username: 'aaaaaaaaaaaaaa' }])
    //     .then(res => {
    //         // 全部成功
    //         console.info(res)
    //     }).catch(err => {
    //         // 全部失败
    //         console.error(err)
    //     })
  } { // 删除单条数据
    // new Orders().DeleteEntity({ username: 'admin' })
    //     .then(res => {
    //         // 删除成功【原本不存在也是删除成功】
    //         console.info(res)
    //     }).catch(err => {
    //         // 删除失败
    //         console.error(err)
    //     })
  } { // 删除多条数据
    // new Orders().DeleteManyEntity({ username: 'admin' })
    //     .then(res => {
    //         // 删除成功【原本不存在也是删除成功】
    //         console.info(res)
    //     }).catch(err => {
    //         // 删除失败
    //         console.error(err)
    //     })
  } { // 删除ids
    // new Orders().DeleteManyByIds(['5e43e14031313628ecea21af'])
    //     .then(res => {
    //         // 删除成功【原本不存在也是删除成功】
    //         // 删除成功 [ { id: '5e43e14031313628ecea21af', state: true, deletedCount: 1 } ]
    //         // 不存在   [ { id: '5e43e14031313628ecea21af', state: true, deletedCount: 0 } ]
    //         console.info(res)
    //     }).catch(err => {
    //         // 删除失败
    //         console.error(err)
    //     })
  } { // 更新单个/多个实体
    // 不需要更新的字段一定要删除 delete obj.pro
    // new Orders().UpdateEntity
    // new Orders().UpdateManyEntity
    // new Orders().UpdateManyEntity({ _id: '5e43e14031313628ecea2001' }, { name: 'p' })
    //     .then(res => {
    //         // res.data
    //         // modifiedCount: 0, 修改数量
    //         // matchedCount: 0 匹配数量
    //         console.info(res)
    //     }).catch(err => {
    //         // 删除失败
    //         console.error(err)
    //     })
  } { // 查询单个实体
    // class Region extends Entity { }
    // new Region().FindOneEntity({})
    //     .then(res => {
    //         console.info(res)
    //     }).catch(err => {
    //         console.error(err)
    //     })
  } { // 查询
    {
      // new Orders().drop()
      // new Products().drop()
      // new Extends().drop()
      // new Orders().AddEntity({ _id: '123456789012345678901234', name: '我的订单' })
      //     .then(res => {
      //         // 成功
      //         console.info(res)
      //     }).catch(err => {
      //         // 失败
      //         console.error(err)
      //     })
      // for (let i = 1; i < 3; i++)
      //     new Products().AddEntity({ id: i, product_name: '我的商品' + i, order_id: ObjectId('123456789012345678901234') })
      //         .then(res => {
      //             // 成功
      //             console.info(res)
      //         }).catch(err => {
      //             // 失败
      //             console.error(err)
      //         })
      // for (let i = 2; i < 5; i++)
      //     new Extends().AddEntity({ id: i, extend_name: '扩展信息' + i, order_id: ObjectId('123456789012345678901234') })
      //         .then(res => {
      //             // 成功
      //             console.info(res)
      //         }).catch(err => {
      //             // 失败
      //             console.error(err)
      //         })
    }
    // 管道的概念
    // [聚合查询]https://www.runoob.com/mongodb/mongodb-aggregate.html
    // https://www.cnblogs.com/jackson-yqj/p/10291505.html
    // new Orders()
    //     .include(new Products(), '_id', 'order_id', 'Products')
    //     .include(new Extends(), '_id', 'order_id', 'Extends')
    //     // .where({ _id: '123456789012345678901235' })//.where({ "i": { $lt: 50 }, $or: [{ "i": { $lt: 43 } }] })
    //     // select noSelect 只能二选一
    //     .select('_id, Extends,  Products')
    //     //  .noSelect('_id')
    //     .sort({ _id: 1 }) // include select sort
    //     .skip(1)
    //     .limit(1)
    //     .toArray().then(res => {
    //         console.info(JSON.stringify(res))
    //     }).catch(err => {
    //         console.error(err)
    //     })
  } { // 【// 待完善，不可用】聚合查询
    // MongoDB Node.js Driver
    // http://mongodb.github.io/node-mongodb-native/
    // class Region extends Entity { }
    // const options = [
    //     [{ $group: { _id: "$pid", num_tutorial: { $sum: 1 } } }],
    //     [{ $project: { id: 1 } }]
    // ]
    // const exe = 0
    // console.info(options[exe])
    // new Region().Aggregate(options[exe])
    //     .then(res => {
    //         console.info(res)
    //     }).catch(err => {
    //         console.error(err)
    //     })
  }
}
// // 应用
// // 通过此表判断相应App是否支持接入IM服务
// class AdminUser extends Entity {
//     async Verify(json) {
//         let result = await this.FindEntity({
//             username: json.username
//         })
//         // 如果未出错并且找到数据数量为0
//         if (result.code === 0 && result.data.length === 0) return false
//         // 如果找到
//         let findUser = result.data[0]
//         if (findUser.password === Utils.getMD5(json.password + findUser.passsalt)) {
//             return true;
//         } else
//             return false
//     }
// }
// class App extends Entity {
//     async Verify(json) {
//         let result = await this.FindEntity({
//             appId: json.appId
//         })
//         if (result.code === 0 && result.data.length === 0) return false;
//         json.appSecret = Utils.getMD5(json.appSecret + result.data[0].passsalt)
//         result = await this.FindEntity({
//             appId: json.appId,
//             appSecret: json.appSecret
//         })
//         if (result.data.length === 0) return false;
//         return true;
//     }
//     // appId: 'ios',
//     // appSecret: Utils.getMD5('a' + passsalt),
//     // passsalt
// }
// // 用户
// // 记录用户信息uid唯一
// class User extends Entity {
//     async Verify(json) {
//         let result = await this.FindEntity({
//             username: json.username
//         })
//         if (result.code === 0 && result.data.length === 0) return null;
//         json.password = Utils.getMD5(json.password + result.data[0].passsalt)
//         if (result.data[0].password !== json.password) return null;
//         return result;
//     }
//     // uid: '唯一',
//     // nickname: '昵称',
//     // avatar: '头像' base64 64×64
//     // ctime
//     // utime
//     // invalid
// }
// // 用户上传的文件
// class UserFiles extends Entity {
// }
// // 用户上传的文件
// class FileOption extends Entity {
// }
// class Site extends Entity {
// }
// class Service extends Entity {
// }
// // 用户登录日志
// class UserLogins extends Entity {
//     // uid: '唯一',
//     // machineCode: '唯一',
//     // Websocket: 连接对象,
//     // ctime
//     // utime
//     async SaveEntity(json) {
//         if (!json.machineCode) {
//             json.machineCode = UUID.v1()
//         }
//         let result = await this.FindEntity({
//             uid: json.uid,
//             machineCode: json.machineCode
//         })
//         if (result.data.length === 0) {
//             result = await this.AddEntity(json)
//             return result.code === 0 ? true : false;
//         } else {
//             let oVal = result.data[0]
//             json = {
//                 ...oVal,
//                 ...json
//             }
//             result = await this.UpdateEntity(oVal, json)
//             return result.code === 0 ? true : false;
//         }
//     }
// }

// //用户会话
// class User_C2C extends Entity {
//     // 用户A uid
//     // 用户B uid
// }
// //用户群组会话
// class User_C2G extends Entity {
// }
// // 消息
// class Message extends Entity {
// }
// class Token extends Entity {
// }

class Orders extends Entity {}
class Products extends Entity {}
class Extends extends Entity {}

class Region extends Entity {}
module.exports = {
  Utils,
  MongoDbConfig,
  ErrHelper,
  ObjectId,
  Entity,
  Orders,
  Products,
  Extends,
  Region
  // MongoDB,
  // User,
  // App,
  // Message,
  // UserLogins,
  // UserFiles,
  // FileOption,
  // Site,
  // Service,
  // Token,
  // AdminUser
}