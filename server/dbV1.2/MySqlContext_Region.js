var mysql = require('mysql')
var connection = mysql.createConnection({
  host: '127.0.0.1',
  user: 'root',
  password: 'Aa123456',
  database: 'chaoyue-cloud-tenant',
  timezone: 'Asia/Shanghai',
  ssl: false,
  supportBigNumbers: true
})
// https://www.cnblogs.com/eaysun/p/5534554.html
// https://blog.csdn.net/joyvonlee/article/details/104873916
// 1、use mysql
// 2、alter user 'root'@'localhost' identified with mysql_native_password by 'Aa123456';
// 3、flush privileges;
connection.connect()
const MySql = {}
// let tables = ['sys_dept']
var {
  Entity
} = require('./MessageEntitys')
class SysRegion extends Entity {}
class SysDictData extends Entity {}
MySql.query = (sql) => {
  connection.query('SELECT * from sys_region', function (error, results, fields) {
    if (error) throw error
    // console.log('The solution is: ', JSON.stringify(results[0]))
    results.forEach(x => {
      // console.info(typeof x.id, typeof Long(x.id))
      x.id += ''
      x.pid += ''
      if (x.pid !== '0') {
        x.parent_name = results.find(y => y.id === x.pid).name
      }
      // x.pids = Long(x.pids)
      x.creator = ''
      x.updater = ''
      delete x.tree_level
      delete x.create_date
      delete x.update_date
    })
    // console.log('The solution is: ', JSON.stringify(results[2]))
    new SysRegion('1001').AddManyEntity(results)

    // new SysDept().AddManyEntity(results)
    // new SysDept().AddEntity(results[0])
    // new SysDept().FindOneEntity({}).then(dbRes => {
    //   console.info(dbRes)
    // })
  })
}
MySql.query()

// new SysDictData().DeleteManyEntity({
//   dict_type_id: 'aa0c31f0-9fbe-11ea-9620-fdfcf3f25d86'
// }, false).then(res => {
//   console.info(res)
// }).catch(res => {
//   console.info(res)
// })

// new SysDictData('1001').FindOneEntity({
//   id: 'ad5edf60-9fbe-11ea-9620-fdfcf3f25d86'
//   // sort: 0
// }).then(res => {
//   console.info(res)
// }).catch(res => {
//   console.info(res)
// })
module.exports = MySql
