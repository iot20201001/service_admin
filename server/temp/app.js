/* eslint-disable camelcase */
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
// 返回的对象是一个键值对，当extended为false的时候，键值对中的值就为'String'或'Array'形式，为true的时候，则可为任何数据类型。
app.use(bodyParser.urlencoded({
  extended: true
}))
// parse application/json
app.use(bodyParser.json())
app.all('*', async (req, res, next) => {
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header('Access-Control-Allow-Headers', '*')
  // 允许的header类型
  // res.header('Access-Control-Allow-Headers', 'content-type,token,tenant_code, Accept-Language,X-Requested-With,TerminalType,foreEndVersion')
  // 跨域允许的请求方式
  res.header('Access-Control-Allow-Methods', '*')
  //
  next()
})

var server = app.listen(8080, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('应用实例，访问地址为 http://%s:%s', host, port)
})