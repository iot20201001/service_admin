// var express = require('express')
// var app = express()
// var bodyParser = require('body-parser')
// // 返回的对象是一个键值对，当extended为false的时候，键值对中的值就为'String'或'Array'形式，为true的时候，则可为任何数据类型。
// app.use(bodyParser.urlencoded({
//   extended: true
// }))
// // parse application/json
// app.use(bodyParser.json())
// // 设置跨域访问
// // https://www.jianshu.com/p/a763dd674293
// app.all('*', (req, res, next) => {
//   // res.header('Access-Control-Allow-Origin', req.environ['ORIGIN'])
//   // res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With,Accept-Language,token,tenant_code')
//   // res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
//   // res.header('X-Powered-By', ' 3.2.1')
//   // res.header('Access-Control-Allow-Credentials', 'TRUE')
//   // res.header('Content-Type', 'application/json;charset=utf-8')
//   // https://www.jb51.net/article/137981.htm
//   // 设置允许跨域的域名，*代表允许任意域名跨域
//   console.info(req.headers.origin)
//   res.header('Access-Control-Allow-Origin', req.headers.origin)
//   // 允许的header类型
//   res.header('Access-Control-Allow-Headers', 'content-type,token,tenant_code, Accept-Language,X-Requested-With')
//   // 跨域允许的请求方式
//   res.header('Access-Control-Allow-Methods', '*')
//   // 跨域允许的请求方式
//   res.header('Access-Control-Allow-Credentials', 'true')
//   next()
// })
// var captchapng = require('captchapng')
// app.get('/auth/captcha', (req, res) => {
//   var code = parseInt(Math.random() * 9000 + 1000, 10)
//   var p = new captchapng(80, 30, code) // width,height,numeric captcha
//   p.color(7, 93, 179, 255) // First color: background (red, green, blue, alpha)
//   p.color(255, 255, 255, 255) // Second color: paint (red, green, blue, alpha)
//   var img = p.getBase64()
//   var imgbase64 = Buffer.from(img, 'base64') // new Buffer(img, 'base64')
//   res.writeHead(200, {
//     'Content-Type': 'image/png'
//   })
//   res.end(imgbase64)
// })
// app.all('*', (req, res, next) => {
//   console.info(req.body)
//   next()
// })
// app.all('/auth/login', (req, res) => {
//   console.info(req.body)
//   res.json({
//     code: 0,
//     data: {
//       code: 0,
//       data: null,
//       msg: ''
//     },
//     msg: ''
//   })
// })
// var server = app.listen(8080, function () {
//   var host = server.address().address
//   var port = server.address().port
//   console.log('应用实例，访问地址为 http://%s:%s', host, port)
// })
