var express = require('express')
var app = express()
// var CaptchaPng = require('captchapng')
const svgCaptcha = require('svg-captcha')
// https: //github.com/produck/svg-captcha
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')

class SysDept extends Entity {
  static async GetPidList (pid) {
    if (pid + '' === '0') return ''
    // 取出当前租户的所有部门 【减少mongodb查询，效率会高些】
    // let result = ''
    // let findObj = await new SysDept(tenant_code).FindOneEntity({ id: pid })
    // if (findObj) {
    //   if (findObj.pid !== '0') {
    //     await SysDept.GetPidList({ id: findObj.pid })
    //   } else {
    //     return ''
    //   }
    // }
  }
}

console.info(SysDept.GetPidList('1067246875800000062'))
var CommonUtils = require('../dbV1.2/CommonUtils')
app.get('/list', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // let r = { 'code': 0, 'msg': 'success', 'data': { 'id': '1264566654331895810', 'username': 'a', 'real_name': 'a', 'headUrl': null, 'gender': 2, 'email': 'a@a.com', 'mobile': '13812345678', 'dept_id': null, 'superAdmin': 0, 'superTenant': 1, 'status': 1, 'remark': null, 'ctime': null, 'role_id_list': null, 'dept_name': null, 'tenant_name': '自定义租户' } }
  new SysDept(tenant_code).toArray(true)
    .then(dbRes => {
      // console.info(dbRes.data.filter(item => item.pid === 0))
      if (dbRes.data.length > 0) {
        dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
      }
      res.json(dbRes)
    })
})
// 新增
app.post('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let data = req.body
  data.id = Utils.UUID_V1()
  if (data.parent_name === '一级部门') data.parent_name = ''
  new SysDept(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysDept(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let data = req.body
  if (data.id === data.pid) {
    return res.json(Utils.ReturnError(null, '上级不能为自己！', 1001))
  }
  if (req.body.parent_name === '一级部门') req.body.parent_name = ''
  new SysDept(tenant_code).UpdateManyEntity({
    pid: req.body.id
  }, {
    parent_name: req.body.name
  })
    .then(_ => {
      new SysDept(tenant_code).UpdateEntity({
        id: req.body.id
      }, req.body)
        .then(dbRes => {
          res.json(dbRes)
        }).catch(err => {
          res.json(err)
        })
    }).catch(err => {
      res.json(err)
    })
})
app.delete('/:id', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let subDeptExists = await new SysDept(tenant_code).where({
    pid: req.params.id
  }).skip(0).limit(1).toArray()
  if (subDeptExists.data.length > 0) {
    return res.json(Utils.resReturn(null, 10001, '存在下级部门，不允许删除！'))
  }
  new SysDept(tenant_code).DeleteManyByIds([req.params.id])
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.resReturn(Utils.ReturnError(err)))
    })
})
// 批量删除
app.delete('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let subDeptExists = await new SysDept(tenant_code).where({
    pid: req.params.id
  }).skip(0).limit(1).toArray()
  if (subDeptExists.data.length > 0) {
    return res.json(Utils.resReturn(null, 10001, '存在下级部门，不允许删除！'))
  }
  new SysDept(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.resReturn(Utils.ReturnError(err)))
    })
})
// 测试数据
// app.get('/init', (req, res) => {
//   let id = Utils.UUID_V1()
//   new SysDept(tenant_code).AddEntity({ pid: 0, id: id, name: '医大四院', parent_name: '', sort: 1 })
//   new SysDept(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'A院区', parent_name: '', sort: 1 })
//   new SysDept(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'B院区', parent_name: '', sort: 1 })
//   res.json(Utils.resReturn(Utils.resReturn(
//     "成功")))
// })
app.subPath = '/sys/dept'
module.exports = app
