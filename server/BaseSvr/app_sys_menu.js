var express = require('express')
var app = express()
// var CaptchaPng = require('captchapng')
const svgCaptcha = require('svg-captcha')
// https: //github.com/produck/svg-captcha
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// var RedisUtils = require('./dbV1.2/RedisUtils')
class SysMenu extends Entity {}
class SysUser extends Entity {}
class SysRole extends Entity {}
app.get('/permissions', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let language = req.headers['accept-language'] || 'zh-CN'
  let uid = req.headers['uid'] || ''
  let findUser = await new SysUser(tenant_code).FindOneEntity({
    id: uid
  })
  if (findUser.data.super_admin === 1 && findUser.data.super_tenant === 1) {
    new SysMenu(tenant_code)
      .where({
        type: 1
      })
      .sort({
        sort: -1
      })
      .toArray(true)
      .then(dbRes => {
        dbRes.data = dbRes.data.map(z => {
          if (z.permissions !== '') return z.permissions
        })
        dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
        res.json(dbRes)
      })
  } else {
    console.info('拿到用户角色列表')
    let role_id_list = findUser.data.role_id_list
    let roles = await new SysRole(tenant_code).FindEntityByIds(role_id_list)
    let menuIds = []
    roles.data.forEach(x => {
      menuIds = [...menuIds, ...x.menu_id_list]
    })
    menuIds = Array.from(new Set(menuIds))
    new SysMenu(tenant_code).FindEntityByIds(menuIds)
      .then(dbRes => {
        dbRes.data = dbRes.data.filter(x => x.type === 1).map(z => {
          if (z.permissions !== '') return z.permissions
        })
        res.json(dbRes)
      })
    // 拿到用户角色列表
    // 拿到角色菜单列表【去除权限项】
    // 菜单项去重
  }
  // let r = {
  //   'code': 0,
  //   'msg': 'success',
  //   'data': ['sys:params:export', 'sys:menu:update', 'sys:menu:delete', 'sys:dept:update', 'sys:params:update', 'sys:dict:delete', 'sys:user:export', 'sys:dept:delete', 'sys:params:delete', 'sys:role:view', 'sys:dict:view', 'sys:dept:view', 'sys:user:delete', 'sys:user:update', 'sys:region:update', 'sys:region:view', 'sys:menu:view', 'sys:params:save', 'sys:user:view', 'sys:menu:save', 'sys:role:save', 'sys:region:delete', 'sys:role:update', '0', 'sys:region:save', 'sys:params:view', 'sys:role:delete', 'sys:user:save', 'sys:dept:save', 'sys:dict:update', 'sys:dict:save']
  // }
  // res.json(r)
})
app.get('/nav', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let language = req.headers['accept-language'] || 'zh-CN'
  let uid = req.headers['uid'] || ''
  console.info(uid)
  // uid = '03707420-9f21-11ea-a040-17a30fdac8cd'
  let findUser = await new SysUser(tenant_code).FindOneEntity({
    id: uid
  })
  // console.info('=======================',findUser.data.super_admin === 1 && findUser.data.super_tenant === 1)
  if (findUser.data.super_admin === 1 && findUser.data.super_tenant === 1) {
    new SysMenu(tenant_code)
      .where({
        type: 0
        // del_flag: 0
      })
      .sort({
        sort: -1
      })
      .toArray(true)
      .then(dbRes => {
        if (dbRes.data.length > 0) {
          dbRes.data = dbRes.data.map(z => {
            try {
              z.name = z.language.find(x => x.language === language).name
              delete z.language
            } catch {}
            return z
          })
          dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
        }
        dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
        res.json(dbRes)
      })
  } else {
    console.info('拿到用户角色列表')
    let role_id_list = findUser.data.role_id_list
    let roles = await new SysRole(tenant_code).FindEntityByIds(role_id_list)
    let menuIds = []
    roles.data.forEach(x => {
      menuIds = [...menuIds, ...x.menu_id_list]
    })
    menuIds = Array.from(new Set(menuIds))
    new SysMenu(tenant_code).FindEntityByIds(menuIds)
      .then(dbRes => {
        if (dbRes.data.length > 0) {
          dbRes.data = dbRes.data.filter(x => x.type === 0).map(z => {
            try {
              z.name = z.language.find(x => x.language === language).name
              delete z.language
            } catch {}
            return z
          })
          dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
        }
        dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
        res.json(dbRes)
      })
    // 拿到用户角色列表
    // 拿到角色菜单列表【去除权限项】
    // 菜单项去重
  }
  // let r = { 'code': 0, 'msg': 'success', 'data': [{ 'id': '1067246875800000002', 'pid': '0', 'children': [{ 'id': '1067246875800000003', 'pid': '1067246875800000002', 'children': [], 'name': '用户管理', 'url': 'sys/user', 'type': 0, 'icon': 'icon-user', 'permissions': '', 'sort': 0, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000009', 'pid': '1067246875800000002', 'children': [], 'name': '部门管理', 'url': 'sys/dept', 'type': 0, 'icon': 'icon-apartment', 'permissions': '', 'sort': 1, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000014', 'pid': '1067246875800000002', 'children': [], 'name': '角色管理', 'url': 'sys/role', 'type': 0, 'icon': 'icon-team', 'permissions': '', 'sort': 2, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }], 'name': '权限管理', 'url': '', 'type': 0, 'icon': 'icon-lock', 'permissions': '', 'sort': 0, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000019', 'pid': '0', 'children': [{ 'id': '1067246875800000020', 'pid': '1067246875800000019', 'children': [], 'name': '菜单管理', 'url': 'sys/menu', 'type': 0, 'icon': 'icon-unorderedlist', 'permissions': null, 'sort': 0, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000025', 'pid': '1067246875800000019', 'children': [], 'name': '参数管理', 'url': 'sys/params', 'type': 0, 'icon': 'icon-fileprotect', 'permissions': '', 'sort': 1, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000031', 'pid': '1067246875800000019', 'children': [], 'name': '字典管理', 'url': 'sys/dict-type', 'type': 0, 'icon': 'icon-gold', 'permissions': '', 'sort': 2, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1164489061834969089', 'pid': '1067246875800000019', 'children': [], 'name': '行政区域', 'url': 'sys/region', 'type': 0, 'icon': 'icon-location', 'permissions': '0', 'sort': 3, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }], 'name': '系统设置', 'url': '', 'type': 0, 'icon': 'icon-setting', 'permissions': null, 'sort': 1, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1127520061821603842', 'pid': '0', 'children': [{ 'id': '1127520228847177730', 'pid': '1127520061821603842', 'children': [], 'name': '租户管理', 'url': 'tenant/tenant', 'type': 0, 'icon': 'icon-team', 'permissions': '', 'sort': 0, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1127521799777603585', 'pid': '1127520061821603842', 'children': [], 'name': '租户角色', 'url': 'tenant/tenant-role', 'type': 0, 'icon': 'icon-team', 'permissions': '', 'sort': 1, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }], 'name': '租户管理', 'url': '', 'type': 0, 'icon': 'icon-home', 'permissions': '', 'sort': 2, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000036', 'pid': '0', 'children': [{ 'id': '1067246875800000037', 'pid': '1067246875800000036', 'children': [], 'name': '登录日志', 'url': 'sys/log-login', 'type': 0, 'icon': 'icon-filedone', 'permissions': '', 'sort': 0, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000038', 'pid': '1067246875800000036', 'children': [], 'name': '操作日志', 'url': 'sys/log-operation', 'type': 0, 'icon': 'icon-solution', 'permissions': '', 'sort': 1, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000039', 'pid': '1067246875800000036', 'children': [], 'name': '异常日志', 'url': 'sys/log-error', 'type': 0, 'icon': 'icon-file-exception', 'permissions': '', 'sort': 2, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }], 'name': '日志管理', 'url': '', 'type': 0, 'icon': 'icon-container', 'permissions': '', 'sort': 4, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000040', 'pid': '0', 'children': [{ 'id': '1067246875800000041', 'pid': '1067246875800000040', 'children': [], 'name': '服务监控', 'url': '{{ window.SITE_CONFIG["apiURL"] }}/monitor', 'type': 0, 'icon': 'icon-medicinebox', 'permissions': '', 'sort': 0, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1067246875800000042', 'pid': '1067246875800000040', 'children': [], 'name': '接口文档', 'url': '{{ window.SITE_CONFIG["apiURL"] }}/swagger-ui.html', 'type': 0, 'icon': 'icon-file-word', 'permissions': '', 'sort': 1, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }], 'name': '系统监控', 'url': '', 'type': 0, 'icon': 'icon-desktop', 'permissions': '', 'sort': 5, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }, { 'id': '1176372255559024642', 'pid': '0', 'children': [{ 'id': '1206460008292216834', 'pid': '1176372255559024642', 'children': [], 'name': '新闻管理', 'url': 'sys/news', 'type': 0, 'icon': 'icon-file-word', 'permissions': '', 'sort': 0, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }], 'name': '功能示例', 'url': '', 'type': 0, 'icon': 'icon-windows', 'permissions': '', 'sort': 999, 'ctime': '2020-05-24 15:19:47', 'resourceList': null, 'parent_name': null }] }
  //  res.json(r)
})
app.get('/select', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let language = req.headers['accept-language'] || 'zh-CN'
  new SysMenu(tenant_code)
    .select('id,pid,name,language')
    .sort({
      sort: 1
    })
    .toArray(true)
    .then(dbRes => {
      if (dbRes.data.length > 0) {
        dbRes.data = dbRes.data.map(z => {
          try {
            z.name = z.language.find(x => x.language === language).name
            delete z.language
          } catch {}
          return z
        })
        dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
      }
      dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
      res.json(dbRes)
    })
})
var CommonUtils = require('../dbV1.2/CommonUtils')
app.get('/list', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let language = req.headers['accept-language'] || 'zh-CN'
  new SysMenu(tenant_code)
    .sort({
      sort: -1
    })
    .toArray(true)
    .then(dbRes => {
      if (dbRes.data.length > 0) {
        dbRes.data = dbRes.data.map(z => {
          try {
            z.name = z.language.find(x => x.language === language).name
            delete z.language
          } catch {}
          return z
        })
        dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
      }
      dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
      res.json(dbRes)
    })
})
// 新增
app.post('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let data = req.body
  data.id = Utils.UUID_V1()
  new SysMenu(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysMenu(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let data = req.body
  if (data.id === data.pid) {
    return res.json(Utils.ReturnError(null, '上级不能为自己！', 1001))
  }
  new SysMenu(tenant_code).UpdateManyEntity({
    pid: req.body.id
  }, {
    parent_name: req.body.name
  })
    .then(_ => {
      new SysMenu(tenant_code).UpdateEntity({
        id: req.body.id
      }, req.body)
        .then(dbRes => {
          res.json(dbRes)
        }).catch(err => {
          res.json(err)
        })
    }).catch(err => {
      res.json(err)
    })
})
app.delete('/:id', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let subMenuExists = await new SysMenu(tenant_code).where({
    pid: req.params.id
  }).skip(0).limit(1).toArray()
  if (subMenuExists.data.length > 0) {
    return res.json(Utils.resReturn(null, 10001, '存在下级菜单，不允许删除！'))
  }
  new SysMenu(tenant_code).DeleteManyByIds([req.params.id])
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.ReturnError(err))
    })
})
// // 批量删除
// app.delete('/', (req, res) => {
//   let tenant_code = req.headers['tenant_code'] || 'tenant_code'
//   new SysMenu(tenant_code).DeleteManyByIds(req.body)
//     .then(dbRes => {
//       res.json(Utils.resReturn(dbRes))
//     }).catch(err => {
//       res.json(Utils.resReturn(Utils.ReturnError(err)))
//     })
// })
// 测试数据
// app.get('/init', (req, res) => {
//   let id = Utils.UUID_V1()
//   new SysMenu().AddEntity({ pid: 0, id: id, name: '医大四院', parent_name: '', sort: 1 })
//   new SysMenu().AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'A院区', parent_name: '', sort: 1 })
//   new SysMenu().AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'B院区', parent_name: '', sort: 1 })
//   res.json(Utils.resReturn(Utils.resReturn(
//     '成功')))
// })
app.subPath = '/sys/menu'
module.exports = app
