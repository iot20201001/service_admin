// express_demo.js 文件
var express = require('express')
var app = express()
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// 实例化子服务特有操作，如需多模块并用可加入文件中【../../dbV1.2/MessageEntitys】
class SysRole extends Entity {}
// 查找
class User extends Entity {}
app.get('/page', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  var skip = 0
  var limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  if (req.query.name) {
    search = {
      ...search,
      ...{
        name: eval(`/${req.query.name}/`)
      }
    }
  }
  let opt = new SysRole(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(async dbRes => {
      // 此处挨个获取用户数
      // if (dbRes.data.length > 0) {
      //   for (var i = 0; i < dbRes.data.length; i++) {
      //     let tt = dbRes.data[i]
      //     try {
      //       // let res = await new User().where({ role_id_list: { '$in': [tt.id] } }).limit(1000).toArray()
      //       let res = await new User().where({
      //         role_id_list: {
      //           $regex: tt.id,
      //           $options: 'i'
      //         }
      //       }).limit(1000).toArray()
      //       tt.userCount = res.total
      //     } catch {}
      //   }
      // }
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 查找
app.get('/list', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysRole(tenant_code).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 新增
app.post('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let data = req.body
  data.id = Utils.UUID_V1()
  new SysRole(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysRole(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysRole(tenant_code).UpdateManyEntity({
    pid: req.body.id
  }, {
    parent_name: req.body.name
  })
    .then(_ => {
      new SysRole(tenant_code).UpdateEntity({
        id: req.body.id
      }, req.body)
        .then(dbRes => {
          res.json(dbRes)
        }).catch(err => {
          res.json(err)
        })
    }).catch(err => {
      res.json(err)
    })
})
// 批量删除
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysRole(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.resReturn(Utils.ReturnError(err)))
    })
})
// 测试数据
// app.get('/init', (req, res) => {
// let tenant_code = req.headers['tenant_code'] || 'tenant_code'
//   let id = Utils.UUID_V1()
//   new SysRole(tenant_code).AddEntity({ pid: 0, id: id, name: '医大四院', parent_name: '', sort: 1 })
//   new SysRole(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'A院区', parent_name: '', sort: 1 })
//   new SysRole(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'B院区', parent_name: '', sort: 1 })
//   res.json(Utils.resReturn(Utils.resReturn(
//     "成功")))
// })
app.subPath = '/sys/role'
module.exports = app
