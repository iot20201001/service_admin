// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// 实例化子服务特有操作，如需多模块并用可加入文件中【../../dbV1.2/MessageEntitys】
class SysParamsHistory extends Entity {}
class SysParams extends Entity {}
// 查找
app.get('/page', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // 查找当前租户信息 如果租户是系统租户则能看到所有的租户参数
  let skip = 0
  let limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  if (req.query.paramCode) {
    search = {
      ...search,
      ...{
        paramCode: eval(`/${req.query.paramCode}/`)
      }
    }
  }
  if (req.query.paramValue) {
    search = {
      ...search,
      ...{
        paramValue: eval(`/${req.query.paramValue}/`)
      }
    }
  }
  let opt = new SysParamsHistory(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  } else {
    opt.sort({
      ctime: -1
    })
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 新增
app.post('/use/:id', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let id = req.params.id
  let findObj = await new SysParamsHistory(tenant_code).FindOneEntity({
    id
  })
  if (!findObj.data) {
    return res.json(Utils.ReturnError(null, '参数编码已被刪除，请刷新后重试！', 1001))
  }
  let history = findObj.data
  // delete history.id
  delete history._id
  history.updater = uid
  new SysParams(tenant_code).SaveEntity(history, {
    paramCode: history.paramCode
  })
    .then(dbRes => {
      res.json(Utils.ReturnOK(dbRes))
    }).catch(err => {
      res.json(Utils.ReturnError(err, '操作失败！', 1001))
    })
})
// 新增
app.post('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.creator = uid
  data.updater = uid
  data.tenant_code = tenant_code
  data.id = Utils.UUID_V1()
  let findObj = await new SysParamsHistory(tenant_code).FindOneEntity({
    paramCode: data.paramCode
  })
  if (findObj.data) {
    return res.json(Utils.ReturnError(null, '参数编码已存在！', 1001))
  }
  new SysParamsHistory(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按dict_type查找
app.get('/findByParamCode/:paramCode', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysParamsHistory(tenant_code)
    .where({
      paramCode: req.params.paramCode
    })
    .toArray()
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.resReturn(err))
    })
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysParamsHistory(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  req.body.updater = uid
  new SysParamsHistory(tenant_code).UpdateManyEntity({
    pid: req.body.id
  }, {
    parent_name: req.body.name
  })
    .then(_ => {
      new SysParamsHistory(tenant_code).UpdateEntity({
        id: req.body.id
      }, req.body)
        .then(dbRes => {
          res.json(Utils.resReturn(dbRes))
        }).catch(err => {
          res.json(Utils.resReturn(err))
        })
    }).catch(err => {
      res.json(Utils.resReturn(err))
    })
})
// 批量删除
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysParamsHistory(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(Utils.resReturn(dbRes)))
    }).catch(err => {
      res.json(Utils.resReturn(Utils.ReturnError(err)))
    })
})
// 测试数据
// app.get('/init', (req, res) => {
//   let id = Utils.UUID_V1()
//   new SysParamsHistory(tenant_code).AddEntity({
//     pid: 0,
//     id: id,
//     name: '医大四院',
//     parent_name: '',
//     sort: 1
//   })
//   new SysParamsHistory(tenant_code).AddEntity({
//     pid: id,
//     id: Utils.UUID_V1(),
//     name: 'A院区',
//     parent_name: '',
//     sort: 1
//   })
//   new SysParamsHistory(tenant_code).AddEntity({
//     pid: id,
//     id: Utils.UUID_V1(),
//     name: 'B院区',
//     parent_name: '',
//     sort: 1
//   })
//   res.json(Utils.resReturn(Utils.resReturn(
//     "成功")))
// })
app.subPath = '/sys/params/history'
module.exports = app
