var express = require('express')
var app = express()
// var CaptchaPng = require('captchapng')
const svgCaptcha = require('svg-captcha')
// https: //github.com/produck/svg-captcha
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// var RedisUtils = require('./dbV1.2/RedisUtils')

// class SysUserToken extends Entity {}

app.get('/mynotice/unread', async (req, res) => {
  let r = { 'code': 0, 'msg': 'success', 'data': [] }
  res.json(r)
})
app.subPath = '/sys/notice'
module.exports = app
