var express = require('express')
var app = express()
// var CaptchaPng = require('captchapng')
const svgCaptcha = require('svg-captcha')
// https: //github.com/produck/svg-captcha
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// var RedisUtils = require('./dbV1.2/RedisUtils')
class SysRegion extends Entity {}
app.get('/select', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysRegion(tenant_code)
    .sort({
      sort: -1
    })
    .toArray(true)
    .then(dbRes => {
      if (dbRes.data.length > 0) {
        dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
      }
      dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
      res.json(dbRes)
    })
})
var CommonUtils = require('../dbV1.2/CommonUtils')
app.get('/list', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let params = {
    pid: req.query.pid || '0'
  }
  new SysRegion(tenant_code)
    .where(params)
    .sort({
      sort: 1
    })
    .toArray(true)
    .then(dbRes => {
      // console.info('-------------->>>>',dbRes.data.length)
      if (dbRes.data.length > 0) {
        dbRes.data.forEach(x => {
          x.hasChildren = x.leaf !== 1
          if (!req.query.pid || req.query.pid !== '0') {
            x.tree_level = 1
            x.level = 1
          }
        })
        // dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
      }
      // dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
      res.json(dbRes)
    })
})
app.get('/tree', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysRegion(tenant_code)
    .select('id,pid,name')
    .sort({
      sort: 1
    })
    .toArray(true)
    .then(dbRes => {
      // if (dbRes.data.length > 0) {
      //   dbRes.data.forEach(x=>{
      //     x.hasChildren = x.leaf !== 1
      //   })
      //   // dbRes.data = CommonUtils.BuildTreeData(dbRes.data)
      // }
      // dbRes.data = dbRes.data.sort((a, b) => a.sort - b.sort)
      res.json(dbRes)
    })
})
// 新增
app.post('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let currRegion = req.body
  if (!currRegion.id) currRegion.id = Utils.UUID_V1()
  let findObj = await new SysRegion(tenant_code).FindOneEntity({
    id: currRegion.id
  })
  if (findObj.data) {
    return res.json(Utils.ReturnError(null, '当前区域标识已存在！', 1001))
  }
  // 查询上级
  if (currRegion.pid !== '0') {
    let parentObj = await new SysRegion(tenant_code).FindOneEntity({
      id: currRegion.pid
    })
    if (parentObj.data) {
      if (parentObj.data.leaf === 1) {
        parentObj.data.leaf = 0
        parentObj.updater = uid
        await new SysRegion(tenant_code).SaveEntity(parentObj.data, { id: parentObj.data.id })
      }
    }
  }
  currRegion.leaf = 1
  currRegion.creator = uid
  currRegion.updater = uid
  new SysRegion(tenant_code).AddEntity(currRegion)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysRegion(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let currRegion = req.body
  if (currRegion.id === currRegion.pid) {
    return res.json(Utils.ReturnError(null, '上级不能为自己！', 1001))
  }
  // 查询数据库中的当前区域
  let findObj = await new SysRegion(tenant_code).FindOneEntity({
    id: currRegion.id
  })
  if (!findObj.data) {
    return res.json(Utils.ReturnError(null, '当前数据不存在，请刷新后重试！', 1001))
  } else if (findObj.data.pid !== currRegion.pid && findObj.data.pid !== '0') {
    // 上级已经改变，则需要判断是否需要设置上级为叶子
    let oldParentObj = await new SysRegion(tenant_code).FindOneEntity({
      id: findObj.data.pid
    })
    let oldParentSubObj = await new SysRegion(tenant_code).FindOneEntity({
      pid: oldParentObj.data.id
    })
    if (!oldParentSubObj.data) {
      oldParentObj.data.leaf = 1
      oldParentObj.data.updater = uid
      await new SysRegion(tenant_code).SaveEntity(oldParentObj.data, {
        id: oldParentObj.data.id
      })
    } else {
      oldParentObj.data.leaf = 0
      oldParentObj.data.updater = uid
      await new SysRegion(tenant_code).SaveEntity(oldParentObj.data, {
        id: oldParentObj.data.id
      })
    }
  }
  // 处理当前上级
  if (currRegion.pid !== '0') {
    let parentObj = await new SysRegion(tenant_code).FindOneEntity({
      id: currRegion.pid
    })
    if (parentObj.data) {
    // 如果当前上级为叶子节点，则设置为非叶子节点
      if (parentObj.data.leaf === 1) {
        parentObj.data.leaf = 0
        parentObj.updater = uid
        await new SysRegion(tenant_code).SaveEntity(parentObj.data, { id: parentObj.data.id })
      }
    }
  }
  currRegion.updater = uid
  new SysRegion(tenant_code).UpdateManyEntity({
    pid: currRegion.id
  }, {
    // 如果更改过名称
    parent_name: currRegion.name
  })
    .then(_ => {
      new SysRegion(tenant_code).UpdateEntity({
        id: currRegion.id
      }, currRegion)
        .then(dbRes => {
          res.json(dbRes)
        }).catch(err => {
          res.json(err)
        })
    }).catch(err => {
      res.json(err)
    })
})
app.delete('/:id', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let subMenuExists = await new SysRegion(tenant_code).where({
    pid: req.params.id
  }).skip(0).limit(1).toArray()
  if (subMenuExists.data.length > 0) {
    return res.json(Utils.resReturn(null, 10001, '存在下级区域，不允许删除！'))
  }
  let findObj = await new SysRegion(tenant_code).FindOneEntity({
    id: req.params.id
  })
  if (findObj.data) {
    if (findObj.data.pid !== '0') {
      // 判断其上级节点是否还有子节点
      let parentSubObj = await new SysRegion(tenant_code).where({
        pid: findObj.data.pid
      }).skip(0).limit(2).toArray(false)
      if (parentSubObj.data.length < 2) {
        let params = {
          leaf: 1,
          updater: uid
        }
        console.info('sssssssssssssssssssssssssssssss')
        await new SysRegion(tenant_code).SaveEntity(params, {
          id: findObj.data.pid
        })
      }
    }
  }
  new SysRegion(tenant_code).DeleteManyByIds([req.params.id])
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.ReturnError(err))
    })
})
// // 批量删除
// app.delete('/', (req, res) => {
//   let tenant_code = req.headers['tenant_code'] || 'tenant_code'
//   new SysRegion(tenant_code).DeleteManyByIds(req.body)
//     .then(dbRes => {
//       res.json(Utils.resReturn(dbRes))
//     }).catch(err => {
//       res.json(Utils.resReturn(Utils.ReturnError(err)))
//     })
// })
// 测试数据
// app.get('/init', (req, res) => {
//   let id = Utils.UUID_V1()
//   new SysRegion().AddEntity({ pid: 0, id: id, name: '医大四院', parent_name: '', sort: 1 })
//   new SysRegion().AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'A院区', parent_name: '', sort: 1 })
//   new SysRegion().AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'B院区', parent_name: '', sort: 1 })
//   res.json(Utils.resReturn(Utils.resReturn(
//     '成功')))
// })
app.subPath = '/sys/region'
module.exports = app
