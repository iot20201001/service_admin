var express = require('express')
var app = express()
// var CaptchaPng = require('captchapng')
const svgCaptcha = require('svg-captcha')
// https: //github.com/produck/svg-captcha
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// var RedisUtils = require('./dbV1.2/RedisUtils')

class SysTenantRole extends Entity {}

app.get('/page', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  var skip = 0; var limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch { }
  let search = {}
  if (req.query.name) search = { ...search, ...{ name: eval(`/${req.query.name}/`) } }
  let opt = new SysTenantRole(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(async dbRes => {
      // 此处挨个获取用户数
      // if (dbRes.data.length > 0) {
      //   for (var i = 0; i < dbRes.data.length; i++) {
      //     let tt = dbRes.data[i]
      //     try {
      //       // let res = await new User().where({ role_id_list: { '$in': [tt.id] } }).limit(1000).toArray()
      //       let res = await new User().where({ role_id_list: { $regex: tt.id, $options: 'i' } }).limit(1000).toArray()
      //       tt.userCount = res.total
      //     } catch{ }
      //   }
      // }
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
  // let r = { 'code': 0, 'msg': 'success', 'data': { 'total': 2, 'list': [{ 'id': '1264566735881748482', 'name': '自定义租户角色', 'remark': '', 'ctime': '2020-05-24 22:39:39', 'menu_id_list': null, 'dept_id_list': null }, { 'id': '1125415693534105602', 'name': '默认租户角色', 'remark': '', 'ctime': '2020-05-24 15:19:47', 'menu_id_list': null, 'dept_id_list': null }] } }
  // res.json(r)
})
// 查找
app.get('/list', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysTenantRole(tenant_code).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 新增
app.post('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let data = req.body
  data.id = Utils.UUID_V1()
  new SysTenantRole(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysTenantRole(tenant_code).FindOneEntity({ id: req.params.id })
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysTenantRole(tenant_code).UpdateManyEntity({ pid: req.body.id }, { parent_name: req.body.name })
    .then(_ => {
      new SysTenantRole(tenant_code).UpdateEntity({ id: req.body.id }, req.body)
        .then(dbRes => {
          res.json(dbRes)
        }).catch(err => {
          res.json(err)
        })
    }).catch(err => {
      res.json(err)
    })
})
// 批量删除
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysTenantRole(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.ReturnError(err))
    })
})
app.subPath = '/sys/tenant/role'
module.exports = app
