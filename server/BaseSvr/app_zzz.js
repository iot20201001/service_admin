// // var {
// //   Entity
// // } = require('./dbV1.2/MessageEntitys')
// // var CommonUtils = require('./dbV1.2/CommonUtils')
// // class SysMenu extends Entity {}
// // let tenant_code = '1001'
// // new SysMenu(tenant_code)
// //   .sort({ sort: 1 })
// //   .limit(15)
// //   .toArray(true)
// //   .then(dbRes => {
// //     // if (dbRes.data.length > 0) { dbRes.data = CommonUtils.BuildTreeData(dbRes.data) }
// //     dbRes.data.forEach(x => {
// //       console.info(x.sort)
// //     })
// //   })

// // let URL = 'aaaaAAA=a'
// // let t = URL.split('?')
// // let r = t[0] + (t[1] !== undefined ? t[1] : '')
// // console.info(r)

// // let tt = '/sys-user?paramCode=123456'
// // console.info(/\?.*/.test(tt))
// // console.info(tt.replace(/\?.*/, ''))

// // let memoryUsage = process.memoryUsage()

// // memoryUsage = Object.keys(memoryUsage).map(x => {
// //   return {
// //     key: x,
// //     value: (memoryUsage[x] / 1024 / 1024).toFixed(2) + 'MB'
// //   }
// // })

// // console.info(memoryUsage)

let ok = {
  formConf: {
    'formRef': 'elForm',
    'formModel': 'formData',
    'size': 'medium',
    'labelPosition': 'right',
    'labelWidth': 100,
    'formRules': 'rules',
    'gutter': 15,
    'disabled': false,
    'span': 12,
    'formBtns': true,
    'method': 'put',
    'reqUrl': '/api/submit'
  },
  drawingList: [{
    '__config__': {
      'label': '用户名',
      'labelWidth': null,
      'showLabel': true,
      'changeTag': true,
      'tag': 'el-input',
      'tagIcon': 'input',
      'required': true,
      'layout': 'colFormItem',
      'span': 12,
      'document': 'https://element.eleme.cn/#/zh-CN/component/input',
      'regList': [],
      'formId': 102,
      'renderKey': 1590649085533
    },
    '__slot__': {
      'prepend': '',
      'append': ''
    },
    'placeholder': '请输入单行文本',
    'style': {
      'width': '100%'
    },
    'clearable': true,
    'prefix-icon': '',
    'suffix-icon': '',
    'maxlength': null,
    'show-word-limit': false,
    'readonly': false,
    'disabled': false,
    '__vModel__': 'username'
  }, {
    '__config__': {
      'label': '密码',
      'showLabel': true,
      'labelWidth': null,
      'changeTag': true,
      'tag': 'el-input',
      'tagIcon': 'password',
      'layout': 'colFormItem',
      'span': 12,
      'required': true,
      'regList': [],
      'document': 'https://element.eleme.cn/#/zh-CN/component/input',
      'formId': 104,
      'renderKey': 1590649109080
    },
    '__slot__': {
      'prepend': '',
      'append': ''
    },
    'placeholder': '请输入密码',
    'show-password': true,
    'style': {
      'width': '100%'
    },
    'clearable': true,
    'prefix-icon': '',
    'suffix-icon': '',
    'maxlength': null,
    'show-word-limit': false,
    'readonly': false,
    'disabled': false,
    '__vModel__': 'password'
  }, {
    '__config__': {
      'label': 'chart-line',
      'showLabel': false,
      'changeTag': true,
      'labelWidth': null,
      'tag': 've-line',
      'tagIcon': 'button',
      'span': 12,
      'layout': 'colFormItem',
      'document': 'https://element.eleme.cn/#/zh-CN/component/button',
      'formId': 105,
      'renderKey': 1590649117717
    },
    '__slot__': {
      'default': '主要按钮'
    },
    'data': {
      'columns': ['date', 'PV', 'EV'],
      'rows': [{
        'date': '01-01',
        'PV': 1231,
        'EV': 1231
      }, {
        'date': '01-02',
        'PV': 1223,
        'EV': 1231
      }, {
        'date': '01-03',
        'PV': 2123,
        'EV': 1231
      }, {
        'date': '01-04',
        'PV': 4123,
        'EV': 1231
      }, {
        'date': '01-05',
        'PV': 3123,
        'EV': 1231
      }, {
        'date': '01-06',
        'PV': 7123,
        'EV': 1231
      }]
    },
    'type': 'primary',
    'icon': 'el-icon-search',
    'round': false,
    'size': 'medium',
    'plain': false,
    'circle': false,
    'disabled': false,
    '__vModel__': 'field105'
  }, {
    '__config__': {
      'label': 'chart-line',
      'showLabel': false,
      'changeTag': true,
      'labelWidth': null,
      'tag': 've-line',
      'tagIcon': 'button',
      'span': 12,
      'layout': 'colFormItem',
      'document': 'https://element.eleme.cn/#/zh-CN/component/button',
      'formId': 107,
      'renderKey': 1590649122987
    },
    '__slot__': {
      'default': '主要按钮'
    },
    'data': {
      'columns': ['date', 'PV', 'EV'],
      'rows': [{
        'date': '01-01',
        'PV': 1231,
        'EV': 1231
      }, {
        'date': '01-02',
        'PV': 1223,
        'EV': 1231
      }, {
        'date': '01-03',
        'PV': 2123,
        'EV': 1231
      }, {
        'date': '01-04',
        'PV': 4123,
        'EV': 1231
      }, {
        'date': '01-05',
        'PV': 3123,
        'EV': 1231
      }, {
        'date': '01-06',
        'PV': 7123,
        'EV': 1231
      }]
    },
    'type': 'primary',
    'icon': 'el-icon-search',
    'round': false,
    'size': 'medium',
    'plain': false,
    'circle': false,
    'disabled': false,
    '__vModel__': 'field107'
  }]
}

// console.info(JSON.stringify(ok))
