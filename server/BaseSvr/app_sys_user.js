// express_demo.js 文件
var express = require('express')
var app = express()
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// 查找
class SysUser extends Entity {}
app.get('/info', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid']
  // console.info('-----as55----------->', uid)
  new SysUser(tenant_code).FindOneEntity({
    id: uid
  })
    .then(dbRes => {
      delete dbRes.data.passsalt
      delete dbRes.data.password
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/page', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid']
  console.info('uid--AAA->', uid, tenant_code, req.query)
  var skip = 0
  var limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  if (req.query.username) {
    search = {
      ...search,
      ...{
        username: eval(`/${req.query.username}/`)
      }
    }
  }
  if (req.query.gender) {
    search = {
      ...search,
      ...{
        gender: Number(req.query.gender)
      }
    }
  }
  if (req.query.dept_id) {
    search = {
      ...search,
      ...{
        dept_id: eval(`/${req.query.dept_id}/`)
      }
    }
  }
  let opt = new SysUser(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(async dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 查找
app.get('/list', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysUser(tenant_code).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 新增
app.post('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  req.body.confirm_password = ''
  let user = req.body
  user.id = Utils.UUID_V1()
  let findUser = await new SysUser(tenant_code).where({
    username: user.username
  }).toArray(false)
  if (findUser.data.length > 0) {
    res.json(Utils.ReturnError(user, '用户名已存在!'))
    return
  }
  user.passsalt = Utils.GetRandomStr()
  user.password = Utils.GetMD5(user.password + user.passsalt)

  delete user.confirm_password
  new SysUser(tenant_code).SaveEntity(user, {
    id: user.id
  }).then(dbRes => {
    res.json(Utils.ReturnOK(user, '操作成功!'))
  }).catch(dbRes => {
    res.json(Utils.ReturnError(user, '操作失败!'))
  })
})
app.put('/password', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  console.info(req.body, uid, tenant_code)
  let user = {
    ...req.body
  }
  let findUser = await new SysUser(tenant_code).where({
    id: uid
  }).toArray(false)
  if (findUser.data.length === 0) {
    res.json(Utils.ReturnError(user, '用户不存在!'))
  } else {
    let passsalt = findUser.data[0].passsalt
    let oldPassword = Utils.GetMD5(user.password + passsalt)
    if (oldPassword !== findUser.data[0].password) {
      res.json(Utils.ReturnError(user, '原密码错误!'))
      return
    }
    user.passsalt = Utils.GetRandomStr()
    user.password = Utils.GetMD5(user.newPassword + user.passsalt)

    delete user.confirm_password
    delete user.newPassword
    new SysUser(tenant_code).SaveEntity(user, {
      id: uid
    }).then(dbRes => {
      res.json(Utils.ReturnOK(user, '操作成功!'))
    }).catch(dbRes => {
      res.json(Utils.ReturnError(user, '操作失败!'))
    })
  }
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysUser(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      delete dbRes.data.passsalt
      delete dbRes.data.password
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  req.body.confirm_password = ''
  let user = req.body

  let findUser = await new SysUser(tenant_code).where({
    username: user.username
  }).toArray(false)
  if (findUser.data.length === 0) {
    res.json(Utils.ReturnError(user, '用户不存在!'))
    return
  } else {
    // 表示用户需要更改用户名
    if (user.id !== findUser.data[0].id) {
      res.json(Utils.ReturnError(user, '用户名被占用!'))
      return
    }
    // 表示用户需要更改密码
    if (user.password !== '') {
      user.passsalt = Utils.GetRandomStr()
      user.password = Utils.GetMD5(user.password + user.passsalt)
    } else {
      delete user.password
    }
  }
  delete user.confirm_password
  delete user.newPassword
  // console.info(user)
  new SysUser(tenant_code).SaveEntity(user, {
    id: user.id
  }).then(dbRes => {
    res.json(Utils.ReturnOK(user, '操作成功!'))
  }).catch(dbRes => {
    res.json(Utils.ReturnError(user, '操作失败!'))
  })
})
// 批量删除
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysUser(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.resReturn(Utils.ReturnError(err)))
    })
})
// 测试数据
// app.get('/init', (req, res) => {
// let tenant_code = req.headers['tenant_code'] || 'tenant_code'
//   let id = Utils.UUID_V1()
//   new SysUser(tenant_code).AddEntity({ pid: 0, id: id, name: '医大四院', parent_name: '', sort: 1 })
//   new SysUser(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'A院区', parent_name: '', sort: 1 })
//   new SysUser(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'B院区', parent_name: '', sort: 1 })
//   res.json(Utils.resReturn(Utils.resReturn(
//     "成功")))
// })
app.subPath = '/sys/user'
module.exports = app
