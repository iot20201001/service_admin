// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// 实例化子服务特有操作，如需多模块并用可加入文件中【../../dbV1.2/MessageEntitys】
class SysLogLogin extends Entity {}
// 查找
app.get('/page', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // 查找当前租户信息 如果租户是系统租户则能看到所有的租户参数
  let skip = 0
  let limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  console.info(req.query)
  if (req.query.creator_name) {
    search = {
      ...search,
      ...{
        creator_name: eval(`/${req.query.creator_name}/`)
      }
    }
  }
  if (req.query.status) {
    search = {
      ...search,
      ...{
        status: Number(req.query.status)
      }
    }
  }
  let opt = new SysLogLogin(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  } else {
    opt.sort({
      ctime: -1
    })
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 批量删除
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // 附带历史数据全部删除
  new SysParams(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(Utils.resReturn(dbRes)))
    }).catch(err => {
      res.json(Utils.resReturn(Utils.ReturnError(err)))
    })
})
app.subPath = '/sys/log/login'
module.exports = app
