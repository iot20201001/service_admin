// express_demo.js 文件
var express = require('express')
var app = express()
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
// 实例化子服务特有操作，如需多模块并用可加入文件中【../../dbV1.2/MessageEntitys】
class Sysdict_type extends Entity {}
// 查找
app.get('/page', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  var skip = 0
  var limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  if (req.query.dict_name) {
    search = {
      ...search,
      ...{
        dict_name: eval(`/${req.query.dict_name}/`)
      }
    }
  }
  if (req.query.dict_type) {
    search = {
      ...search,
      ...{
        dict_type: eval(`/${req.query.dict_type}/`)
      }
    }
  }
  let opt = new Sysdict_type(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      // console.info(err)
      res.json(err)
    })
})
// 新增
app.post('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.creator = uid
  data.updasyncater = uid
  data.id = Utils.UUID_V1()
  let findObj = await new Sysdict_type(tenant_code).FindOneEntity({
    dict_type: data.dict_type
  })
  if (findObj.data) {
    return res.json(Utils.ReturnError(null, '字典类型已存在！', 1001))
  }
  new Sysdict_type(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 查找all
// class SysDictData extends Entity { }
app.get('/all', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new Sysdict_type(tenant_code)
    .include(new SysDictData(tenant_code), 'id', 'dict_type_id', 'dataList')
    .select('_id,dict_name,dict_type,sort,remark,id,ctime,utime,dataList')
    .sort({
      sort: 1
    })
    .toArray()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按id查找
app.get('/:id', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new Sysdict_type(tenant_code).FindOneEntity({
    id: req.params.id
  })
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 按dict_type查找
app.get('/findByDictType/:dict_type', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new Sysdict_type(tenant_code)
    .where({
      dict_type: req.params.dict_type
    })
    .include(new SysDictData(tenant_code), 'id', 'dict_type_id', 'dictData')
    .select('_id,dict_name,dict_type,sort,remark,id,ctime,utime,dictData')
    .sort({
      sort: 1
    })
    .toArray()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 修改
app.put('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  req.body.updater = uid
  new Sysdict_type(tenant_code).UpdateManyEntity({
    pid: req.body.id
  }, {
    parent_name: req.body.name,
    updater: uid
  })
    .then(_ => {
      req.body.updater = uid
      new Sysdict_type(tenant_code).UpdateEntity({
        id: req.body.id
      }, req.body)
        .then(dbRes => {
          res.json(dbRes)
        }).catch(err => {
          res.json(err)
        })
    }).catch(err => {
      res.json(err)
    })
})
// 批量删除
class SysDictData extends Entity {}
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // 删除数据
  req.body.forEach(async typeId => {
    await new SysDictData(tenant_code).DeleteManyEntity({
      dict_type_id: typeId
    }, false)
  })
  new Sysdict_type(tenant_code).DeleteManyByIds(req.body)
    .then(dbRes => {
      res.json(Utils.resReturn(dbRes))
    }).catch(err => {
      res.json(Utils.resReturn(err))
    })
})
// 测试数据
// app.get('/init', (req, res) => {
//   let tenant_code = req.headers['tenant_code'] || 'tenant_code'
//   let id = Utils.UUID_V1()
//   new Sysdict_type(tenant_code).AddEntity({ pid: 0, id: id, name: '医大四院', parent_name: '', sort: 1 })
//   new Sysdict_type(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'A院区', parent_name: '', sort: 1 })
//   new Sysdict_type(tenant_code).AddEntity({ pid: id, id: Utils.UUID_V1(), name: 'B院区', parent_name: '', sort: 1 })
//   res.json(Utils.resReturn(Utils.resReturn(
//     "成功")))
// })

async function getDictDataByCode (code) {
  var result = []
  var dict = {}
  dict = await new Sysdict_type(tenant_code)
    .where({
      dict_type: code
    })
    .include(new SysDictData(tenant_code), 'id', 'dict_type_id', 'dictData')
    .select('_id,dict_name,dict_type,sort,remark,id,ctime,utime,dictData')
    .sort({
      sort: 1
    })
    .toArray()

  console.log(dict.data[0])

  if (dict && dict.data[0].dictData && dict.data[0].dictData.length > 0) {
    dict.data[0].dictData.forEach(e => {
      var temp = {
        dict_label: e.dict_label,
        dict_value: e.dict_value
      }
      result.push(temp)
    })
  }
  return result
}

app.get('/findForCascade/:dict_type', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  var result = await getDictDataByCode(req.params.dict_type)
  if (result && result.length > 0) {
    for (var i = 0; i < result.length; i++) {
      var temp = await getDictDataByCode(result[i].dict_value)
      if (temp && temp.length > 0) {
        result[i].children = temp
      }
    }
  }

  res.json(Utils.resReturn(result))
})
app.subPath = '/sys/dict/type'
module.exports = app
