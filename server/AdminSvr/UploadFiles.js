// express_demo.js 文件
var express = require('express')
var path = require('path')
var fs = require('fs')
var UUID = require('uuid')
var { Utils, Entity } = require('../dbV1.2/MessageEntitys')
class wxUserFiles extends Entity { }
var crypto = require('crypto')
var cryptoFile = async (filePath) => {
  return await new Promise((resolve, reject) => {
    if (fs.existsSync(filePath)) {
      let stream = fs.createReadStream(filePath)
      let fsHash = crypto.createHash('md5')
      stream.on('data', d => {
        fsHash.update(d)
      })
      stream.on('end', _ => {
        let md5 = fsHash.digest('hex')
        resolve(md5)
      })
    } else {
      reject('file not exists')
    }
  })
}
var app = express()
// https://www.cnblogs.com/jeacy/p/6992435.html
// var mime = require('mime');

var multer = require('multer')
mkdirsSync('/data/service/bingli/tmp/')
mkdirsSync('/ugo/UploadFiles')
app.use(multer({ dest: '/data/service/bingli/tmp/' }).array('multerFile'))
// 递归创建目录 异步方法
function mkdirs (dirname, callback) {
  fs.exists(dirname, function (exists) {
    if (exists) {
      // 是个目录则执行callback方法
      callback()
    } else {
      // 递归调用mkdirs
      /* console.log(dirname);
          console.log(path.dirname(dirname)); */
      mkdirs(path.dirname(dirname), function () {
        fs.mkdir(dirname, callback)
        // console.log('在' + path.dirname(dirname) + '目录创建好' + dirname  +'目录');
      })
    }
  })
}
// 递归创建目录 同步方法
function mkdirsSync (dirname) {
  if (fs.existsSync(dirname)) {
    return true
  } else {
    if (mkdirsSync(path.dirname(dirname))) {
      fs.mkdirSync(dirname)
      return true
    }
  }
}
// mkdirs('./hello/a/b/c',() => {
//     console.log('done');
// });

// mkdirsSync('hello/a/b/c');
const copyFile = (srcFileInfo, destFileInfo, isOverwrite = true, dateStr = '') => {
  return new Promise((resolve, reject) => {
    if (!isOverwrite) {
      if (fs.existsSync(destFileInfo.path)) {
        reject({
          code: 1001,
          msg: 'file write fail, file exists',
          srcFileName: srcFileInfo.originalname,
          destFileName: destFileInfo.originalname
        })
        return
      }
    }
    fs.readFile(srcFileInfo.path, (err, data) => {
      if (err) {
        reject({
          code: 1001,
          msg: 'file read fail',
          srcFileName: srcFileInfo.originalname,
          destFileName: destFileInfo.originalname
        })
      }
      // 写入文件
      mkdirsSync(destFileInfo.dirPath)
      fs.writeFile(destFileInfo.path, data, async (err) => {
        if (err) {
          reject({
            code: 1002,
            msg: 'file write fail',
            srcFileName: srcFileInfo.originalname,
            destFileName: destFileInfo.originalname
          })
          console.log(err)
        } else {
          resolve({
            code: 0,
            msg: 'file write success',
            srcFileName: srcFileInfo.originalname,
            destFileName: dateStr + '/' + destFileInfo.originalname,
            md5: await cryptoFile(destFileInfo.path)
          })
        }
      })
    })
  })
}
const formatDate = function (fmt = 'yyyy-MM-dd') {
  let date = new Date()
  var o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'H+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds(),
    'S+': date.getMilliseconds()
  }
  // 因为date.getFullYear()出来的结果是number类型的,所以为了让结果变成字符串型，下面有两种方法：
  if (/(y+)/.test(fmt)) {
    // 第一种：利用字符串连接符“+”给date.getFullYear()+''，加一个空字符串便可以将number类型转换成字符串。
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      // 第二种：使用String()类型进行强制数据类型转换String(date.getFullYear())，这种更容易理解。
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (('00' + o[k]).substr(String(o[k]).length)))
    }
  }
  return fmt
}

const copyFileToDir = async (srcFileInfo, dirPath, isMd5Name = true, isUuidName = true, isOverwrite = true) => {
  let destFileInfo = {}
  if (isMd5Name) {
    destFileInfo.originalname = await cryptoFile(srcFileInfo.path) + '.' + srcFileInfo.originalname.split('.').pop()
  } else if (isUuidName) {
    destFileInfo.originalname = UUID.v1() + '.' + srcFileInfo.originalname.split('.').pop()
  } else {
    destFileInfo.originalname = srcFileInfo.originalname
  }
  let dateStr = formatDate()
  destFileInfo.path = dirPath + '/' + dateStr + '/' + destFileInfo.originalname
  destFileInfo.dirPath = dirPath + '/' + dateStr
  return copyFile(srcFileInfo, destFileInfo, isOverwrite, dateStr)
}
const MyPromiseAll = (promiseArray) => {
  // 捕获所有异常
  // https://www.jianshu.com/p/3b42565e65ef
  // 同类参考： https://www.jianshu.com/p/70fdf5bc8599
  return new Promise((resolve, reject) => {
    var handlePromise = Promise.all(promiseArray.map(e => {
      return e.catch((err) => {
        return err
      })
    }))
    handlePromise.then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}
const copyFilesToDir = (srcfileInfoList, dirPath, isMd5Name = true, isUuidName = true, isOverwrite = true) => {
  return new Promise((resolve, reject) => {
    let promiseArray = []
    srcfileInfoList.forEach(e => {
      let srcFileInfo = {
        path: e.path,
        originalname: e.originalname
      }
      promiseArray.push(copyFileToDir(srcFileInfo, dirPath, isMd5Name, isUuidName, isOverwrite))
    })
    // 不能捕获所有异常
    // https://www.jianshu.com/p/3b42565e65ef
    MyPromiseAll(promiseArray)
      .then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
  })
}

// app.post('*', function (req, res) {
//   res.json(Utils.resReturn(null, 1001, '上传失败！'))
// })
// var upload = multer({ dest: './upload/' });
app.all('/',
  // upload.array('multerFile'),
  async function (req, res) {
    let dirPath = path.resolve(__dirname, '..')
    dirPath = path.join('', '/ugo/UploadFiles')
    // dirPath = path.join(dirPath, "wxUploadFiles");
    // console.info(req.body)
    // console.info(req.files)
    // console.info(Array.isArray(req.body.files))
    let isMd5Name = req.query.isMd5Name === '1'
    // console.info(isMd5Name)
    let isUuidName = req.query.isUuidName === '1'
    let isOverwrite = req.query.isOverwrite === '1'
    // console.info(isMd5Name, isUuidName, isOverwrite)
    copyFilesToDir(req.files, dirPath, isMd5Name, isUuidName, isOverwrite)
      .then(response => {
        // console.info(response)
        // console.info(req.headers['uid'])
        var promises = []
        response.forEach(async file => {
          if (file.code === 0) {
            promises.push(new wxUserFiles().AddEntity({
              uid: req.headers['uid'],
              md5: file.md5,
              fileNickName: file.srcFileName,
              fileName: file.destFileName
            }))
          }
        })
        Promise.all(promises).then(_ => {
          res.json(Utils.resReturn(response))
        }).catch(_ => {
          res.json(Utils.resReturn(response))
        })
      }).catch(err => {
        console.info(err)
        res.json(Utils.resReturn(err, 1001, '上传失败！'))
      })
    // res.json(Utils.resReturn(null, 0, 'hahah'))
  })
app.all('/test', async function (req, res) {
  res.json({ code: 0, msg: 'test........' + new Date() })
})
app.get('/myfiles', async function (req, res) {
  let result = await new wxUserFiles().where({
    uid: req.headers['uid']
  }).limit(10000).toArray(false)
  // console.info(result)
  // res.json(Utils.resReturn(err, 1001, '上传失败！'))
  res.json(Utils.resReturnResult(result))
})
app.get('/reset', async function (req, res) {
  let result = await new wxUserFiles().where({
    uid: req.headers['uid']
  }).limit(10000).toArray(false)
  let dirPath = path.resolve(__dirname, '..')
  dirPath = path.join(dirPath, 'uploadFiles')

  result.data.forEach(file => {
    let _path = path.join(dirPath, file.fileName)
    if (fs.existsSync(_path)) {
      fs.unlinkSync(_path)
    }
  })
  try {
    result = await new wxUserFiles().DeleteManyEntity({
      uid: req.headers['uid']
    })
    res.json(Utils.resReturnResult(result))
  } catch (err) {
    res.json(Utils.resReturn('err'))
  }
})
app.get('/del/:md5', async function (req, res) {
  let result = await new wxUserFiles().where({
    uid: req.headers['uid'],
    md5: req.params.md5
  }).limit(10000).toArray(false)
  let dirPath = path.resolve(__dirname, '..')
  dirPath = path.join(dirPath, 'uploadFiles')

  // console.info(req)
  // res.json(Utils.resReturn(result.data.length, 0, '上传失败！'))
  result.data.forEach(file => {
    let _path = path.join(dirPath, file.fileName)
    if (fs.existsSync(_path)) {
      fs.unlinkSync(_path)
    }
  })
  try {
    result = await new wxUserFiles().DeleteEntity({
      uid: req.headers['uid'],
      md5: req.params.md5
    })
    res.json(Utils.resReturnResult(result))
  } catch (err) {
    res.json(Utils.resReturn('err'))
  }
})
app.subPath = '/UploadFiles'
module.exports = app
