// express_demo.js 文件
var express = require('express')
var app = express()
const WebSocket = require('ws')
var _ = require('lodash')
var qs = require('qs')
const {
  exec,
  spawn
} = require('child_process')
var iconv = require('iconv-lite')
var encoding = 'cp936'
var binaryEncoding = 'binary'
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')

const wsServer = new WebSocket.Server({
  port: 998
})
var allWebScoketClient = []
let formatDate = (value = null, fmt = 'yyyy-MM-dd hh:mm:ss') => {
  let getDate = value === null ? new Date() : new Date(value)
  let o = {
    'M+': getDate.getMonth() + 1,
    'd+': getDate.getDate(),
    'h+': getDate.getHours(),
    'm+': getDate.getMinutes(),
    's+': getDate.getSeconds(),
    'q+': Math.floor((getDate.getMonth() + 3) / 3),
    'S': getDate.getMilliseconds()
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}
let child_process = null
const execCmd = (cmdStr, ws) => {
  try {
    ws.send('hello')
    if (child_process) {
      child_process.kill()
    }
    child_process = exec(cmdStr, {
      cwd: '/ugo/UploadFiles',
      maxBuffer: 1024 * 1024 * 1024,
      encoding: binaryEncoding
    })

    child_process.stdout.on('data', (data) => {
      ws.send(JSON.stringify({
        code: 0,
        msg: iconv.decode(new Buffer.from(data, binaryEncoding), encoding).replace('\r\n', '') + formatDate()
      }))
      console.info(iconv.decode(new Buffer.from(data, binaryEncoding), encoding).replace('\r\n', '') + formatDate())
    })
    child_process.stderr.on('data', (data) => {
      ws.send(JSON.stringify({
        code: 0,
        msg: iconv.decode(new Buffer.from(data, binaryEncoding), encoding).replace('\r\n', '') + formatDate()
      }))
    })
    child_process.on('close', (code) => {
      ws.send(JSON.stringify({
        code: 2,
        msg: 'close'
      }))
    })
  } catch (err) {
    ws.send(JSON.stringify({
      code: 1,
      data: err
    }))
  }
}
// execCmd('pm2 monitor', null)
wsServer.on('connection', async (ws, req) => { // 用于获取连接时候的参数 eg: ws://ip:port/path?token=xxxxxx
  let url = req.url
  // let header = req.headers['aaa']
  // console.log('url is ' + url); // "/path?token=xxxxxx"
  let prarms = qs.parse(_.split(url, '?')[1]) // token=xxxxxx
  console.log('url is ' + JSON.stringify(prarms)) // {token:xxxxxxx}
  let result = prarms // await new App().Verify(prarms)
  console.info(result)

  // new User().drop();
  ws.on('message', function (message) {
    console.info(message)
    ws.send(message)
    // let json = JSON.parse(message)
    // console.info(json)
    // execCmd('dir', ws)
    // execCmd('ping 127.0.0.1', ws)
    execCmd('java -version', ws)
    // if (json.cmd === 1) {}
  })

  ws.on('error', function (error) {
    console.log('错误' + error)
  })

  ws.on('open', function (e) {
    ws.send('open')
  })

  ws.on('close', function (e) {
    console.log('close')
    let index = allWebScoketClient.findIndex(e => {
      if (e) {
        return e.uid === prarms.uid && e.machineCode === prarms.machineCode
      }
    })
    if (index !== -1) delete allWebScoketClient[index]
    // _.pull(list, ws);
    // console.log('在线人数' + list.length);
  })
})

app.subPath = '/admin/executeCmd'
module.exports = app
