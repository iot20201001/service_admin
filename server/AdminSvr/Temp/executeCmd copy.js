/* eslint-disable new-cap */
const vm = require('vm')
const script = new vm.Script('return add(a,b)', {
  timeout: 50
})
const sandbox = {
  m: 1,
  n: 2,
  add (m, n) {
    return m + n
  }
}
const context = new vm.createContext(sandbox)
let ok = script.runInContext(context)
console.info(ok)
