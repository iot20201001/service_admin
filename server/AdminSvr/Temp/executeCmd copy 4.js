// express_demo.js 文件
var express = require('express')
var app = express()
const WebSocket = require('ws')
var _ = require('lodash')
var qs = require('qs')
const {
  exec,
  spawn
} = require('child_process')
var iconv = require('iconv-lite')
const fs = require('fs')
var encoding = 'cp936'
var binaryEncoding = 'binary'
var {
  Utils,
  Entity
} = require('../../dbV1.2/MessageEntitys')
let formatDate = (value = null, fmt = 'yyyy-MM-dd hh:mm:ss') => {
  let getDate = value === null ? new Date() : new Date(value)
  let o = {
    'M+': getDate.getMonth() + 1,
    'd+': getDate.getDate(),
    'h+': getDate.getHours(),
    'm+': getDate.getMinutes(),
    's+': getDate.getSeconds(),
    'q+': Math.floor((getDate.getMonth() + 3) / 3),
    'S': getDate.getMilliseconds()
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}
let child_process = null
const execCmd = (cmdStr) => {
  try {
    child_process = spawn('pm2', ['monitor'],
      {
        // silent: true,
        stdio: ['pipe', 'ipc', 'pipe'],
        maxBuffer: 1024 * 1024 * 1024,
        encoding: binaryEncoding
      }
    )
    child_process.stdout.on('data', (data) => {
      console.info(iconv.decode(new Buffer.from(data, binaryEncoding), encoding).replace('\r\n', '') + formatDate())
    })
    child_process.stderr.on('data', (data) => {
      console.info(iconv.decode(new Buffer.from(data, binaryEncoding), encoding).replace('\r\n', '') + formatDate())
    })
    child_process.on('close', (code) => {
      console.info(code, 'close..............')
    })
  } catch (err) {
    console.info(err)
  }
}
execCmd('pm2 monitor', null)
// execCmd('ping 127.0.0.1', null)
