// express_demo.js 文件
var express = require('express')
var app = express()
const WebSocket = require('ws')
var _ = require('lodash')
var qs = require('qs')
const {
  exec
} = require('child_process')
var iconv = require('iconv-lite')
var encoding = 'cp936'
var binaryEncoding = 'binary'
var {
  Utils,
  Entity
} = require('../../dbV1.2/MessageEntitys')

const wsServer = new WebSocket.Server({
  port: 998
})
var allWebScoketClient = []
let formatDate = (value = null, fmt = 'yyyy-MM-dd hh:mm:ss') => {
  let getDate = value === null ? new Date() : new Date(value)
  let o = {
    'M+': getDate.getMonth() + 1,
    'd+': getDate.getDate(),
    'h+': getDate.getHours(),
    'm+': getDate.getMinutes(),
    's+': getDate.getSeconds(),
    'q+': Math.floor((getDate.getMonth() + 3) / 3),
    'S': getDate.getMilliseconds()
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}
let child_process = null
const execCmd = (cmdStr, ws) => {
  try {
    if (child_process) {
      child_process.kill()
    }
    child_process = exec(cmdStr)
    child_process.stdout.on('data', (data) => {
      ws.send({
        code: 0,
        msg: iconv.decode(new Buffer.from(data, binaryEncoding), encoding).replace('\r\n', '') + formatDate()
      })
    })
    child_process.stderr.on('data', (data) => {
      ws.send({
        code: 0,
        msg: iconv.decode(new Buffer.from(data, binaryEncoding), encoding).replace('\r\n', '') + formatDate()
      })
    })
    child_process.on('close', (code) => {
      ws.send({
        code: 2,
        msg: 'close'
      })
    })
  } catch (err) {
    ws.send({
      code: 1,
      data: err
    })
  }
}
wsServer.on('connection', async (ws, req) => { // 用于获取连接时候的参数 eg: ws://ip:port/path?token=xxxxxx
  let url = req.url
  // let header = req.headers['aaa']
  // console.log('url is ' + url); // "/path?token=xxxxxx"
  let prarms = qs.parse(_.split(url, '?')[1]) // token=xxxxxx
  console.log('url is ' + JSON.stringify(prarms)) // {token:xxxxxxx}
  let result = prarms // await new App().Verify(prarms)
  if (!result) ws.close()
  else {
    // 添加入队列
    // result = await new User().FindEntity({
    //     uid: prarms.uid
    // })
    // result = await new UserLogins().SaveEntity({
    //     uid: prarms.uid,
    //     machineCode: "83c4cc00-450f-11ea-8b4d-ef050416c969",
    //     ws
    // })
    // if (!prarms.machineCode) { prarms.machineCode = Utils.uuid_v1() }
    let index = allWebScoketClient.findIndex(e => {
      if (e) {
        return e.uid === prarms.uid && e.machineCode === prarms.machineCode
      }
    })
    // 找到直接更新
    if (index !== -1) {
      allWebScoketClient[index].ws = ws
    } else {
      // 找到一个为 undefined 的 赋值
      index = allWebScoketClient.findIndex(e => {
        return e === undefined
      })
      // 找到直接赋值
      if (index !== -1) {
        allWebScoketClient[index] = {
          uid: prarms.uid,
          machineCode: prarms.machineCode,
          ws
        }
      }
      // 未找到则添加
      else {
        allWebScoketClient.push({
          uid: prarms.uid,
          machineCode: prarms.machineCode,
          ws
        })
      }
    }
    try {
      allWebScoketClient.forEach(e => {
        e.ws.send(e.machineCode + '---------------------------->>>>>>>' + allWebScoketClient.length)
      })
    } catch (err) {
      console.info(err)
    }
    // new User().drop();
    ws.on('message', function incoming (message) {
      let json = JSON.parse(message)
      console.info(json)
      if (json.cmd === 1) {}
    })

    ws.on('error', function (error) {
      console.log('错误' + error)
    })

    ws.on('open', function (e) {
      ws.send('open')
    })

    ws.on('close', function (e) {
      console.log('close')
      let index = allWebScoketClient.findIndex(e => {
        if (e) {
          return e.uid === prarms.uid && e.machineCode === prarms.machineCode
        }
      })
      if (index !== -1) delete allWebScoketClient[index]
      // _.pull(list, ws);
      // console.log('在线人数' + list.length);
    })
  }
})

app.subPath = '/admin/executeCmd'
module.exports = app
