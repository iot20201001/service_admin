// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class PreLibMarking extends Entity {}
class PreLibMarkingHistory extends Entity {}
// 查找
app.get('/page', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // 查找当前租户信息 如果租户是系统租户则能看到所有的租户参数
  let skip = 0
  let limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  console.info(req.query)
  if (req.query.creator_name) {
    search = {
      ...search,
      ...{
        creator_name: eval(`/${req.query.creator_name}/`)
      }
    }
  }
  if (req.query.status) {
    search = {
      ...search,
      ...{
        status: Number(req.query.status)
      }
    }
  }
  let opt = new SysLogLogin(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  } else {
    opt.sort({
      ctime: -1
    })
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 批量删除
app.post('/recvMarkingData', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  new PreLibMarking(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.post('/markingHistory', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  new PreLibMarkingHistory(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  new PreLibMarkingHistory(tenant_code).DeleteManyEntity(data)
  new PreLibMarking(tenant_code).DeleteEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.post('/clearAll', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new PreLibMarkingHistory(tenant_code).drop()
  new PreLibMarking(tenant_code).drop()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/queryAllMesId', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new PreLibMarking(tenant_code).toArray()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.post('/queryPlan', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let data = req.body
  new PreLibMarkingHistory(tenant_code).where(data).toArray()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.subPath = '/prelib/marking'
module.exports = app