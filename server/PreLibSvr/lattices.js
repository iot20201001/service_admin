var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class SysDept extends Entity {}
class LibLattices extends Entity {}

let Init = async _ => {
  // 查询库位时需要加载子部门的库位信息
  try {
    let dept = await new SysDept('1001').FindOneEntity({
      id: '8bde67f0-a083-11ea-b42e-7dab4e78f954'
    })
    if (dept.code !== 0 || !dept.data) return
    dept = dept.data
    console.info(dept.id)
    for (var huojia = 1; huojia < 4; huojia++) {
      let _huojia = await new LibLattices('1001').AddEntity({
        id: Utils.UUID_V1(),
        pid: '0',
        TREE_LEVEL: 3,
        TREE_IS_LEAF: 0,
        TREE_PATH: `${dept.name}-${huojia}`,
        dept_id: dept.id,
        pids: ['0']
      })
      for (var row = 1; row < 7; row++) {
        let _row = await new LibLattices('1001').AddEntity({
          id: Utils.UUID_V1(),
          pid: _huojia.data.id,
          TREE_LEVEL: 4,
          TREE_IS_LEAF: 0,
          TREE_PATH: `${dept.name}-${huojia}-${row}`,
          dept_id: dept.id,
          pids: ['0', _huojia.data.id]
        })
        for (var col = 1; col < 12; col++) {
          let _col = await new LibLattices('1001').AddEntity({
            id: Utils.UUID_V1(),
            pid: _row.data.id,
            TREE_LEVEL: 5,
            TREE_IS_LEAF: 1,
            TREE_PATH: `${dept.name}-${huojia}-${row}-${col}`,
            dept_id: dept.id,
            pids: ['0', _huojia.data.id, _row.data.id]
          })
        }
      }
    }
  } catch (err) {
    console.info(err)
  }
}

// Init()
let params = {
  dept_id: {
    $in: ['86d46d90-a083-11ea-b42e-7dab4e78f954', '8bde67f0-a083-11ea-b42e-7dab4e78f954', '3a8e6ba0-a31e-11ea-ac2c-7700958f46c2', '406504d0-a31e-11ea-ac2c-7700958f46c2', '956a19e0-a083-11ea-b42e-7dab4e78f954']
  },
  TREE_LEVEL: 3
}
new LibLattices('1001').where(params)
  .toArray()
  .then(dbRes => {
    console.warn(dbRes)
  })
/**
精准到库房
库房-货架-层-格
【举例】
XJ                    厂房/库房
  XJA                 子库房
    XJA-1             货架
      XJA-1-1         层
        XJA-1-1-1     格
  XJB               **子库房
    XJB1              **子库房
      XJB1-1          货架
        XJB1-1-1      层
          XJB1-1-1-1  格
    XJB2              子库房
      XJB2-1          货架
        XJB2-1-1      层
          XJB2-1-1-1  格
*/
