const {
  MongoClient,
  ObjectId
} = require('mongodb')
const {
  Utils
} = require('../dbV1.2/MongoDB')
var MongoDbConfig = {
  dbOptions: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // poolSize: 50,
    // ssl: false,
    // keepAlive: 240,
    // auto_reconnect: true,
    connectTimeoutMS: 300000,
    socketTimeoutMS: 300000,
    promiseLibrary: global.Promise
  },
  // mongodb: //ydsy:ydsy20200215@172.101.108.172:27017/?authSource=ydsy&readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false
  // url: 'mongodb://ydsy:ydsy20200215@172.101.108.172:27017',
  // url: "mongodb://ydsy:ydsy20200215@mongo.ugo.com.cn:27017",
  url: 'mongodb://192.168.200.1:27017,192.168.200.1:27018,192.168.200.1:27019/?replicaSet=testrs',
  dbName: 'tenant'
}
var ErrHelper = { // ExecptionHelper
  OK: {
    code: 0,
    msg: '成功！'
  },
  DbConnectError: {
    code: 90001,
    msg: '数据库连接失败！'
  },
  IsExistError: {
    code: 90001,
    msg: '数据已存在！'
  },
  DataVerifyError: {
    code: 90002,
    msg: '数据校验失败！'
  },
  DataTypeVerifyError: {
    code: 90002,
    msg: '数据类型或格式异常！'
  },
  InsertOneError: {
    code: 90101,
    msg: '插入一条数据失败！'
  },
  InsertManyError: {
    code: 90102,
    msg: '插入多条数据失败！'
  },
  DeleteOneError: {
    code: 90111,
    msg: '删除一条数据失败！'
  },
  DeleteManyError: {
    code: 90112,
    msg: '删除多条数据失败！'
  },
  DeleteManyByIdsError: {
    code: 90113,
    msg: '存在删除失败的数据！'
  },
  UpdateOneError: {
    code: 90121,
    msg: '更新一条数据失败！'
  },
  UpdateManyError: {
    code: 90122,
    msg: '更新多条数据失败！'
  },
  FindOneError: {
    code: 90131,
    msg: '查找一条数据失败！'
  },
  FindManyError: {
    code: 90132,
    msg: '查找多条数据失败！'
  },
  FindError: {
    code: 90141,
    msg: '查找失败！'
  },
  DropError: {
    code: 90141,
    msg: '删表失败！'
  },
  AggregateError: {
    code: 90151,
    msg: '聚合查询失败！'
  }
}
class MongoDB {
  static db = {}
  static dbConfig = {}
  constructor(defaultConfig, tenant = 'Tenant_Default') {
    this.tenant = tenant
    defaultConfig.dbName = tenant
    MongoDB.dbConfig[tenant] = defaultConfig
    if (!MongoDB.db[tenant]) {
      this.connect()
    }
  }
  static BuildObjectId(jsonObject) {
    // console.info(jsonObject)
    if (jsonObject._id) {
      jsonObject._id = ObjectId(jsonObject._id)
    }
    // console.info(jsonObject)
    return jsonObject
  }
  connect() {
    return new Promise((resolve, reject) => {
      if (MongoDB.db[this.tenant]) return resolve(MongoDB.db[this.tenant])
      MongoClient.connect(MongoDB.dbConfig[this.tenant].url, MongoDB.dbConfig[this.tenant].dbOptions, (err, db) => {
        if (err) return reject(err)
        if (MongoDB.db[this.tenant]) db.close()
        else {
          MongoDB.db[this.tenant] = {
            db,
            dbo: db.db(MongoDB.dbConfig[this.tenant].dbName)
          }
        }
        resolve(MongoDB.db[this.tenant])
      })
    })
  }
  async insertOne(tableName, jsonObject, isObjectId = true) {
    let r = await Utils.VerifyData(jsonObject)
    if (r.code !== 0) return r
    if (isObjectId) {
      jsonObject = MongoDB.BuildObjectId(jsonObject)
    }
    jsonObject.ctime = Date.now()
    jsonObject.utime = Date.now()
    return new Promise((resolve, reject) => {
      // 优化数据库操作，不加setTimeout，会出现假死的状态，相当于给了mongodb喘息的机会
      // setTimeout(async _ => {
      this.connect().then(_ => {
        MongoDB.db[this.tenant].dbo
          .collection(tableName)
          .insertOne(jsonObject, (err, res) => {
            if (err) {
              reject(Utils.ReturnDbResult(
                err,
                ErrHelper.InsertOneError
              ))
            } else {
              // res.data.ops [addObject]
              resolve(Utils.ReturnDbResult(res, ErrHelper.OK))
            }
          })
      }).catch(err => {
        reject(Utils.ReturnDbResult(err, ErrHelper.DbConnectError))
      })
      // }, 1);
    })
  }
}

class haha {
  constructor() {
    this = {
      ...this,
      ...new MongoDB(MongoDbConfig)
    }
  }
}
let mydb = new haha()
let ok = async _ => {
  console.time('label')
  var js = []
  for (var i = 0; i < 10; i++) {
    js.push({
      code1: i
    })
    mydb.insertOne('ok', {
        code1: i
      })
      .then(res => {})
      .catch(err => {
        console.error(err)
      })
  }
  console.timeEnd('label')
  console.info(js.length * sizeof(js[0]))
}
ok()