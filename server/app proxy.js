var express = require('express')
var app = express()
// https://www.npmjs.com/package/http-proxy-middleware
// https://developer.aliyun.com/mirror/npm/package/http-proxy-middleware
var proxy = require('http-proxy-middleware')

var options = {
  target: 'http://localhost:8080',
  changeOrigin: true,
  ws: true,
  pathRewrite: {
    '^/a8080': ''
  }
}

var exampleProxy = proxy('**', options)
app.use('/a8080', exampleProxy)
var bodyParser = require('body-parser')

// 返回的对象是一个键值对，当extended为false的时候，键值对中的值就为'String'或'Array'形式，为true的时候，则可为任何数据类型。
app.use(bodyParser.urlencoded({
  extended: true
}))
// parse application/json
app.use(bodyParser.json())

app.all('*', async (req, res, next) => {
  // https://www.jb51.net/article/137981.htm
  // 设置允许跨域的域名，*代表允许任意域名跨域
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  // 允许的header类型
  res.header('Access-Control-Allow-Headers', '*')
  // res.header('Access-Control-Allow-Headers', 'content-type,token,tenant_code, Accept-Language,X-Requested-With,TerminalType,foreEndVersion')
  // 跨域允许的请求方式
  res.header('Access-Control-Allow-Methods', '*')
  console.info(req.path)
  next()
})
app.get('/hello', (req, res) => {
  res.json({
    code: 0
  })
})
// http://localhost:12345/hello
var server = app.listen(12345, function () {
  var host = server.address().address
  var port = server.address().port
  app.verifyTokenUrl = (`http://${host}:${port}/OpenVerifyToken`)
  console.log('应用实例，访问地址为 http://%s:%s', host, port)
})
