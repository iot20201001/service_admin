var mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27117/test', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
var Schema = mongoose.Schema
const kittySchema = new Schema({
  name: Number
})
const Kitten = mongoose.model('Kitten', kittySchema)
let main = async () => {
  const db = mongoose.connection
  db.on('error', console.error.bind(console, 'connection error:'))
  db.once('open', function () {
    // we're connected!
    console.info("we're connected!")
    let tt = new Kitten()
    tt.name = 'a1234'
    tt.save()
  })
}
main()

// var blogSchema = new Schema({
//   title: String, // String is shorthand for {type: String}
//   author: String,
//   body: String,
//   comments: [{
//     body: String,
//     date: Date
//   }],
//   date: {
//     type: Date,
//     default: Date.now
//   },
//   hidden: Boolean,
//   meta: {
//     votes: Number,
//     favs: Number
//   }
// })