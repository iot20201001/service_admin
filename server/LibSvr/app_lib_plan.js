// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class LibPlan extends Entity {}
class LibPlanHistory extends Entity {}
class LibStockLattices extends Entity {}
app.post('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  data.packageCode = `PT-${Utils.FormatDate(null, 'yyyy-MM-dd-hh-mm-ss-S')}`
  await new LibPlanHistory(tenant_code).AddEntity(data)
    .then(dbRes => {}).catch(err => {
      return res.json(err)
    })
  new LibPlan(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})

app.get('/getPlan', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // let uid = req.headers['uid'] || ''
  // let data = req.body
  // data.id = Utils.UUID_V1()
  // data.uid = uid
  let params = {}
  // console.error(req.query)
  if (req.query.needId) params.needId = req.query.needId
  new LibPlan(tenant_code).where(params).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/Computed', (req, res) => {
  // 计算后需要把零件置为占用状态【物码直接占用，批码按数量占用】
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  let params = {}
  // console.error(req.query)
  if (req.query.needId) params.needId = req.query.needId
  new LibPlan(tenant_code).where(params).toArray(true)
    .then(async dbRes => {
      // 未匹配零件 partCode 标识
      // eslint-disable-next-line camelcase
      let t_partCodeList = [null, undefined, '缺件', '', '-', '待计算']
      // 多配套包同时配套需要整合图号、机型、批次、架次等信息
      let t = dbRes.data[0]
      // 即将要处理的零件列表
      let tt_tt = t.bomList
      if (t.partList && t.partList.length > 0) {
        tt_tt = t.partList
      }
      let tempPartList = JSON.parse(JSON.stringify(tt_tt))
      // 最终结果放这里
      let rr = []
      // 存放缺件图号
      let tempList = tempPartList.filter(x => t_partCodeList.includes(x.partCode) || !x.partCode)
      // 找出即将计算的图号，数量等信息【目前不考虑机型、批次、架次】
      for (var i = 0; i < tempList.length; i++) {
        let tt = await new LibStockLattices(tenant_code).where({
          partNo: tempList[i].partNo
        }).toArray()
        // 如果找到对应图号
        if (tt.code === 0 && tt.data.length > 0) {
          // 此处应该按未占用的数量计算【可用数量 = 当前数量 - 减去锁定数量】
          // 去除可用数量为0的零件 即 符合要求的零件
          tt.data.forEach(x => {
            if (!(x.lockNumber > 0)) x.lockNumber = 0
          })
          // ★★★★★★★★此处需要过滤指定机型架次的零件【排除不属于该机型架次的零件】
          let okPartList = tt.data.filter(y => (Number(y.number + '') - Number(y.lockNumber + '')))
          let okNumber = 0
          // 计算符合要求的零件的数量
          if (okPartList.length > 0) {
            okNumber = okPartList.map(x => x.number - x.lockNumber).reduce((a, b) => a + b)
          }
          // 如果有可以使用的零件
          if (okNumber > 0) {
            // 如果可用数量小于等于需求数量
            if (tempList[i].partCount >= okNumber) {
              // 计算缺件数量
              tempList[i].partCount -= okNumber
              // 处理匹配零件
              tt.data.forEach(x => {
                // 计算当前可用数量
                x.partCount = Number(x.number + '') - Number(x.lockNumber + '')
                // 锁定数量加上当前使用数量
                x.lockNumber += x.partCount
              })
              rr.push(...tt.data)
              // 还存在缺件
              if (tempList[i].partCount > 0) {
                rr.push({
                  partCode: '缺件',
                  partName: tempList[i].partName,
                  partCount: tempList[i].partCount,
                  partNo: tempList[i].partNo
                })
              }
            } else {
              // 处理匹配零件
              // 可用数量大于需求数量
              let total = 0
              for (var j = 0; j < tt.data.length; j++) {
                if (total <= tempList[i].partCount) {
                  let number = tempList[i].partCount - total
                  // 实际可用数量
                  let tNumber = Number(tt.data[j].number + '') - Number(tt.data[j].lockNumber + '')
                  if (tNumber > 0) {
                    total += Math.min(tNumber, number)
                    rr.push({
                      ...tt.data[j],
                      partCount: Math.min(tNumber, number), // tNumber > number ? number : tNumber,
                      lockNumber: tt.data[j].lockNumber + Math.min(tNumber, number) // (tNumber > number ? number : tNumber)
                    })
                  }
                }
              }
              // 不存在缺件了
            }
          } else {
            // 没有可使用的新零件
            // 还原缺件
            rr.push(tempList[i])
          }
        }
      }
      if (rr.length > 0) {
        rr.forEach(async x => {
          // 前端需要
          x.isComputed = true
          x.isScanOK = false
          // 排除缺件零件
          if (t_partCodeList.includes(x.partCode)) return
          // 更改占用(锁定)状态
          // 此处还需要进行异常判断【锁定数量超限的话就异常】
          await new LibStockLattices(tenant_code).UpdateEntity({
            id: x.id
          }, x)
        })
      }
      // ======处理之前匹配的零件============================================================================================
      // 如果上次有已经计算过的零件
      let oldPartList = tempPartList.filter(x => !t_partCodeList.includes(x.partCode))
      // let oldPartList = t.partList || []
      // Start 此段操作之后  有效数据全部存于t.partList
      // 上次有计算
      if (oldPartList.length > 0) {
        // 本次有新增
        if (rr.length > 0) {
          t.partList = [...oldPartList, ...rr]
        }
        // 本次无新增
        else {
          t.partList = oldPartList
        }
      } else {
        // 上次无计算
        // 本次有新增
        if (rr.length > 0) {
          t.partList = rr
        }
        // 本次无新增
        else {
          // 这里不处理缺件的，缺件的稍后统一处理
          if (!t.partList) t.partList = []
          // t.partList = tempPartList
        }
      }
      // End 此段操作之后  有效数据全部存于t.partList
      let partNos = Array.from(new Set(t.partList.map(x => x.partNo)))
      tempPartList.forEach(x => {
        if (!partNos.includes(x.partNo)) {
          t.partList.push({
            partCode: '缺件',
            partName: x.partName,
            partCount: x.partCount,
            partNo: x.partNo
          })
        }
      })
      // 去掉匹配上数量为0的零件
      // 此步骤已不需要
      t.partList = t.partList.filter(x => x.partCount > 0)
      await new LibPlan(tenant_code).UpdateEntity({
        _id: t._id
      }, t)
        .then(dbRes => {}).catch(err => {
          res.json(err)
        })
      // 处理bomList
      //
      res.json(dbRes)
      // console.warn(dbRes)
    }).catch(err => {
      res.json(err)
      console.info(err)
    })
})
app.put('/Complete', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.updater = uid
  new LibPlan(tenant_code).UpdateEntity({
    _id: data._id
  }, data)
    .then(dbRes => {
      res.json(Utils.ReturnOK(dbRes))
    }).catch(err => {
      res.json(err)
    })
})
app.put('/resetComputed', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let t_partCodeList = [null, undefined, '缺件', '', '-', '待计算']
  let needId = req.body.needId
  let resetPartList = req.body.resetPartList
  // delPartList
  let plan = await new LibPlan(tenant_code).FindOneEntity({
    needId: needId
  })
  if (plan.code !== 0 || !plan.data) return res.json(Utils.ReturnError(needId, '配套包不存在了'))
  plan = plan.data
  for (var i = 0; i < resetPartList.length; i++) {
    // 找到待删除
    let delIndex = plan.partList.findIndex(x => x.partCode === resetPartList[i].partCode &&
      x.partCount === resetPartList[i].partCount &&
      x.leafLattices === resetPartList[i].leafLattices)
    // 处理缺件
    let x = plan.partList.find(x => x.partNo === resetPartList[i].partNo && t_partCodeList.includes(x.partCode))
    if (x) {
      // 之前存在缺件，直接修改
      x.partCount += resetPartList[i].partCount
      // 删除当前
      plan.partList.splice(delIndex, 1)
    } else {
      // 之前不存在缺件
      let obj = {
        partCode: '缺件',
        partName: resetPartList[i].partName,
        partCount: resetPartList[i].partCount,
        partNo: resetPartList[i].partNo
      }
      // 删除当前，添加缺件
      plan.partList.splice(delIndex, 1, obj)
    }
    // 恢复数量锁定
    let ttt = await new LibStockLattices(tenant_code).FindOneEntity({
      id: resetPartList[i].id
    })
    if (ttt.code === 0 && ttt.data) {
      ttt.data.lockNumber -= resetPartList[i].partCount
      await new LibStockLattices(tenant_code).UpdateEntity({
        id: resetPartList[i].id
      }, ttt.data)
    }
  }
  // 保存新计划
  new LibPlan(tenant_code).UpdateEntity({
    needId: needId
  }, plan)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.subPath = '/lib/plan'
module.exports = app

/*
入库

// 前端逻辑
  查询本地是否已有数据，有则不做任何操作
  否则
      查询零件库存表，如已存在则提示相应状态
      否则
        查询开合格证信息，若找到合格的零件信息，则展示
        【查出相关库位信息 默认位置】
      否则
        提示未找到对应编码的零件
*/
