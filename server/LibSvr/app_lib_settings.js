// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class LibSettings extends Entity {}
app.post('/', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let dept_id = req.headers['dept_id'] || ''
  let data = req.body
  if (!data.id) data.id = Utils.UUID_V1()
  if (!data.creator) data.creator = uid
  data.updater = uid
  data.dept_id = dept_id
  new LibSettings(tenant_code).SaveEntity(data, {
    dept_id
  })
    .then(dbRes => {
      res.json(Utils.ReturnOK(dbRes))
    }).catch(err => {
      res.json(err)
    })
})

app.get('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let dept_id = req.headers['dept_id'] || ''
  new LibSettings(tenant_code).FindOneEntity({
    dept_id
  }).then(dbRes => {
    res.json(dbRes)
  }).catch(err => {
    res.json(err)
  })
})
app.subPath = '/lib/settings'
module.exports = app

/*
入库

// 前端逻辑
  查询本地是否已有数据，有则不做任何操作
  否则
      查询零件库存表，如已存在则提示相应状态
      否则
        查询开合格证信息，若找到合格的零件信息，则展示
        【查出相关库位信息 默认位置】
      否则
        提示未找到对应编码的零件
*/
