// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class PreLibMarking extends Entity {}
class PreLibMarkingHistory extends Entity {}
class PreLibCertificate extends Entity {}
class LibLattices extends Entity {}
class SysUser extends Entity {}
class SysDept extends Entity {}
class LibPlan extends Entity {}
class LibOutStock extends Entity {}
class LibOutStockHistory extends Entity {}
class LibStockLattices extends Entity {}

app.get('/FindPartByPackageCode/:packageCode', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let dept_id = req.headers['dept_id'] || ''
  let packageCode = req.params.packageCode
  new LibPlan(tenant_code).FindOneEntity({
      packageCode
    })
    .then(async dbRes => {
      if (dbRes.data) {
        res.json(dbRes)
      } else {
        let packageOutStock = await new LibOutStock(tenant_code).FindOneEntity({
          packageCode
        })
        if (packageOutStock.data) {
          packageOutStock.data.statusOutStock = true
          res.json(packageOutStock)
        } else
          res.json(Utils.ReturnError(null, '配套包信息不存在'))
      }
    }).catch(err => {
      console.info(err)
    })
})

let getSubdept_ids = async function _getSubdept_ids(tenant_code, dept_id, name) {
  return new Promise(async (resolve, reject) => {
    let subDepts = await new SysDept(tenant_code).where({
      pid: dept_id
    }).toArray()
    if (subDepts.code === 0 && subDepts.data.length > 0) {
      let asyncFun = []
      subDepts.data.forEach(x => {
        // console.info(x.id, x.name)
        asyncFun.push(_getSubdept_ids(tenant_code, x.id, x.name))
      })
      Promise.all(asyncFun).then(x => {
        // console.info(x.length)
        resolve([dept_id, ...x].join(','))
      })
    } else {
      resolve([dept_id])
    }
  })
}
// let ok = async _ => {
//   let rr = await getSubdept_ids('1001', '86d46d90-a083-11ea-b42e-7dab4e78f954', 'XJ')
//   console.warn(JSON.stringify(rr.split(',')))
// }
// ok()
app.get('/getLatticesByPid', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let pid = req.query.pid
  console.info(req.query, req.params)
  let user = await new SysUser(tenant_code).FindOneEntity({
    id: uid
  })
  if (user.code !== 0 || !user.data) return Utils.ReturnError(null, '用户不存在！')
  user = user.data
  let params = null
  if (pid !== undefined) {
    params = {
      pid: pid
    }
  } else {
    let dept_ids = await getSubdept_ids(tenant_code, user.dept_id, 'XJ')
    params = {
      dept_id: {
        $in: dept_ids.split(',')
        // ['86d46d90-a083-11ea-b42e-7dab4e78f954', '8bde67f0-a083-11ea-b42e-7dab4e78f954', '3a8e6ba0-a31e-11ea-ac2c-7700958f46c2', '406504d0-a31e-11ea-ac2c-7700958f46c2', '956a19e0-a083-11ea-b42e-7dab4e78f954']
      },
      TREE_LEVEL: 3
    }
  }
  new LibLattices(tenant_code).where(params)
    .toArray()
    .then(dbRes => {
      // console.warn(params)
      if (dbRes.data) {
        res.json(Utils.ReturnOK(dbRes.data))
      } else {
        res.json(Utils.ReturnError(null, '库位信息不存在'))
      }
    }).catch(err => {
      res.json(Utils.ReturnError(err, '库位信息不存在'))
      // console.info(err)
    })
})
app.post('/out', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let dept_id = req.headers['dept_id'] || ''
  let packageCode = req.body.packageCode
  new LibPlan(tenant_code).FindOneEntity({
      packageCode
    })
    .then(async dbRes => {
      if (dbRes.data) {
        let package = dbRes.data
        // 过滤掉已扫码的零件
        let okPartList = package.partList.filter(x => x.isComplete)
        if (okPartList.length === 0) {
          return res.json(Utils.ReturnError(null, '本次操作未检测到已扫码零件'))
        }
        let newPartList = package.partList.filter(x => !x.isComplete)
        let packageOut = {
          ...package,
          uid,
          dept_id,
          id: Utils.UUID_V1(),
          partList: okPartList
        }
        delete packageOut._id
        let ok = await new LibOutStock(tenant_code).SaveEntity(packageOut, {
          id: packageOut.id
        })
        if (newPartList.length > 0) {
          package.packageCode = `PT-${Utils.FormatDate(null, 'yyyy-MM-dd-hh-mm-ss-S')}`
          package.partList = newPartList
          ok = await new LibPlan(tenant_code).UpdateEntity({
            packageCode
          }, package)
          res.json(Utils.ReturnOK(null, '成功！'))
        } else {
          ok = await new LibPlan(tenant_code).DeleteEntity({
            packageCode
          })
          res.json(Utils.ReturnOK(null, '成功！'))
        }
      } else {
        res.json(Utils.ReturnError(null, '配套包信息不存在'))
      }
    }).catch(err => {
      console.info(err)
    })
})
app.subPath = '/lib/outstock'
module.exports = app