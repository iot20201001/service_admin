// // express_demo.js 文件
// var express = require('express')
// var app = express()
var {
  Utils,
  Entity
} = require('../../dbV1.2/MessageEntitys')
// class Test extends Entity {}

var MongoClient = require('mongodb').MongoClient
var url = 'mongodb://localhost:27017/runoob'
MongoClient.connect(url, {
  useNewUrlParser: true
}, async (err, client) => {
  if (err) throw err
  console.log('数据库已创建')
  var db = client.db('runoob')
  var session = client.startSession()
  await session.startTransaction()
  await db.collection('tableName').insertOne({
    name: '000',
    password: '123456'
  }, {
    session
  })
  await db.collection('tableName').insertOne({
    _id: 0,
    name: '000'
  }, {
    session
  })
  await db.collection('tableName')
    .insertOne({
      id: Utils.UUID_V1(),
      code: 0
    }, {
      session
    })
  // await session.abortTransaction()
  await session.commitTransaction()
  session.endSession()
})