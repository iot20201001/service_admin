const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://192.168.200.1:27017,192.168.200.1:27018,192.168.200.1:27019/?replicaSet=testrs' // ?retryWrites=false'//
const dbname = 'runoob'
// https://www.bookstack.cn/read/TypeORM-0.2.20-zh/spilt.1.migrations.md
// https://www.cnblogs.com/laien/p/5610884.html
// /Elasticsearch——QueryBuilder简单查询
// https://www.cnblogs.com/sbj-dawn/p/8891419.html
// https://www.npmjs.com/package/mongodb
// http://mongodb.github.io/node-mongodb-native/3.6/api/
// http: //mongodb.github.io/node-mongodb-native/
//  Transaction numbers are only allowed on a replica set member or mongos
// Multi-document transactions are available for replica sets only.
var client = null
MongoClient.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
  // promiseLibrary: global.Promise
}, function (err, db) {
  console.info(err)
  client = db
})
async function test() {
  const db = client.db(dbname)
  const session = client.startSession()
  try {
    await session.startTransaction()
    await db.collection('sihwu111').insertOne({
      code: 0
    })
    await session.commitTransaction()
  } catch (e) {
    console.log('e', e)
    await session.abortTransaction()
  } finally {
    session.endSession()
    console.log('finally')
  }
}

setTimeout(() => {
  test()
}, 500)
// setInterval(() => {
//   test()
// }, 500)
// setInterval(() => {
//   test()
// }, 500)