// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../../dbV1.2/MessageEntitys')
class PreLibCertificate extends Entity {}
// https://www.cnblogs.com/huiwenhua/p/11449044.html
let ok = async _ => {
  let tenant_code = '1001'
  let partCode = 'WM-f417b4c4-8f53-4ef9-814d-7e594198fa88'
  new PreLibCertificate(tenant_code).FindOneEntity({
      // mesId: '7ccb3555-baa8-4ae1-841a-d3d12c66637f'
      partsList: {
        $elemMatch: {
          partCode
          // $eq: {
          //   partCode: partCode
          // }
        }
      }
    })
    .then(dbRes => {
      console.info(dbRes.data.partsList.filter(x => x.partCode === partCode))
    }).catch(err => {
      console.info(err)
    })
}
// ok()

/*
入库

// 前端逻辑
  查询本地是否已有数据，有则不做任何操作
  否则
      查询零件库存表，如已存在则提示相应状态
      否则
        查询开合格证信息，若找到合格的零件信息，则展示
        【查出相关库位信息 默认位置】
      否则
        提示未找到对应编码的零件
*/