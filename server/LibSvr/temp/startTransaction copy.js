const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://192.168.200.1:27017,192.168.200.1:27018,192.168.200.1:27019/?replicaSet=testrs' // ?retryWrites=false'//
const dbname = 'runoob'

//  Transaction numbers are only allowed on a replica set member or mongos
// Multi-document transactions are available for replica sets only.
var client = null
MongoClient.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
  // promiseLibrary: global.Promise
}, function (err, db) {
  console.info(err)
  client = db
})
async function test() {
  const db = client.db(dbname)
  const session = client.startSession()
  try {
    await session.startTransaction()
    // await db.collection('sihwu1').deleteMany({
    //   name: {
    //     $in: [null, '0001']
    //   }
    // }, {
    //   session
    // })
    let a = await db.collection('sihwu1').findOne({
      name: '0001'
    })
    console.info('-------------', a)
    if (!a) {
      a.count += 10
    }
    await db.collection('sihwu1').updateOne({
      name: '0001'
    }, {
      $set: a
    }, {
      session
    })
    let b = await db.collection('sihwu1').findOne({
      name: '0001'
    })
    // await db.collection('sihwu1').insertOne({
    //   _id: 0,
    //   name: '0001'
    // }, {
    //   session
    // })
    // await session.abortTransaction()
    // await db.collection('sihwu1').insertOne({})
    await session.commitTransaction()
    a = await db.collection('sihwu1').findOne({
      name: '0001'
    })
    console.info(a.count, b.count, a.count - b.count)
  } catch (e) {
    console.log('e', e)
    await session.abortTransaction()
  } finally {
    session.endSession()
    console.log('finally')
  }
}

setTimeout(() => {
  setInterval(() => {
    for (var i = 0; i < 1; i++) {
      test()
    }
  }, 500)
}, 1500)
// setInterval(() => {
//   test()
// }, 500)
// setInterval(() => {
//   test()
// }, 500)