const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://127.0.0.1:27017/'
const dbname = 'runoob'

//  Transaction numbers are only allowed on a replica set member or mongos
// Multi-document transactions are available for replica sets only.

async function test() {
  const client = await MongoClient.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    promiseLibrary: global.Promise
  })
  const db = client.db(dbname)
  const session = client.startSession({
    readConcern: {
      level: 'majority'
    },
    writeConcern: {
      w: 'majority'
    }
  })
  try {
    await session.startTransaction()
    await db.collection('sihwu1').deleteMany({
      name: '000'
    }, {
      session
    })
    await db.collection('sihwu1').insertOne({
      name: '000',
      password: '123456'
    }, {
      session
    })
    await db.collection('sihwu1').insertOne({
      _id: 0,
      name: '000'
    }, {
      session
    })
    await session.commitTransaction()
  } catch (e) {
    console.log('e', e)
    await session.abortTransaction()
  } finally {
    session.endSession()
    console.log('finally')
  }
}

test()