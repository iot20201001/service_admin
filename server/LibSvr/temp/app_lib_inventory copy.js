// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../../dbV1.2/MessageEntitys')
class PreLibMarking extends Entity {}
class PreLibMarkingHistory extends Entity {}
class PreLibCertificate extends Entity {}
class LibLattices extends Entity {}
class SysUser extends Entity {}
class SysDept extends Entity {}
// 查找
app.get('/page', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // 查找当前租户信息 如果租户是系统租户则能看到所有的租户参数
  let skip = 0
  let limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  console.info(req.query)
  if (req.query.creator_name) {
    search = {
      ...search,
      ...{
        creator_name: eval(`/${req.query.creator_name}/`)
      }
    }
  }
  if (req.query.status) {
    search = {
      ...search,
      ...{
        status: Number(req.query.status)
      }
    }
  }
  let opt = new SysLogLogin(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  } else {
    opt.sort({
      ctime: -1
    })
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 批量删除
app.post('/recvMarkingData', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  new PreLibMarking(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.post('/markingHistory', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  new PreLibMarkingHistory(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  new PreLibMarkingHistory(tenant_code).DeleteManyEntity(data)
  new PreLibMarking(tenant_code).DeleteEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.post('/clearAll', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new PreLibMarkingHistory(tenant_code).drop()
  new PreLibMarking(tenant_code).drop()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/queryAllMesId', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new PreLibMarking(tenant_code).toArray()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/FindPartByPartCode/:partCode', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let partCode = req.params.partCode
  new PreLibCertificate(tenant_code).FindOneEntity({
    partsList: {
      $elemMatch: {
        partCode
      }
    }
  })
    .then(dbRes => {
      if (dbRes.data) {
        res.json(Utils.ReturnOK(dbRes.data.partsList.filter(x => x.partCode === partCode)))
        // console.info(dbRes.data.partsList.filter(x => x.partCode === partCode))
      } else {
        res.json(Utils.ReturnError(null, '零件信息不存在'))
      }
    }).catch(err => {
      console.info(err)
    })
})

let getSubdept_ids = async function _getSubdept_ids (tenant_code, dept_id, name) {
  return new Promise(async (resolve, reject) => {
    let subDepts = await new SysDept(tenant_code).where({
      pid: dept_id
    }).toArray()
    if (subDepts.code === 0 && subDepts.data.length > 0) {
      let asyncFun = []
      subDepts.data.forEach(x => {
        console.info(x.id, x.name)
        asyncFun.push(_getSubdept_ids(tenant_code, x.id, x.name))
      })
      Promise.all(asyncFun).then(x => {
        console.info(x.length)
        resolve([name, ...x].join(','))
      })
    } else {
      resolve([name])
    }
  })
}
let ok = async _ => {
  let rr = await getSubdept_ids('1001', '86d46d90-a083-11ea-b42e-7dab4e78f954', 'XJ')
  console.warn(JSON.stringify(rr))
}
ok()
app.get('/getLatticesByPid', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let pid = req.query.pid
  console.info(req.query, req.params)
  let user = await new SysUser(tenant_code).FindOneEntity({
    id: uid
  })
  if (user.code !== 0 || !user.data) return Utils.ReturnError(null, '用户不存在！')
  user = user.data
  let params = {
    dept_id: user.dept_id,
    TREE_LEVEL: 3
  }
  if (pid !== undefined) {
    params = {
      pid: pid
    }
  }
  new LibLattices(tenant_code).where(params)
    .toArray()
    .then(dbRes => {
      // console.warn(params)
      if (dbRes.data) {
        res.json(Utils.ReturnOK(dbRes.data))
      } else {
        res.json(Utils.ReturnError(null, '库位信息不存在'))
      }
    }).catch(err => {
      res.json(Utils.ReturnError(err, '库位信息不存在'))
      // console.info(err)
    })
})
app.subPath = '/lib/inventory'
module.exports = app

/*
入库

// 前端逻辑
  查询本地是否已有数据，有则不做任何操作
  否则
      查询零件库存表，如已存在则提示相应状态
      否则
        查询开合格证信息，若找到合格的零件信息，则展示
        【查出相关库位信息 默认位置】
      否则
        提示未找到对应编码的零件
*/
