// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class PreLibMarking extends Entity {}
class PreLibMarkingHistory extends Entity {}
class PreLibCertificate extends Entity {}
class LibLattices extends Entity {}
class SysUser extends Entity {}
class SysDept extends Entity {}
class LibInStockHistory extends Entity {}
class LibInStock extends Entity {}
class LibStock extends Entity {}
class LibStockDetails extends Entity {}
// 查找
app.get('/page', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // 查找当前租户信息 如果租户是系统租户则能看到所有的租户参数
  let skip = 0
  let limit = 5
  try {
    skip = (parseInt(req.query.page) - 1) * req.query.limit
    limit = parseInt(req.query.limit)
  } catch {}
  let search = {}
  console.info(req.query)
  if (req.query.creator_name) {
    search = {
      ...search,
      ...{
        creator_name: eval(`/${req.query.creator_name}/`)
      }
    }
  }
  if (req.query.status) {
    search = {
      ...search,
      ...{
        status: Number(req.query.status)
      }
    }
  }
  let opt = new SysLogLogin(tenant_code)
  opt = opt.where(search)
  if (req.query.orderField) {
    let sort = {}
    eval(`sort.${req.query.orderField} = ${(req.query.order === 'desc') ? -1 : 1}`)
    // sort = Object.keys(sort).map(key => { key: parseInt(sort[key] + "") })[0]
    opt.sort(sort)
  } else {
    opt.sort({
      ctime: -1
    })
  }
  opt.skip(skip).limit(limit).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
// 批量删除
app.post('/recvMarkingData', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  new PreLibMarking(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.post('/markingHistory', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  new PreLibMarkingHistory(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.delete('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  new PreLibMarkingHistory(tenant_code).DeleteManyEntity(data)
  new PreLibMarking(tenant_code).DeleteEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.post('/clearAll', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new PreLibMarkingHistory(tenant_code).drop()
  new PreLibMarking(tenant_code).drop()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/queryAllMesId', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  new PreLibMarking(tenant_code).toArray()
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/FindPartByPartCode/:partCode', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let dept_id = req.headers['dept_id'] || ''
  let partCode = req.params.partCode
  new PreLibCertificate(tenant_code).FindOneEntity({
    partsList: {
      $elemMatch: {
        partCode
      }
    }
  })
    .then(dbRes => {
      if (dbRes.data) {
        let part = dbRes.data
        new LibInStock(tenant_code).where({
          partCode: partCode,
          certificateNo: part.partsList.find(x => x.partCode === partCode).certificateNo
        }).toArray(false).then(dbRes2 => {
          if (dbRes2.code === 0 && dbRes2.data.length > 0 && dept_id !== dbRes2.data[0].dept_id) {
            // 非 本部门【/子部门/子仓库 零件 ？？】
            return res.json(Utils.ReturnError(part, '偏移'))
          } else if (part.type === 0 && dbRes2.code === 0 && dbRes2.data.length > 0) {
            // 物码 已入库零件
            return res.json(Utils.ReturnError(part, '零件已入库'))
          } else if (part.type === 1 && dbRes2.code === 0 && dbRes2.code === 0 && dbRes2.data.length > 0 && dbRes2.data[0].maxNumber <= dbRes2.data[0].number) {
            // 批码 已全部入库
            return res.json(Utils.ReturnError(part, '零件已全部入库'))
          } else {
            // 判断接收单位是不是这里
            // 是否允许 子部门/子仓库 接收?
            // 重复入库问题，物码  所有仓库唯一
            // 重复入库问题，批码  所有仓库总数量必须一致
            dbRes.data.partsList = dbRes.data.partsList.filter(x => x.partCode === partCode)
            try {
              if (part.type === 1) dbRes.data.partsList[0].number = dbRes2.data[0].maxNumber - dbRes2.data[0].number
            } catch {}
            res.json(Utils.ReturnOK(dbRes.data))
          }
        }).catch(err => {
          res.json(Utils.ReturnError(err, '系统内部错误'))
        })
        // console.info(dbRes.data.partsList.filter(x => x.partCode === partCode))
      } else {
        res.json(Utils.ReturnError(null, '零件信息不存在'))
      }
    }).catch(err => {
      console.info(err)
    })
})

let getSubdept_ids = async function _getSubdept_ids (tenant_code, dept_id, name) {
  return new Promise(async (resolve, reject) => {
    let subDepts = await new SysDept(tenant_code).where({
      pid: dept_id
    }).toArray()
    if (subDepts.code === 0 && subDepts.data.length > 0) {
      let asyncFun = []
      subDepts.data.forEach(x => {
        // console.info(x.id, x.name)
        asyncFun.push(_getSubdept_ids(tenant_code, x.id, x.name))
      })
      Promise.all(asyncFun).then(x => {
        // console.info(x.length)
        resolve([dept_id, ...x].join(','))
      })
    } else {
      resolve([dept_id])
    }
  })
}
// let ok = async _ => {
//   let rr = await getSubdept_ids('1001', '86d46d90-a083-11ea-b42e-7dab4e78f954', 'XJ')
//   console.warn(JSON.stringify(rr.split(',')))
// }
// ok()
app.get('/getLatticesByPid', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let pid = req.query.pid
  console.info(req.query, req.params)
  let user = await new SysUser(tenant_code).FindOneEntity({
    id: uid
  })
  if (user.code !== 0 || !user.data) return Utils.ReturnError(null, '用户不存在！')
  user = user.data
  let params = null
  if (pid !== undefined) {
    params = {
      pid: pid
    }
  } else {
    let dept_ids = await getSubdept_ids(tenant_code, user.dept_id, 'XJ')
    params = {
      dept_id: {
        $in: dept_ids.split(',')
        // ['86d46d90-a083-11ea-b42e-7dab4e78f954', '8bde67f0-a083-11ea-b42e-7dab4e78f954', '3a8e6ba0-a31e-11ea-ac2c-7700958f46c2', '406504d0-a31e-11ea-ac2c-7700958f46c2', '956a19e0-a083-11ea-b42e-7dab4e78f954']
      },
      TREE_LEVEL: 3
    }
  }
  new LibLattices(tenant_code).where(params)
    .toArray()
    .then(dbRes => {
      // console.warn(params)
      if (dbRes.data) {
        res.json(Utils.ReturnOK(dbRes.data))
      } else {
        res.json(Utils.ReturnError(null, '库位信息不存在'))
      }
    }).catch(err => {
      res.json(Utils.ReturnError(err, '库位信息不存在'))
      // console.info(err)
    })
})
// Service 判断零件是否已入库【批码 已全部入库，入库数量超预期；物码：是否已入库】
let StoreHouseInclude = async (tenant_code, dept_id, arr) => {
  let asyncFun = []
  arr.forEach(x => {
    if (x.type === 0) {
      asyncFun.push(
        new LibInStock(tenant_code).where({
          partCode: x.partCode, dept_id, certificateNo: x.certificateNo
        }).toArray(false)
      )
    }
  })
  let results = await Promise.all(asyncFun)
  results = results.map(x => {
    if (x.data && x.data.length > 0) {
      return Array.from(new Set(x.data.map(x => x.partCode))).join(',')
    }
  })
  let ok = results.filter(x => x !== null && x !== undefined)
  if (ok.length > 0) {
    console.info('=================================================')
    console.info(JSON.stringify(ok))
    return Utils.ReturnError(ok, `零件【${ok[0]}】已入库`)
  } else {
    return [{ code: 0 }]
  }
}
app.post('/in2', async (req, res) => {
// class LibStock extends Entity {}
// class LibStockDetails extends Entity {}
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let dept_id = req.headers['dept_id'] || ''
  let arr = req.body
  arr.forEach(x => {
    x.id = Utils.UUID_V1()
    x.creator = uid
    x.dept_id = dept_id
  })
  let result = await StoreHouseInclude(tenant_code, dept_id, arr)
  res.json(result)
})
app.post('/in', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let dept_id = req.headers['dept_id'] || ''
  // console.info('=========================', dept_id)
  // 取得零件信息
  let arr = req.body
  // 填充信息
  arr.forEach(x => {
    x.id = Utils.UUID_V1()
    x.creator = uid
    x.dept_id = dept_id
  })
  // 区分物码和批码
  let arrWM = arr.filter(x => x.type === 0)
  let arrPM = arr.filter(x => x.type === 1)
  // 查看是否存在偏移**************★★★★★★
  // 判断零件是否已入库
  // 判断条件  同一 部门、零件编码、合格证号  视为唯一一次操作
  let asyncFun = []
  arr.forEach(x => {
    if (x.type === 0) {
      asyncFun.push(
        new LibInStock(tenant_code).where({
          partCode: x.partCode,
          dept_id,
          certificateNo: x.certificateNo
        }).toArray(false)
      )
    }
  })
  let rr = await Promise.all(asyncFun)
  rr = rr.map(x => {
    if (x.data && x.data.length > 0) {
      return Array.from(new Set(x.data.map(x => x.partCode))).join(',')
    }
  })
  let ok = rr.filter(x => x !== null && x !== undefined)
  if (ok.length > 0) {
    res.json(Utils.ReturnError(null, `零件【${ok[0]}】已入库`))
    console.info('=================================================')
    console.info(JSON.stringify(rr))
  } else {
    arr.forEach(x => {
      x.number = Number(x.number + '')
    })
    new LibInStockHistory(tenant_code).AddManyEntity(arr)
    // 物码【直接入库】
    // 批码【查询是否有历史入库，改库存】
    // 事务根据零件图号、机型、架次、批次、接收单位、部门id 修改库存
    console.info(JSON.stringify(arr))
    // 如果是物码  直接插入
    // 如果是批码  先找  未找到 直接插入  找到 修改库存
    let arrWM = arr.filter(x => x.type === 0)
    let arrPM = arr.filter(x => x.type === 1)
    if (arrWM.length > 0) {
      await new LibInStock(tenant_code).AddManyEntity(arrWM)
    }
    asyncFun = []
    arrPM.forEach(x => {
      let params = {}
      if (x.partCode) params.partCode = x.partCode
      if (x.aircraftType) params.aircraftType = x.aircraftType
      if (x.apponitSortie) params.apponitSortie = x.apponitSortie
      if (x.produceBatch) params.produceBatch = x.produceBatch
      if (x.certificateNo) params.certificateNo = x.certificateNo
      params.dept_id = dept_id
      asyncFun.push(new LibInStock(tenant_code).FindOneEntity(params))
    })
    rr = await Promise.all(asyncFun)
    let rrUpdate = rr.map(x => {
      if (x.data && x.data) {
        return x.data.partCode // Array.from(new Set(x.data.map(x => x.partCode))).join(',')
      }
    })
    let tt_rrUpdate = rrUpdate.filter(x => x !== null && x !== undefined)
    asyncFun = []
    rr = rr.map(x => {
      if (x.data && x.data) {
        let obj = arr.find(y => y.partCode === x.data.partCode)
        // 批码修改数量后保存
        x.data.number += Number(obj.number + '')
        if (x.data.maxNumber >= x.data.number) {
          asyncFun.push(new LibInStock(tenant_code).SaveEntity(x.data, {
            _id: x.data._id
          }))
        }
        // return Array.from(new Set(x.data.map(x => x.partCode))).join(',')
      }
    })
    arrPM = arrPM.filter(x => !tt_rrUpdate.includes(x.partCode))
    if (arrPM.length > 0) {
      asyncFun.push(new LibInStock(tenant_code).AddManyEntity(arrPM))
    }
    rr = await Promise.all(asyncFun)
    rr = rr.map(x => {
      if (x.code === 0) {
        if (x.ops && x.data.result) return Array.from(new Set(x.ops.map(x => x.partCode))).join(',')
        else if (!x.ops && x.data && x.data.length > 0) return Array.from(new Set(x.data.map(x => x.partCode))).join(',')
      } else {
        return `Error`
      }
    })
    res.json(Utils.ReturnOK(rr))
    console.info('=================================================')
    console.info(JSON.stringify(rr))
  }
})

app.get('/getTestData', async (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let dept_id = req.headers['dept_id'] || ''
  let rr = await new PreLibCertificate(tenant_code)
    .select('partsNo')
    .toArray(false)
  res.json(rr)
})
app.subPath = '/lib/instock'
module.exports = app

/*
入库

// 前端逻辑
  查询本地是否已有数据，有则不做任何操作
  否则
      查询零件库存表，如已存在则提示相应状态
      否则
        查询开合格证信息，若找到合格的零件信息，则展示
        【查出相关库位信息 默认位置】
      否则
        提示未找到对应编码的零件
*/
