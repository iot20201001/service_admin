// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class LibPlan extends Entity {}
class LibInStock extends Entity {}
class LibStockLattices extends Entity {}
app.post('/', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  new LibPlan(tenant_code).AddEntity(data)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})

app.get('/getPlan', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  // let uid = req.headers['uid'] || ''
  // let data = req.body
  // data.id = Utils.UUID_V1()
  // data.uid = uid
  let params = {}
  // console.error(req.query)
  if (req.query.needId) params.needId = req.query.needId
  new LibPlan(tenant_code).where(params).toArray(true)
    .then(dbRes => {
      res.json(dbRes)
    }).catch(err => {
      res.json(err)
    })
})
app.get('/Computed', (req, res) => {
  // 计算后需要把零件置为占用状态【物码直接占用，批码按数量占用】
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.uid = uid
  let params = {}
  // console.error(req.query)
  if (req.query.needId) params.needId = req.query.needId
  new LibPlan(tenant_code).where(params).toArray(true)
    .then(async dbRes => {
      // 未匹配零件 partCode 标识
      // eslint-disable-next-line camelcase
      let t_partCodeList = ['缺件', '', '-', '待计算']
      // 多配套包同时配套需要整合图号、机型、批次、架次等信息
      let t = dbRes.data[0]
      // 最终结果
      let rr = []
      let tempList = t.partList ? t.partList.filter(x => t_partCodeList.includes(x.partCode)) : t.bomList
      // 找出即将计算的图号，数量等信息【目前不考虑机型、批次、架次】
      for (var i = 0; i < tempList.length; i++) {
        let tt = await new LibStockLattices(tenant_code).where({
          partNo: tempList[i].partNo
        }).toArray()
        // 如果找到对应图号
        if (tt.code === 0 && tt.data.length > 0) {
          // 此处应该按未占用的数量计算【可用数量 = 当前数量 - 减去锁定数量】
          let okNumber = tt.data.map(y => (Number(y.number + '') - Number(y.lockNumber || '0'))).reduce((a, b) => a + b)
          if (okNumber > 0) {
            if (tempList[i].partCount > okNumber) {
              // 如果可用数量小于需求数量
              // 缺件数量
              tempList[i].partCount -= okNumber
              // 处理匹配零件
              tt.data.forEach(x => {
                x.partCount = Number(x.number + '') - Number(x.lockNumber || '0')
                x.lockNumber = x.number
              })
              rr = [...tt.data]
              // 处理缺件
              rr.push({
                partCode: '缺件',
                partName: tempList[i].partName,
                partCount: tempList[i].partCount,
                partNo: tempList[i].partNo
              })
            } else {
              // 处理匹配零件
              // 可用数量大于需求数量
              let total = 0
              for (var j = 0; j < tt.data.length; j++) {
                if (total < tempList[j].partCount) {
                  let number = tempList[j].partCount - total
                  total += tt.data[j].number
                  let tNumber = Number(tt.data[j].number + '') - Number(tt.data[j].lockNumber || '0')
                  rr.push({
                    ...tt.data[j],
                    partCount: tNumber > number ? number : tNumber,
                    lockNumber: tt.data[j].lockNumber + (tNumber > number ? number : tNumber)
                  })
                }
              }
            }
          }
        }
      }
      if (rr.length > 0) {
        rr.forEach(x => {
          x.isComputed = true
          x.isScanOK = false
        })
        // 更改占用状态
        rr.forEach(async x => {
          if (t_partCodeList.includes(x.partCode)) return
          // 此处还需要进行异常判断【锁定数量超限的话就异常】
          await new LibStockLattices(tenant_code).UpdateEntity({
            _id: x._id
          }, x)
        })
      }
      // 如果上次有已经计算过的零件
      // let oldPartList = t.partList ? t.partList.filter(x => !t_partCodeList.includes(x.partCode)) : []
      let oldPartList = t.partList || []
      // 上次有计算
      if (oldPartList.length > 0) {
        // 本次有新增
        if (rr.length > 0) { t.partList = [...oldPartList, ...rr] }
        // 本次无新增
        else { t.partList = oldPartList }
      } else {
        // 上次无计算
        // 本次有新增
        if (rr.length > 0) { t.partList = rr }
        // 本次无新增
        else {
          // 处理bomList【这里处理不合理】
          // t.partList = t.bomList.forEach(x => x.partCode = '')
        }
      }
      let partNos = t.partList ? Array.from(new Set(t.partList.map(x => x.partNo))) : []
      if (!t.partList) t.partList = []
      t.bomList.forEach(x => {
        if (!partNos.includes(x.partNo)) {
          t.partList.push({
            partCode: '缺件',
            partName: x.partName,
            partCount: x.partCount,
            partNo: x.partNo
          })
        }
      })
      await new LibPlan(tenant_code).UpdateEntity({
        _id: t._id
      }, t)
        .then(dbRes => {
        }).catch(err => {
          res.json(err)
        })
      // 处理bomList
      //
      res.json(dbRes)
      // console.warn(dbRes)
    }).catch(err => {
      res.json(err)
      console.info(err)
    })
})
app.put('/Complete', (req, res) => {
  let tenant_code = req.headers['tenant_code'] || 'tenant_code'
  let uid = req.headers['uid'] || ''
  let data = req.body
  data.id = Utils.UUID_V1()
  data.updater = uid
  new LibPlan(tenant_code).UpdateEntity({
    _id: data._id
  }, data)
    .then(dbRes => {
      res.json(Utils.ReturnOK(dbRes))
    }).catch(err => {
      res.json(err)
    })
})
app.subPath = '/lib/plan'
module.exports = app

/*
入库

// 前端逻辑
  查询本地是否已有数据，有则不做任何操作
  否则
      查询零件库存表，如已存在则提示相应状态
      否则
        查询开合格证信息，若找到合格的零件信息，则展示
        【查出相关库位信息 默认位置】
      否则
        提示未找到对应编码的零件
*/
