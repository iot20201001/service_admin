// express_demo.js 文件
var express = require('express')
var app = express()
var {
  Utils,
  Entity
} = require('../dbV1.2/MessageEntitys')
class LibPlan extends Entity {}
class LibInStock extends Entity {}
class LibStockLattices extends Entity {}

let ok = async _ => {
  let tenant_code = '1001'

  let params = {}
  params.needId = '877c62c7-1470-4bb0-8db7-860861f8a2e0'
  new LibPlan(tenant_code).where(params).toArray(true)
    .then(async dbRes => {
      let t = dbRes.data[0]
      for (var i = 0; i < t.bomList.length; i++) {
        let tt = await new LibStockLattices(tenant_code).where({
          partsNo: t.bomList[i].partNo
        }).toArray()
        if (tt.code === 0 && tt.data.length > 0) {
          t.bomList[i] = [...tt.data]
        }
      }
      //
      console.info(dbRes)
    }).catch(err => {
      console.info(err)
    })
}
ok()
