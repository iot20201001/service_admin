/**
 * 配置参考: https://cli.vuejs.org/zh/config/
 */
// https://stackoverflow.com/questions/61884293/how-to-fix-error-unexpected-usage-loading-foreign-module-for-monaco-editor-in-v
// https://stackoverflow.com/questions/61884293/how-to-fix-error-unexpected-usage-loading-foreign-module-for-monaco-editor-in-v
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin')
// const TerserPlugin = require('terser-webpack-plugin')
// const PurgecssPlugin = require('purgecss-webpack-plugin')
module.exports = {
  transpileDependencies: ['vuex-persist', 'vuex-persistedstate'],
  configureWebpack: {
    devtool: false,
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000
      },
      nodeEnv: 'production',
      minimize: true,
      // minimizer: [
      //   new TerserPlugin({
      //     extractComments: 'false',
      //     parallel: true
      //   })
      // ],
      removeEmptyChunks: true,
      removeAvailableModules: true,
      mergeDuplicateChunks: true
    },
    plugins: [
      new MonacoWebpackPlugin({
        languages: ['javascript', 'css', 'html', 'typescript', 'json'],
        features: ['!gotoSymbol']
      })
      // new PurgecssPlugin({
      //   paths: glob.sync(`${PATHS.src}/**/*`, {
      //     nodir: true
      //   })
      // })
    ]
  },
  baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  chainWebpack: config => {
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .test(/\.svg$/)
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
  },
  // 默认打开eslint效验，如果需要关闭，设置成false即可
  lintOnSave: true,
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8001,
    overlay: {
      errors: true,
      warnings: true
    }
  }
}
