import axios from 'axios'
import {
  Message
} from 'element-ui'
import Cookies from 'js-cookie'
import router from '@/router'
import qs from 'qs'
import {
  clearLoginInfo
} from '@/utils'
import isPlainObject from 'lodash/isPlainObject'
// 创建一个错误
function errorCreate (msg) {
  const error = new Error(msg)
  errorLog(error)
  // throw error
}
// 记录和显示错误
function errorLog (error) {
  // 添加到日志
  // store.dispatch('d2admin/log/push', {
  //   message: '数据请求异常',
  //   type: 'danger',
  //   meta: {
  //     error
  //   }
  // })
  // 复印到控制台
  if (process.env.NODE_ENV === 'development') {
    // util.log.danger('>>>>>> Error >>>>>>')
    console.log(error)
  }
  // 显示提示
  Message({
    message: error.message,
    type: 'error',
    duration: 5 * 1000
  })
}
const baseURL = window.SITE_CONFIG['apiURL'] // 'http://iot.egdmail.cn'// 'https://bingli.cmu4h.com.cn'
const notTipUrl = ['/sys/dict/type/all', '/sys/menu/nav']
const http = axios.create({
  baseURL: window.SITE_CONFIG['apiURL'],
  timeout: 1000 * 180
  // withCredentials: true
})

/**
 * 请求拦截
 */
http.interceptors.request.use(config => {
  config.headers['Accept-Language'] = Cookies.get('language') || 'zh-CN'
  config.headers['terminalType'] = 'PCWeb'
  config.headers['foreEndVersion'] = 'V2.1'
  config.headers['token'] = Cookies.get('token') || localStorage.getItem('token') || ''
  // console.info(Cookies.get('token'), Cookies.get('tenant_code'))
  config.headers['tenant_code'] = Cookies.get('tenant_code') || 'tenant_code'
  // 默认参数
  var defaults = {}
  // 防止缓存，GET请求默认带_t参数
  if (config.method === 'get') {
    // console.error('--------------11111111111-----------', config.params)
    config.params = {
      ...config.params,
      ...{
        '_t': new Date().getTime()
      }
    }
  }
  if (isPlainObject(config.params)) {
    config.params = {
      ...defaults,
      ...config.params
    }
  }
  if (isPlainObject(config.data)) {
    config.data = {
      ...defaults,
      ...config.data
    }
    if (/^application\/x-www-form-urlencoded/.test(config.headers['content-type'])) {
      config.data = qs.stringify(config.data)
    }
  }
  return config
}, error => {
  return Promise.reject(error)
})

/**
 * 响应拦截
 */
http.interceptors.response.use(response => {
  // dataAxios 是 axios 返回数据中的 data
  const dataAxios = response.data
  // eslint-disable-next-line no-debugger
  // debugger
  console.info(response.config.url, JSON.stringify(dataAxios))
  // 这个状态码是和后端约定的
  const {
    code
  } = dataAxios
  if (code === 0) {
    // [ 示例 ] code === 0 代表没有错误
    return Promise.resolve(response)
  } else if (code === undefined) {
    // 如果没有 code 代表这不是项目后端开发的接口
    return dataAxios
  } else if (code === 401) {
    clearLoginInfo()
    errorCreate(`${dataAxios.msg}`)
    // console.info(response.config.url)
    if (!response.config.url.includes('/login')) {
      router.replace({
        name: 'login'
      })
    }
    // return Promise.reject(response.data.msg)
  } else if (code > 0 && code < 99999) {
    // errorCreate(`${dataAxios.msg}: ${response.config.url}`)
    let isFind = false
    notTipUrl.forEach(x => {
      console.info(response.config.url, x, response.config.url.indexOf(x) !== -1)
      if (response.config.url.indexOf(x) !== -1) {
        isFind = true
      }
    })
    if (!isFind) { errorCreate(`${dataAxios.msg}AAAAAA`) }
    // Promise.resolve(dataAxios)
    // return Promise.reject(response.data.msg)
    // [ 示例 ] 其它和后台约定的 code
  } else {
    // 不是正确的 code
    errorCreate(`${dataAxios.msg}: ${response.config.url}`)
    // Promise.resolve(dataAxios)
  }
  return response
},
error => {
  if (error && error.response) {
    switch (error.response.status) {
      case 400: error.message = '请求错误'; break
      case 401: error.message = '未授权，请登录'; break
      case 403: error.message = '拒绝访问'; break
      case 404: error.message = `请求地址出错: ${error.response.config.url}`; break
      case 408: error.message = '请求超时'; break
      case 500: error.message = '服务器内部错误'; break
      case 501: error.message = '服务未实现'; break
      case 502: error.message = '网关错误'; break
      case 503: error.message = '服务不可用'; break
      case 504: error.message = '网关超时'; break
      case 505: error.message = 'HTTP版本不受支持'; break
      default: break
    }
  }
  if (error.message === 'Network Error') error.message = `请求地址出错: ${error.response.config} - ${error.config.url}`
  errorLog(error)
  return Promise.reject(error)
})
http.baseURL = baseURL
export default http
