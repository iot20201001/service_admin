import RenMonacoEditor from './src/ren-monaco-editor'

RenMonacoEditor.install = function (Vue) {
  Vue.component(RenMonacoEditor.name, RenMonacoEditor)
}

export default RenMonacoEditor
