import Vue from 'vue'
import Element from 'element-ui'
import App from '@/App'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'
import '@/icons'
import '@/element-ui/theme/index.css'
import '@/assets/scss/aui.scss'
import http from '@/utils/request'
import renRadioGroup from '@/components/ren-radio-group'
import renSelect from '@/components/ren-select'
import renProcessMultiple from '@/components/ren-process-multiple'
import renProcessStart from '@/components/ren-process-start'
import renProcessRunning from '@/components/ren-process-running'
import renProcessDetail from '@/components/ren-process-detail'
import renDeptTree from '@/components/ren-dept-tree'
import renRegionTree from '@/components/ren-region-tree'
import renMonacoEditor from '@/components/ren-monaco-editor'
import {
  hasPermission,
  getDictLabel
} from '@/utils'
import cloneDeep from 'lodash/cloneDeep'
// import hotkeys from 'hotkeys-js'
import SvgIcon from '@/components/SvgIcon'
import VeLine from 'v-charts/lib/line.common'
// https://blog.csdn.net/solocao/article/details/89023450
// https://blog.csdn.net/libin_1/article/details/51835232

// hotkeys('f5', (event, handler) => {
//   // Prevent the default refresh event under WINDOWS system
//   event.preventDefault()
//   alert('you pressed F5!')
// })
// hotkeys('ctrl+a,ctrl+b,r,f', function (event, handler) {
//   switch (handler.key) {
//     case 'ctrl+a': alert('you pressed ctrl+a!')
//       break
//     case 'ctrl+b': alert('you pressed ctrl+b!')
//       break
//     case 's':
//       this.$router.push({ path: '/login' })
//       break
//     case 'f': alert('you pressed f!')
//       break
//     default: alert(event)
//   }
// })
// https://www.cnblogs.com/dhpong/p/10500370.html
// import Print from 'vue-print-nb'
import Print from '@/plugs/print'
// import VueSocketIO from 'vue-socket.io'
// import SocketIO from 'socket.io-client'
// const options = { path: '/' }
// Vue.use(new VueSocketIO({
//   debug: true,
//   // connection: SocketIO('ws://iot.egdmail.cn', options),
//   connection: 'ws://iot.egdmail.cn',
//   vuex: {
//     store,
//     actionPrefix: 'SOCKET_',
//     mutationPrefix: 'SOCKET_'
//   },
//   options: { path: '/' }
// }))

Vue.use(Print)
Vue.component('svg-icon', SvgIcon)
Vue.component(VeLine.name, VeLine)

Vue.config.productionTip = false

Vue.use(renRadioGroup)
Vue.use(renSelect)
Vue.use(renDeptTree)
Vue.use(renRegionTree)
Vue.use(renMonacoEditor)
Vue.use(renProcessMultiple)
Vue.use(renProcessStart)
Vue.use(renProcessRunning)
Vue.use(renProcessDetail)

Vue.use(Element, {
  size: 'default',
  i18n: (key, value) => i18n.t(key, value)
})

// 挂载全局
Vue.prototype.$http = http
Vue.prototype.$hasPermission = hasPermission
Vue.prototype.$getDictLabel = getDictLabel

// 保存整站vuex本地储存初始状态
window.SITE_CONFIG['storeState'] = cloneDeep(store.state)
Vue.prototype.$resetDataForm = (dataForm) => {
  delete dataForm._id
  delete dataForm.ctime
  delete dataForm.utime
  delete dataForm.tenant_code
  return dataForm
}
let formatDate = (value, fmt = 'yyyy-MM-dd hh:mm:ss') => {
  let getDate = new Date(value)
  let o = {
    'M+': getDate.getMonth() + 1,
    'd+': getDate.getDate(),
    'h+': getDate.getHours(),
    'm+': getDate.getMinutes(),
    's+': getDate.getSeconds(),
    'q+': Math.floor((getDate.getMonth() + 3) / 3),
    'S': getDate.getMilliseconds()
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}
Vue.prototype.$formatDate = formatDate
// 时间格式化
// 用法：<div>{{data | dataFormat('yyyy-MM-dd hh:mm:ss')}}</div>
Vue.filter('formatDate', (value, fmt = 'yyyy-MM-dd hh:mm:ss') => {
  return formatDate(value, fmt)
})

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
