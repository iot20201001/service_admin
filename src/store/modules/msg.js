export default {
  namespaced: true,
  state: {
    socket: null,
    msgList: []
  },
  getters: {
    getSocket: state => state.socket,
    getMsgList: state => state.msgList, // .splice(state.msgList.length - 5),
    getMsgCount: state => state.msgList.length
  },
  mutations: {
    setSocket (state, _socket) {
      state.socket = _socket
    },
    pushMsg (state, msg) {
      state.msgList.push(msg)
    },
    popMsg (state) {
      return state.msgList.pop()
    },
    clearMsg (state) {
      state.msgList = []
    }
  },
  actions: {
    pushMsg ({ commit }, msg) {
      commit('pushMsg', msg)
    },
    clearMsg ({ commit }) {
      commit('clearMsg')
    }
  }
}
