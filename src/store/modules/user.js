export default {
  namespaced: true,
  state: {
    id: 0,
    name: '',
    real_name: '',
    superAdmin: 1,
    superTenant: 1,
    tenant_name: ''
  }
}
